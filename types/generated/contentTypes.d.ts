import type { Schema, Attribute } from '@strapi/strapi';

export interface AdminPermission extends Schema.CollectionType {
  collectionName: 'admin_permissions';
  info: {
    name: 'Permission';
    description: '';
    singularName: 'permission';
    pluralName: 'permissions';
    displayName: 'Permission';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    action: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    actionParameters: Attribute.JSON & Attribute.DefaultTo<{}>;
    subject: Attribute.String &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    properties: Attribute.JSON & Attribute.DefaultTo<{}>;
    conditions: Attribute.JSON & Attribute.DefaultTo<[]>;
    role: Attribute.Relation<'admin::permission', 'manyToOne', 'admin::role'>;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'admin::permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'admin::permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface AdminUser extends Schema.CollectionType {
  collectionName: 'admin_users';
  info: {
    name: 'User';
    description: '';
    singularName: 'user';
    pluralName: 'users';
    displayName: 'User';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    firstname: Attribute.String &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    lastname: Attribute.String &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    username: Attribute.String;
    email: Attribute.Email &
      Attribute.Required &
      Attribute.Private &
      Attribute.Unique &
      Attribute.SetMinMaxLength<{
        minLength: 6;
      }>;
    password: Attribute.Password &
      Attribute.Private &
      Attribute.SetMinMaxLength<{
        minLength: 6;
      }>;
    resetPasswordToken: Attribute.String & Attribute.Private;
    registrationToken: Attribute.String & Attribute.Private;
    isActive: Attribute.Boolean &
      Attribute.Private &
      Attribute.DefaultTo<false>;
    roles: Attribute.Relation<'admin::user', 'manyToMany', 'admin::role'> &
      Attribute.Private;
    blocked: Attribute.Boolean & Attribute.Private & Attribute.DefaultTo<false>;
    preferedLanguage: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<'admin::user', 'oneToOne', 'admin::user'> &
      Attribute.Private;
    updatedBy: Attribute.Relation<'admin::user', 'oneToOne', 'admin::user'> &
      Attribute.Private;
  };
}

export interface AdminRole extends Schema.CollectionType {
  collectionName: 'admin_roles';
  info: {
    name: 'Role';
    description: '';
    singularName: 'role';
    pluralName: 'roles';
    displayName: 'Role';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String &
      Attribute.Required &
      Attribute.Unique &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    code: Attribute.String &
      Attribute.Required &
      Attribute.Unique &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    description: Attribute.String;
    users: Attribute.Relation<'admin::role', 'manyToMany', 'admin::user'>;
    permissions: Attribute.Relation<
      'admin::role',
      'oneToMany',
      'admin::permission'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<'admin::role', 'oneToOne', 'admin::user'> &
      Attribute.Private;
    updatedBy: Attribute.Relation<'admin::role', 'oneToOne', 'admin::user'> &
      Attribute.Private;
  };
}

export interface AdminApiToken extends Schema.CollectionType {
  collectionName: 'strapi_api_tokens';
  info: {
    name: 'Api Token';
    singularName: 'api-token';
    pluralName: 'api-tokens';
    displayName: 'Api Token';
    description: '';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String &
      Attribute.Required &
      Attribute.Unique &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    description: Attribute.String &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }> &
      Attribute.DefaultTo<''>;
    type: Attribute.Enumeration<['read-only', 'full-access', 'custom']> &
      Attribute.Required &
      Attribute.DefaultTo<'read-only'>;
    accessKey: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    lastUsedAt: Attribute.DateTime;
    permissions: Attribute.Relation<
      'admin::api-token',
      'oneToMany',
      'admin::api-token-permission'
    >;
    expiresAt: Attribute.DateTime;
    lifespan: Attribute.BigInteger;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'admin::api-token',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'admin::api-token',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface AdminApiTokenPermission extends Schema.CollectionType {
  collectionName: 'strapi_api_token_permissions';
  info: {
    name: 'API Token Permission';
    description: '';
    singularName: 'api-token-permission';
    pluralName: 'api-token-permissions';
    displayName: 'API Token Permission';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    action: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    token: Attribute.Relation<
      'admin::api-token-permission',
      'manyToOne',
      'admin::api-token'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'admin::api-token-permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'admin::api-token-permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface AdminTransferToken extends Schema.CollectionType {
  collectionName: 'strapi_transfer_tokens';
  info: {
    name: 'Transfer Token';
    singularName: 'transfer-token';
    pluralName: 'transfer-tokens';
    displayName: 'Transfer Token';
    description: '';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String &
      Attribute.Required &
      Attribute.Unique &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    description: Attribute.String &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }> &
      Attribute.DefaultTo<''>;
    accessKey: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    lastUsedAt: Attribute.DateTime;
    permissions: Attribute.Relation<
      'admin::transfer-token',
      'oneToMany',
      'admin::transfer-token-permission'
    >;
    expiresAt: Attribute.DateTime;
    lifespan: Attribute.BigInteger;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'admin::transfer-token',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'admin::transfer-token',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface AdminTransferTokenPermission extends Schema.CollectionType {
  collectionName: 'strapi_transfer_token_permissions';
  info: {
    name: 'Transfer Token Permission';
    description: '';
    singularName: 'transfer-token-permission';
    pluralName: 'transfer-token-permissions';
    displayName: 'Transfer Token Permission';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    action: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    token: Attribute.Relation<
      'admin::transfer-token-permission',
      'manyToOne',
      'admin::transfer-token'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'admin::transfer-token-permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'admin::transfer-token-permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginUploadFile extends Schema.CollectionType {
  collectionName: 'files';
  info: {
    singularName: 'file';
    pluralName: 'files';
    displayName: 'File';
    description: '';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String & Attribute.Required;
    alternativeText: Attribute.String;
    caption: Attribute.String;
    width: Attribute.Integer;
    height: Attribute.Integer;
    formats: Attribute.JSON;
    hash: Attribute.String & Attribute.Required;
    ext: Attribute.String;
    mime: Attribute.String & Attribute.Required;
    size: Attribute.Decimal & Attribute.Required;
    url: Attribute.String & Attribute.Required;
    previewUrl: Attribute.String;
    provider: Attribute.String & Attribute.Required;
    provider_metadata: Attribute.JSON;
    related: Attribute.Relation<'plugin::upload.file', 'morphToMany'>;
    folder: Attribute.Relation<
      'plugin::upload.file',
      'manyToOne',
      'plugin::upload.folder'
    > &
      Attribute.Private;
    folderPath: Attribute.String &
      Attribute.Required &
      Attribute.Private &
      Attribute.SetMinMax<
        {
          min: 1;
        },
        number
      >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::upload.file',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::upload.file',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginUploadFolder extends Schema.CollectionType {
  collectionName: 'upload_folders';
  info: {
    singularName: 'folder';
    pluralName: 'folders';
    displayName: 'Folder';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMax<
        {
          min: 1;
        },
        number
      >;
    pathId: Attribute.Integer & Attribute.Required & Attribute.Unique;
    parent: Attribute.Relation<
      'plugin::upload.folder',
      'manyToOne',
      'plugin::upload.folder'
    >;
    children: Attribute.Relation<
      'plugin::upload.folder',
      'oneToMany',
      'plugin::upload.folder'
    >;
    files: Attribute.Relation<
      'plugin::upload.folder',
      'oneToMany',
      'plugin::upload.file'
    >;
    path: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMax<
        {
          min: 1;
        },
        number
      >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::upload.folder',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::upload.folder',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginContentReleasesRelease extends Schema.CollectionType {
  collectionName: 'strapi_releases';
  info: {
    singularName: 'release';
    pluralName: 'releases';
    displayName: 'Release';
  };
  options: {
    draftAndPublish: false;
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String & Attribute.Required;
    releasedAt: Attribute.DateTime;
    scheduledAt: Attribute.DateTime;
    timezone: Attribute.String;
    actions: Attribute.Relation<
      'plugin::content-releases.release',
      'oneToMany',
      'plugin::content-releases.release-action'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::content-releases.release',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::content-releases.release',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginContentReleasesReleaseAction
  extends Schema.CollectionType {
  collectionName: 'strapi_release_actions';
  info: {
    singularName: 'release-action';
    pluralName: 'release-actions';
    displayName: 'Release Action';
  };
  options: {
    draftAndPublish: false;
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    type: Attribute.Enumeration<['publish', 'unpublish']> & Attribute.Required;
    entry: Attribute.Relation<
      'plugin::content-releases.release-action',
      'morphToOne'
    >;
    contentType: Attribute.String & Attribute.Required;
    locale: Attribute.String;
    release: Attribute.Relation<
      'plugin::content-releases.release-action',
      'manyToOne',
      'plugin::content-releases.release'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::content-releases.release-action',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::content-releases.release-action',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginUsersPermissionsPermission
  extends Schema.CollectionType {
  collectionName: 'up_permissions';
  info: {
    name: 'permission';
    description: '';
    singularName: 'permission';
    pluralName: 'permissions';
    displayName: 'Permission';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    action: Attribute.String & Attribute.Required;
    role: Attribute.Relation<
      'plugin::users-permissions.permission',
      'manyToOne',
      'plugin::users-permissions.role'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::users-permissions.permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::users-permissions.permission',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginUsersPermissionsRole extends Schema.CollectionType {
  collectionName: 'up_roles';
  info: {
    name: 'role';
    description: '';
    singularName: 'role';
    pluralName: 'roles';
    displayName: 'Role';
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    description: Attribute.String;
    type: Attribute.String & Attribute.Unique;
    permissions: Attribute.Relation<
      'plugin::users-permissions.role',
      'oneToMany',
      'plugin::users-permissions.permission'
    >;
    users: Attribute.Relation<
      'plugin::users-permissions.role',
      'oneToMany',
      'plugin::users-permissions.user'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::users-permissions.role',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::users-permissions.role',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginUsersPermissionsUser extends Schema.CollectionType {
  collectionName: 'up_users';
  info: {
    name: 'user';
    description: '';
    singularName: 'user';
    pluralName: 'users';
    displayName: 'User';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    username: Attribute.String &
      Attribute.Required &
      Attribute.Unique &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    email: Attribute.Email &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 6;
      }>;
    provider: Attribute.String;
    password: Attribute.Password &
      Attribute.Private &
      Attribute.SetMinMaxLength<{
        minLength: 6;
      }>;
    resetPasswordToken: Attribute.String & Attribute.Private;
    confirmationToken: Attribute.String & Attribute.Private;
    confirmed: Attribute.Boolean & Attribute.DefaultTo<false>;
    blocked: Attribute.Boolean & Attribute.DefaultTo<false>;
    role: Attribute.Relation<
      'plugin::users-permissions.user',
      'manyToOne',
      'plugin::users-permissions.role'
    >;
    azureId: Attribute.String;
    employee: Attribute.Relation<
      'plugin::users-permissions.user',
      'oneToOne',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::users-permissions.user',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::users-permissions.user',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginPmsPmsproject extends Schema.CollectionType {
  collectionName: 'pmsprojects';
  info: {
    singularName: 'pmsproject';
    pluralName: 'pmsprojects';
    displayName: 'PMS Project';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    projectName: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    description: Attribute.Text &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 2000;
      }>;
    clientName: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    startDate: Attribute.Date;
    endDate: Attribute.Date;
    threads: Attribute.Relation<
      'plugin::pms.pmsproject',
      'oneToMany',
      'plugin::pms.pmsthread'
    >;
    projectOwner: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    region: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    country: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    status: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::pms.pmsproject',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::pms.pmsproject',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginPmsPmsthread extends Schema.CollectionType {
  collectionName: 'pms_threads';
  info: {
    singularName: 'pmsthread';
    pluralName: 'pmsthreads';
    displayName: 'PMS Thread';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    threadName: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    threadLead: Attribute.String & Attribute.Required;
    description: Attribute.Text &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 2000;
      }>;
    domainName: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    techStack: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 500;
      }>;
    project: Attribute.Relation<
      'plugin::pms.pmsthread',
      'manyToOne',
      'plugin::pms.pmsproject'
    >;
    type: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 500;
      }>;
    versions: Attribute.Relation<
      'plugin::pms.pmsthread',
      'oneToMany',
      'plugin::pms.pmsversion'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::pms.pmsthread',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::pms.pmsthread',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginPmsPmsversion extends Schema.CollectionType {
  collectionName: 'pmsversions';
  info: {
    singularName: 'pmsversion';
    pluralName: 'pmsversions';
    displayName: 'pmsversion';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    versionName: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    description: Attribute.Text &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 2000;
      }>;
    startDate: Attribute.Date & Attribute.Required;
    endDate: Attribute.Date & Attribute.Required;
    versionComment: Attribute.Relation<
      'plugin::pms.pmsversion',
      'oneToMany',
      'plugin::pms.versioncommentorrequest'
    >;
    status: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    thread: Attribute.Relation<
      'plugin::pms.pmsversion',
      'manyToOne',
      'plugin::pms.pmsthread'
    >;
    versionResources: Attribute.Relation<
      'plugin::pms.pmsversion',
      'manyToMany',
      'plugin::pms.pmsversionresource'
    >;
    isDelayed: Attribute.Enumeration<['true', 'false']> &
      Attribute.Required &
      Attribute.DefaultTo<'false'>;
    projectName: Attribute.String & Attribute.Required;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::pms.pmsversion',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::pms.pmsversion',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginPmsPmsversionresource extends Schema.CollectionType {
  collectionName: 'pmsversionresources';
  info: {
    singularName: 'pmsversionresource';
    pluralName: 'pmsversionresources';
    displayName: 'Pms Version Resource';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    threadName: Attribute.String & Attribute.Required;
    projectName: Attribute.String & Attribute.Required;
    startDate: Attribute.Date & Attribute.Required;
    endDate: Attribute.Date & Attribute.Required;
    employee: Attribute.Relation<
      'plugin::pms.pmsversionresource',
      'manyToOne',
      'api::employee.employee'
    >;
    type: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 20;
      }>;
    versionDls: Attribute.Relation<
      'plugin::pms.pmsversionresource',
      'manyToMany',
      'plugin::pms.pmsversion'
    >;
    billable: Attribute.Enumeration<['true', 'false']> & Attribute.Required;
    feedback: Attribute.Relation<
      'plugin::pms.pmsversionresource',
      'manyToMany',
      'plugin::feedbacks.pmsfeedback'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::pms.pmsversionresource',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::pms.pmsversionresource',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginPmsVersioncommentorrequest
  extends Schema.CollectionType {
  collectionName: 'versioncommentorrequest';
  info: {
    singularName: 'versioncommentorrequest';
    pluralName: 'versioncommentorrequests';
    displayName: 'Version Comment Or Request';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    comments: Attribute.String;
    version: Attribute.Relation<
      'plugin::pms.versioncommentorrequest',
      'manyToOne',
      'plugin::pms.pmsversion'
    >;
    feedback_assesment_form: Attribute.Relation<
      'plugin::pms.versioncommentorrequest',
      'manyToMany',
      'plugin::feedbacks.pmsfeedbackassesmentformformat'
    >;
    isRequestApproved: Attribute.String;
    isRequested: Attribute.Boolean;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::pms.versioncommentorrequest',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::pms.versioncommentorrequest',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginFeedbacksPmsfeedback extends Schema.CollectionType {
  collectionName: 'pmsfeedbacks';
  info: {
    singularName: 'pmsfeedback';
    pluralName: 'pmsfeedbacks';
    displayName: 'PMS Feedbacks';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    feedback: Attribute.JSON;
    feedback_trigger_date: Attribute.Time;
    cooldown_period_date: Attribute.Time;
    versionResource: Attribute.Relation<
      'plugin::feedbacks.pmsfeedback',
      'manyToMany',
      'plugin::pms.pmsversionresource'
    >;
    feedbackAssesmentForm: Attribute.Relation<
      'plugin::feedbacks.pmsfeedback',
      'manyToOne',
      'plugin::feedbacks.pmsfeedbackassesmentformformat'
    >;
    submittedDate: Attribute.DateTime;
    expired: Attribute.Boolean;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::feedbacks.pmsfeedback',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::feedbacks.pmsfeedback',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginFeedbacksPmsfeedbackassesmentformformat
  extends Schema.CollectionType {
  collectionName: 'pmsfeedbackassesmentformformats';
  info: {
    singularName: 'pmsfeedbackassesmentformformat';
    pluralName: 'pmsfeedbackassesmentformformats';
    displayName: 'PMS Feedback Or Assesment Form Format';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  attributes: {
    createdAt: Attribute.DateTime;
    title: Attribute.String;
    description: Attribute.String;
    isAssesment: Attribute.Boolean;
    type: Attribute.String;
    triggerAction: Attribute.String;
    triggerActionPeriod: Attribute.Integer;
    cooldownPriod: Attribute.Integer;
    FormParameters: Attribute.JSON;
    version_comment_requests: Attribute.Relation<
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      'manyToMany',
      'plugin::pms.versioncommentorrequest'
    >;
    pmsFeedbacks: Attribute.Relation<
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      'oneToMany',
      'plugin::feedbacks.pmsfeedback'
    >;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface PluginI18NLocale extends Schema.CollectionType {
  collectionName: 'i18n_locale';
  info: {
    singularName: 'locale';
    pluralName: 'locales';
    collectionName: 'locales';
    displayName: 'Locale';
    description: '';
  };
  options: {
    draftAndPublish: false;
  };
  pluginOptions: {
    'content-manager': {
      visible: false;
    };
    'content-type-builder': {
      visible: false;
    };
  };
  attributes: {
    name: Attribute.String &
      Attribute.SetMinMax<
        {
          min: 1;
          max: 50;
        },
        number
      >;
    code: Attribute.String & Attribute.Unique;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'plugin::i18n.locale',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'plugin::i18n.locale',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiAccountDeletionChecklistAccountDeletionChecklist
  extends Schema.CollectionType {
  collectionName: 'account_deletion_checklists';
  info: {
    singularName: 'account-deletion-checklist';
    pluralName: 'account-deletion-checklists';
    displayName: 'Account Deletion Checklist';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    account: Attribute.String;
    isCompleted: Attribute.Boolean;
    comments: Attribute.Text;
    resignation: Attribute.Relation<
      'api::account-deletion-checklist.account-deletion-checklist',
      'oneToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::account-deletion-checklist.account-deletion-checklist',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::account-deletion-checklist.account-deletion-checklist',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiAssetAsset extends Schema.CollectionType {
  collectionName: 'assets';
  info: {
    singularName: 'asset';
    pluralName: 'assets';
    displayName: 'Assets';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    serialNumber: Attribute.String & Attribute.Required & Attribute.Unique;
    asset_vendor: Attribute.Relation<
      'api::asset.asset',
      'manyToOne',
      'api::asset-vendor.asset-vendor'
    >;
    type: Attribute.Enumeration<
      ['Laptop', 'Mouse', 'Keyboard', 'External haddisk']
    >;
    model: Attribute.String & Attribute.Required;
    configuration: Attribute.Enumeration<['High', 'Medium', 'Low']>;
    memory: Attribute.Decimal;
    ram: Attribute.Decimal;
    processor: Attribute.String;
    workingStatus: Attribute.Enumeration<['Working', 'Not working']>;
    assignedStatus: Attribute.Enumeration<['Not assigned', 'Assigned']> &
      Attribute.DefaultTo<'Not assigned'>;
    remark: Attribute.Text;
    employee: Attribute.Relation<
      'api::asset.asset',
      'oneToOne',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::asset.asset',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::asset.asset',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiAssetHandoverAssetHandover extends Schema.CollectionType {
  collectionName: 'asset_handovers';
  info: {
    singularName: 'asset-handover';
    pluralName: 'asset-handovers';
    displayName: 'Asset Handover';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    method: Attribute.String;
    service: Attribute.String;
    trackingNumber: Attribute.String;
    comments: Attribute.String;
    resignation: Attribute.Relation<
      'api::asset-handover.asset-handover',
      'manyToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::asset-handover.asset-handover',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::asset-handover.asset-handover',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiAssetSubmissionChecklistAssetSubmissionChecklist
  extends Schema.CollectionType {
  collectionName: 'asset_submission_checklists';
  info: {
    singularName: 'asset-submission-checklist';
    pluralName: 'asset-submission-checklists';
    displayName: 'Asset Submission Checklist';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    assest: Attribute.String;
    isCompleted: Attribute.Boolean;
    comments: Attribute.Text;
    resignation: Attribute.Relation<
      'api::asset-submission-checklist.asset-submission-checklist',
      'oneToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::asset-submission-checklist.asset-submission-checklist',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::asset-submission-checklist.asset-submission-checklist',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiAssetVendorAssetVendor extends Schema.CollectionType {
  collectionName: 'asset_vendors';
  info: {
    singularName: 'asset-vendor';
    pluralName: 'asset-vendors';
    displayName: 'Asset Vendor';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String & Attribute.Required & Attribute.Unique;
    contact: Attribute.String;
    email: Attribute.Email;
    address: Attribute.Text;
    assets: Attribute.Relation<
      'api::asset-vendor.asset-vendor',
      'oneToMany',
      'api::asset.asset'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::asset-vendor.asset-vendor',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::asset-vendor.asset-vendor',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiBillableStatusBillableStatus extends Schema.CollectionType {
  collectionName: 'billable_statuses';
  info: {
    singularName: 'billable-status';
    pluralName: 'billable-statuses';
    displayName: 'Billable Status';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::billable-status.billable-status',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::billable-status.billable-status',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiBuzzFeedBuzzFeed extends Schema.CollectionType {
  collectionName: 'buzz_feeds';
  info: {
    singularName: 'buzz-feed';
    pluralName: 'buzz-feeds';
    displayName: 'Buzz Feed';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    feed: Attribute.Text;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::buzz-feed.buzz-feed',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::buzz-feed.buzz-feed',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiChecklistEnumChecklistEnum extends Schema.CollectionType {
  collectionName: 'checklist_enums';
  info: {
    singularName: 'checklist-enum';
    pluralName: 'checklist-enums';
    displayName: 'Checklist Enum';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::checklist-enum.checklist-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::checklist-enum.checklist-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiCountryCountry extends Schema.CollectionType {
  collectionName: 'countries';
  info: {
    singularName: 'country';
    pluralName: 'countries';
    displayName: 'country';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    country: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::country.country',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::country.country',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiDepartmentDepartment extends Schema.CollectionType {
  collectionName: 'departments';
  info: {
    singularName: 'department';
    pluralName: 'departments';
    displayName: 'Department';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    departmentHead: Attribute.Relation<
      'api::department.department',
      'oneToOne',
      'api::employee.employee'
    >;
    exitClearanceApprovalNeeded: Attribute.Boolean;
    exitClearanceApproverId: Attribute.Relation<
      'api::department.department',
      'oneToOne',
      'api::employee.employee'
    >;
    designations: Attribute.Relation<
      'api::department.department',
      'oneToMany',
      'api::designation.designation'
    >;
    projects: Attribute.Relation<
      'api::department.department',
      'oneToMany',
      'api::project.project'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::department.department',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::department.department',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiDesignationDesignation extends Schema.CollectionType {
  collectionName: 'designations';
  info: {
    singularName: 'designation';
    pluralName: 'designations';
    displayName: 'Designation';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    department: Attribute.Relation<
      'api::designation.designation',
      'manyToOne',
      'api::department.department'
    >;
    image: Attribute.Media;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::designation.designation',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::designation.designation',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiDocumentDocument extends Schema.CollectionType {
  collectionName: 'documents';
  info: {
    singularName: 'document';
    pluralName: 'documents';
    displayName: 'Document';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    document: Attribute.Media;
    employee: Attribute.Relation<
      'api::document.document',
      'manyToOne',
      'api::employee.employee'
    >;
    type: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::document.document',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::document.document',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeEmployee extends Schema.CollectionType {
  collectionName: 'employees';
  info: {
    singularName: 'employee';
    pluralName: 'employees';
    displayName: 'Employee';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    firstName: Attribute.String;
    middleName: Attribute.String;
    lastName: Attribute.String;
    status: Attribute.String & Attribute.DefaultTo<'Active'>;
    maritalStatus: Attribute.String;
    gender: Attribute.String;
    bloodGroup: Attribute.String;
    dateOfBirth: Attribute.Date;
    permanentAddress: Attribute.Text;
    correspondenceAddress: Attribute.Text;
    contactNumber: Attribute.String;
    personalEmailId: Attribute.Email;
    alternativeContactNumber: Attribute.String;
    nationality: Attribute.String;
    adharCardNumber: Attribute.String;
    panCardNumber: Attribute.String;
    eppfNo: Attribute.String;
    passport: Attribute.String;
    dateOfJoining: Attribute.Date;
    dateOfLeaving: Attribute.Date;
    employeeType: Attribute.String;
    profilePicture: Attribute.Media;
    isExperienced: Attribute.Boolean;
    employeeEducations: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::employee-education.employee-education'
    >;
    employeeFamilies: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::employee-family.employee-family'
    >;
    documents: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::document.document'
    >;
    employeeBanks: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::employee-bank.employee-bank'
    >;
    designation: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'api::designation.designation'
    >;
    employeeSkills: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::employee-skill.employee-skill'
    >;
    department: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'api::department.department'
    >;
    isTeamLead: Attribute.Boolean;
    employeeExperiences: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::employee-experience.employee-experience'
    >;
    user: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'plugin::users-permissions.user'
    >;
    onboardingStartDate: Attribute.Date;
    trainingPeriodEndDate: Attribute.Date;
    isBillable: Attribute.String;
    uan: Attribute.String;
    form_progress: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'api::form-progress.form-progress'
    >;
    level: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'api::level.level'
    >;
    tl_task_clearances: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::tl-task-clearance.tl-task-clearance'
    >;
    it_asset_clearances: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::it-asset-clearance.it-asset-clearance'
    >;
    finance_clearances: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'api::finance-clearance.finance-clearance'
    >;
    empId: Attribute.String;
    project: Attribute.Relation<
      'api::employee.employee',
      'manyToOne',
      'api::project.project'
    >;
    resignation: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'api::resignation.resignation'
    >;
    pms_version_resources: Attribute.Relation<
      'api::employee.employee',
      'oneToMany',
      'plugin::pms.pmsversionresource'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee.employee',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeAssetEmployeeAsset extends Schema.CollectionType {
  collectionName: 'employee_assets';
  info: {
    singularName: 'employee-asset';
    pluralName: 'employee-assets';
    displayName: 'Employee assets';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    employeeName: Attribute.String;
    serialNumber: Attribute.String;
    model: Attribute.String;
    startDate: Attribute.Date;
    endDate: Attribute.Date;
    status: Attribute.Enumeration<['Active', 'Inactive']> &
      Attribute.DefaultTo<'Active'>;
    employeeId: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-asset.employee-asset',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-asset.employee-asset',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeBankEmployeeBank extends Schema.CollectionType {
  collectionName: 'employee_banks';
  info: {
    singularName: 'employee-bank';
    pluralName: 'employee-banks';
    displayName: 'Employee Bank';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    accountNumber: Attribute.String;
    bankName: Attribute.String;
    ifsc: Attribute.String;
    type: Attribute.String;
    name: Attribute.String;
    accountHolderName: Attribute.String;
    branchName: Attribute.String;
    isDefault: Attribute.Boolean;
    employee: Attribute.Relation<
      'api::employee-bank.employee-bank',
      'manyToOne',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-bank.employee-bank',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-bank.employee-bank',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeBondEmployeeBond extends Schema.CollectionType {
  collectionName: 'employee_bonds';
  info: {
    singularName: 'employee-bond';
    pluralName: 'employee-bonds';
    displayName: 'Employee Bond';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    employee: Attribute.Relation<
      'api::employee-bond.employee-bond',
      'oneToOne',
      'api::employee.employee'
    >;
    name: Attribute.String;
    description: Attribute.Text;
    durationInMonths: Attribute.Integer;
    startDate: Attribute.Date;
    endDate: Attribute.Date;
    document: Attribute.Relation<
      'api::employee-bond.employee-bond',
      'oneToOne',
      'api::document.document'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-bond.employee-bond',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-bond.employee-bond',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeEducationEmployeeEducation
  extends Schema.CollectionType {
  collectionName: 'employee_educations';
  info: {
    singularName: 'employee-education';
    pluralName: 'employee-educations';
    displayName: 'Employee Education';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    employee: Attribute.Relation<
      'api::employee-education.employee-education',
      'manyToOne',
      'api::employee.employee'
    >;
    institute: Attribute.String;
    stream: Attribute.String;
    degree: Attribute.String;
    qualification: Attribute.String;
    marksheetReminder: Attribute.Date;
    passoutYear: Attribute.String;
    grades: Attribute.String;
    document: Attribute.Relation<
      'api::employee-education.employee-education',
      'oneToOne',
      'api::document.document'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-education.employee-education',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-education.employee-education',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeExperienceEmployeeExperience
  extends Schema.CollectionType {
  collectionName: 'employee_experiences';
  info: {
    singularName: 'employee-experience';
    pluralName: 'employee-experiences';
    displayName: 'Employee Experience';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    companyName: Attribute.String;
    designation: Attribute.String;
    startDate: Attribute.Date;
    endDate: Attribute.Date;
    hrContactNumber: Attribute.String;
    hrEmailId: Attribute.Email;
    tlContactNumber: Attribute.String;
    tlEmailId: Attribute.Email;
    employee: Attribute.Relation<
      'api::employee-experience.employee-experience',
      'manyToOne',
      'api::employee.employee'
    >;
    document: Attribute.Relation<
      'api::employee-experience.employee-experience',
      'oneToOne',
      'api::document.document'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-experience.employee-experience',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-experience.employee-experience',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeFamilyEmployeeFamily extends Schema.CollectionType {
  collectionName: 'employee_families';
  info: {
    singularName: 'employee-family';
    pluralName: 'employee-families';
    displayName: 'Employee Family';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    firstName: Attribute.String;
    middleName: Attribute.String;
    lastName: Attribute.String;
    contactNumber: Attribute.String;
    dateOfBirth: Attribute.Date;
    anniversaryDate: Attribute.Date;
    relation: Attribute.String;
    employee: Attribute.Relation<
      'api::employee-family.employee-family',
      'manyToOne',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-family.employee-family',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-family.employee-family',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeGroupClearanceEmployeeGroupClearance
  extends Schema.CollectionType {
  collectionName: 'employee_group_clearances';
  info: {
    singularName: 'employee-group-clearance';
    pluralName: 'employee-group-clearances';
    displayName: 'Employee Group Clearance';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    groupName: Attribute.String;
    groupAdmin: Attribute.String;
    resignation: Attribute.Relation<
      'api::employee-group-clearance.employee-group-clearance',
      'manyToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-group-clearance.employee-group-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-group-clearance.employee-group-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeResignationClearanceEmployeeResignationClearance
  extends Schema.CollectionType {
  collectionName: 'employee_resignation_clearances';
  info: {
    singularName: 'employee-resignation-clearance';
    pluralName: 'employee-resignation-clearances';
    displayName: 'Employee Resignation Clearance';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    resourceUsername: Attribute.String;
    resourcePassword: Attribute.String;
    type: Attribute.String;
    resignation: Attribute.Relation<
      'api::employee-resignation-clearance.employee-resignation-clearance',
      'manyToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-resignation-clearance.employee-resignation-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-resignation-clearance.employee-resignation-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiEmployeeSkillEmployeeSkill extends Schema.CollectionType {
  collectionName: 'employee_skills';
  info: {
    singularName: 'employee-skill';
    pluralName: 'employee-skills';
    displayName: 'Employee Skill';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    rating: Attribute.Integer;
    reviewer: Attribute.Integer;
    certificate: Attribute.Media;
    remark: Attribute.Text;
    skill: Attribute.Relation<
      'api::employee-skill.employee-skill',
      'manyToOne',
      'api::skill.skill'
    >;
    employee: Attribute.Relation<
      'api::employee-skill.employee-skill',
      'manyToOne',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::employee-skill.employee-skill',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::employee-skill.employee-skill',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiExitFormChecklistExitFormChecklist
  extends Schema.CollectionType {
  collectionName: 'exit_form_checklists';
  info: {
    singularName: 'exit-form-checklist';
    pluralName: 'exit-form-checklists';
    displayName: 'Exit Form Checklist';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.String;
    isCompleted: Attribute.Boolean;
    comments: Attribute.Text;
    resignation: Attribute.Relation<
      'api::exit-form-checklist.exit-form-checklist',
      'oneToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::exit-form-checklist.exit-form-checklist',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::exit-form-checklist.exit-form-checklist',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiFeedbackFeedback extends Schema.CollectionType {
  collectionName: 'feedbacks';
  info: {
    singularName: 'feedback';
    pluralName: 'feedbacks';
    displayName: 'feedback';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    form_id: Attribute.Integer;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::feedback.feedback',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::feedback.feedback',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiFinanceClearanceFinanceClearance
  extends Schema.CollectionType {
  collectionName: 'finance_clearances';
  info: {
    singularName: 'finance-clearance';
    pluralName: 'finance-clearances';
    displayName: 'Finance Clearance';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.String;
    status: Attribute.String;
    comments: Attribute.String;
    employee: Attribute.Relation<
      'api::finance-clearance.finance-clearance',
      'manyToOne',
      'api::employee.employee'
    >;
    type: Attribute.String;
    resignation: Attribute.Relation<
      'api::finance-clearance.finance-clearance',
      'manyToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::finance-clearance.finance-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::finance-clearance.finance-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiFormProgressFormProgress extends Schema.CollectionType {
  collectionName: 'form_progresses';
  info: {
    singularName: 'form-progress';
    pluralName: 'form-progresses';
    displayName: 'Form Progress';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    basic: Attribute.String;
    professional: Attribute.String;
    education: Attribute.String;
    family: Attribute.String;
    work: Attribute.String;
    official: Attribute.String;
    employee: Attribute.Relation<
      'api::form-progress.form-progress',
      'oneToOne',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::form-progress.form-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::form-progress.form-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiItAssetClearanceItAssetClearance
  extends Schema.CollectionType {
  collectionName: 'it_asset_clearances';
  info: {
    singularName: 'it-asset-clearance';
    pluralName: 'it-asset-clearances';
    displayName: 'IT Asset Clearance';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    asset: Attribute.String;
    status: Attribute.String;
    employee: Attribute.Relation<
      'api::it-asset-clearance.it-asset-clearance',
      'manyToOne',
      'api::employee.employee'
    >;
    resignation: Attribute.Relation<
      'api::it-asset-clearance.it-asset-clearance',
      'manyToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::it-asset-clearance.it-asset-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::it-asset-clearance.it-asset-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiLevelLevel extends Schema.CollectionType {
  collectionName: 'levels';
  info: {
    singularName: 'level';
    pluralName: 'levels';
    displayName: 'Level';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    rank: Attribute.Integer;
    image: Attribute.Media;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::level.level',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::level.level',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiProjectProject extends Schema.CollectionType {
  collectionName: 'projects';
  info: {
    singularName: 'project';
    pluralName: 'projects';
    displayName: 'Project';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    description: Attribute.Text;
    clientName: Attribute.String;
    region: Attribute.Enumeration<
      ['Asia', 'Asia Pacific', 'Europe', 'North America', 'South America']
    >;
    country: Attribute.String;
    department: Attribute.Relation<
      'api::project.project',
      'manyToOne',
      'api::department.department'
    >;
    threads: Attribute.Relation<
      'api::project.project',
      'oneToMany',
      'api::thread.thread'
    >;
    projectManager: Attribute.Relation<
      'api::project.project',
      'oneToOne',
      'api::employee.employee'
    >;
    employees: Attribute.Relation<
      'api::project.project',
      'oneToMany',
      'api::employee.employee'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::project.project',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::project.project',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiQualificationListQualificationList
  extends Schema.CollectionType {
  collectionName: 'qualification_lists';
  info: {
    singularName: 'qualification-list';
    pluralName: 'qualification-lists';
    displayName: 'Qualification List';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::qualification-list.qualification-list',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::qualification-list.qualification-list',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiReasonsEnumReasonsEnum extends Schema.CollectionType {
  collectionName: 'reasons_enums';
  info: {
    singularName: 'reasons-enum';
    pluralName: 'reasons-enums';
    displayName: 'Reasons Enum';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    reason: Attribute.Text;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::reasons-enum.reasons-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::reasons-enum.reasons-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiRegionRegion extends Schema.CollectionType {
  collectionName: 'regions';
  info: {
    singularName: 'region';
    pluralName: 'regions';
    displayName: 'region';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    region: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::region.region',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::region.region',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationResignation extends Schema.CollectionType {
  collectionName: 'resignations';
  info: {
    singularName: 'resignation';
    pluralName: 'resignations';
    displayName: 'Resignation';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    description: Attribute.Text;
    resignationDate: Attribute.Date;
    approvedDate: Attribute.Date;
    exitDate: Attribute.Date;
    isRequested: Attribute.Boolean;
    actualReason: Attribute.Text;
    isRejected: Attribute.Boolean;
    employee: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::employee.employee'
    >;
    employee_resignation_clearances: Attribute.Relation<
      'api::resignation.resignation',
      'oneToMany',
      'api::employee-resignation-clearance.employee-resignation-clearance'
    >;
    employee_group_clearances: Attribute.Relation<
      'api::resignation.resignation',
      'oneToMany',
      'api::employee-group-clearance.employee-group-clearance'
    >;
    tl_task_clearances: Attribute.Relation<
      'api::resignation.resignation',
      'oneToMany',
      'api::tl-task-clearance.tl-task-clearance'
    >;
    it_asset_clearances: Attribute.Relation<
      'api::resignation.resignation',
      'oneToMany',
      'api::it-asset-clearance.it-asset-clearance'
    >;
    itAssetClearanceComment: Attribute.Text;
    finance_clearances: Attribute.Relation<
      'api::resignation.resignation',
      'oneToMany',
      'api::finance-clearance.finance-clearance'
    >;
    financeClearanceComment: Attribute.Text;
    asset_handovers: Attribute.Relation<
      'api::resignation.resignation',
      'oneToMany',
      'api::asset-handover.asset-handover'
    >;
    exit_form_checklist: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::exit-form-checklist.exit-form-checklist'
    >;
    asset_submission_checklist: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::asset-submission-checklist.asset-submission-checklist'
    >;
    account_deletion_checklist: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::account-deletion-checklist.account-deletion-checklist'
    >;
    resignation_exit_clearance_progress: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::resignation-exit-clearance-progress.resignation-exit-clearance-progress'
    >;
    resignation_nda_progress: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::resignation-nda-progress.resignation-nda-progress'
    >;
    resignation_experience_letter_progress: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::resignation-experience-letter-progress.resignation-experience-letter-progress'
    >;
    resignation_checklist_progress: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'api::resignation-checklist-progress.resignation-checklist-progress'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation.resignation',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationChecklistAccountEnumResignationChecklistAccountEnum
  extends Schema.CollectionType {
  collectionName: 'resignation_checklist_account_enums';
  info: {
    singularName: 'resignation-checklist-account-enum';
    pluralName: 'resignation-checklist-account-enums';
    displayName: 'Resignation Checklist Account Enum';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.Text;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-checklist-account-enum.resignation-checklist-account-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-checklist-account-enum.resignation-checklist-account-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationChecklistAssetEnumResignationChecklistAssetEnum
  extends Schema.CollectionType {
  collectionName: 'resignation_checklist_asset_enums';
  info: {
    singularName: 'resignation-checklist-asset-enum';
    pluralName: 'resignation-checklist-asset-enums';
    displayName: 'Resignation Checklist Asset Enum';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.Text;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-checklist-asset-enum.resignation-checklist-asset-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-checklist-asset-enum.resignation-checklist-asset-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationChecklistExitFormEnumResignationChecklistExitFormEnum
  extends Schema.CollectionType {
  collectionName: 'resignation_checklist_exit_form_enums';
  info: {
    singularName: 'resignation-checklist-exit-form-enum';
    pluralName: 'resignation-checklist-exit-form-enums';
    displayName: 'Resignation Checklist Exit Form Enum';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.Text;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-checklist-exit-form-enum.resignation-checklist-exit-form-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-checklist-exit-form-enum.resignation-checklist-exit-form-enum',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationChecklistProgressResignationChecklistProgress
  extends Schema.CollectionType {
  collectionName: 'resignation_checklist_progresses';
  info: {
    singularName: 'resignation-checklist-progress';
    pluralName: 'resignation-checklist-progresses';
    displayName: 'Resignation Checklist Progress';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    exitFormProgress: Attribute.String;
    assetSubmissionProgress: Attribute.String;
    accountDeletionProgress: Attribute.String;
    resignation: Attribute.Relation<
      'api::resignation-checklist-progress.resignation-checklist-progress',
      'oneToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-checklist-progress.resignation-checklist-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-checklist-progress.resignation-checklist-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationExitClearanceProgressResignationExitClearanceProgress
  extends Schema.CollectionType {
  collectionName: 'resignation_exit_clearance_progresses';
  info: {
    singularName: 'resignation-exit-clearance-progress';
    pluralName: 'resignation-exit-clearance-progresses';
    displayName: 'Resignation Exit Clearance Progress';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    employeeClearanceProgress: Attribute.String;
    tlClearanceProgress: Attribute.String;
    itClearanceProgress: Attribute.String;
    financeClearanceProgress: Attribute.String;
    employeeClearanceVerified: Attribute.Boolean;
    tlClearanceVerified: Attribute.Boolean;
    itClearanceVerified: Attribute.Boolean;
    financeClearanceVerified: Attribute.Boolean;
    resignation: Attribute.Relation<
      'api::resignation-exit-clearance-progress.resignation-exit-clearance-progress',
      'oneToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-exit-clearance-progress.resignation-exit-clearance-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-exit-clearance-progress.resignation-exit-clearance-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationExperienceLetterProgressResignationExperienceLetterProgress
  extends Schema.CollectionType {
  collectionName: 'resignation_experience_letter_progresses';
  info: {
    singularName: 'resignation-experience-letter-progress';
    pluralName: 'resignation-experience-letter-progresses';
    displayName: 'Resignation Experience Letter Progress';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    progress: Attribute.String;
    scheduledDate: Attribute.Date;
    holdOutReason: Attribute.Text;
    personalEmailId: Attribute.Email;
    resignation: Attribute.Relation<
      'api::resignation-experience-letter-progress.resignation-experience-letter-progress',
      'oneToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-experience-letter-progress.resignation-experience-letter-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-experience-letter-progress.resignation-experience-letter-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiResignationNdaProgressResignationNdaProgress
  extends Schema.CollectionType {
  collectionName: 'resignation_nda_progresses';
  info: {
    singularName: 'resignation-nda-progress';
    pluralName: 'resignation-nda-progresses';
    displayName: 'Resignation NDA Progress';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    hrProgress: Attribute.String;
    employeeProgress: Attribute.String;
    scheduledDate: Attribute.Date;
    isVerified: Attribute.Boolean;
    resignation: Attribute.Relation<
      'api::resignation-nda-progress.resignation-nda-progress',
      'oneToOne',
      'api::resignation.resignation'
    >;
    isSentbyEmployee: Attribute.Boolean;
    isSentbyHR: Attribute.Boolean;
    isScheduled: Attribute.Boolean;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::resignation-nda-progress.resignation-nda-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::resignation-nda-progress.resignation-nda-progress',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiRolesScreenRolesScreen extends Schema.CollectionType {
  collectionName: 'roles_screens';
  info: {
    singularName: 'roles-screen';
    pluralName: 'roles-screens';
    displayName: 'Roles Screens';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    role: Attribute.Relation<
      'api::roles-screen.roles-screen',
      'manyToOne',
      'plugin::users-permissions.role'
    >;
    screens: Attribute.Relation<
      'api::roles-screen.roles-screen',
      'oneToMany',
      'api::screen.screen'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::roles-screen.roles-screen',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::roles-screen.roles-screen',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiScreenScreen extends Schema.CollectionType {
  collectionName: 'screens';
  info: {
    singularName: 'screen';
    pluralName: 'screens';
    displayName: 'Screens';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String & Attribute.Required & Attribute.Unique;
    url: Attribute.String;
    logo: Attribute.Media & Attribute.Required;
    subScreens: Attribute.JSON;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::screen.screen',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::screen.screen',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiShiftShift extends Schema.CollectionType {
  collectionName: 'shifts';
  info: {
    singularName: 'shift';
    pluralName: 'shifts';
    displayName: 'Shift';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    days: Attribute.String;
    holidays: Attribute.String;
    startTime: Attribute.Time;
    endTime: Attribute.Time;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::shift.shift',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::shift.shift',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiSkillSkill extends Schema.CollectionType {
  collectionName: 'skills';
  info: {
    singularName: 'skill';
    pluralName: 'skills';
    displayName: 'Skill';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    description: Attribute.Text;
    employeeSkills: Attribute.Relation<
      'api::skill.skill',
      'oneToMany',
      'api::employee-skill.employee-skill'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::skill.skill',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::skill.skill',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiTechStackTechStack extends Schema.CollectionType {
  collectionName: 'tech_stacks';
  info: {
    singularName: 'tech-stack';
    pluralName: 'tech-stacks';
    displayName: 'Tech Stack';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    techStack: Attribute.String;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::tech-stack.tech-stack',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::tech-stack.tech-stack',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiThreadThread extends Schema.CollectionType {
  collectionName: 'threads';
  info: {
    singularName: 'thread';
    pluralName: 'threads';
    displayName: 'Thread';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    name: Attribute.String;
    description: Attribute.String;
    domain: Attribute.Enumeration<
      ['Automobile', 'Private Equity', 'Media', 'Sales']
    >;
    lead: Attribute.Relation<
      'api::thread.thread',
      'oneToOne',
      'api::employee.employee'
    >;
    project: Attribute.Relation<
      'api::thread.thread',
      'manyToOne',
      'api::project.project'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::thread.thread',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::thread.thread',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiTlTaskClearanceTlTaskClearance
  extends Schema.CollectionType {
  collectionName: 'tl_task_clearances';
  info: {
    singularName: 'tl-task-clearance';
    pluralName: 'tl-task-clearances';
    displayName: 'TL Task Clearance';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    task: Attribute.String;
    status: Attribute.String;
    comments: Attribute.Text;
    employee: Attribute.Relation<
      'api::tl-task-clearance.tl-task-clearance',
      'manyToOne',
      'api::employee.employee'
    >;
    resignation: Attribute.Relation<
      'api::tl-task-clearance.tl-task-clearance',
      'manyToOne',
      'api::resignation.resignation'
    >;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::tl-task-clearance.tl-task-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::tl-task-clearance.tl-task-clearance',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiVersionCommentOrRequestVersionCommentOrRequest
  extends Schema.CollectionType {
  collectionName: 'version_comment_or_requests';
  info: {
    singularName: 'version-comment-or-request';
    pluralName: 'version-comment-or-requests';
    displayName: 'Version Comment Or Request';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    reqId: Attribute.Integer;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::version-comment-or-request.version-comment-or-request',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::version-comment-or-request.version-comment-or-request',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiVersionDetailVersionDetail extends Schema.CollectionType {
  collectionName: 'version_details';
  info: {
    singularName: 'version-detail';
    pluralName: 'version-details';
    displayName: 'version-detail';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    version_number: Attribute.Integer;
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::version-detail.version-detail',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::version-detail.version-detail',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

export interface ApiVersionResourceVersionResource
  extends Schema.CollectionType {
  collectionName: 'version_resources';
  info: {
    singularName: 'version-resource';
    pluralName: 'version-resources';
    displayName: 'version-resources';
    description: '';
  };
  options: {
    draftAndPublish: true;
  };
  attributes: {
    createdAt: Attribute.DateTime;
    updatedAt: Attribute.DateTime;
    publishedAt: Attribute.DateTime;
    createdBy: Attribute.Relation<
      'api::version-resource.version-resource',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
    updatedBy: Attribute.Relation<
      'api::version-resource.version-resource',
      'oneToOne',
      'admin::user'
    > &
      Attribute.Private;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface ContentTypes {
      'admin::permission': AdminPermission;
      'admin::user': AdminUser;
      'admin::role': AdminRole;
      'admin::api-token': AdminApiToken;
      'admin::api-token-permission': AdminApiTokenPermission;
      'admin::transfer-token': AdminTransferToken;
      'admin::transfer-token-permission': AdminTransferTokenPermission;
      'plugin::upload.file': PluginUploadFile;
      'plugin::upload.folder': PluginUploadFolder;
      'plugin::content-releases.release': PluginContentReleasesRelease;
      'plugin::content-releases.release-action': PluginContentReleasesReleaseAction;
      'plugin::users-permissions.permission': PluginUsersPermissionsPermission;
      'plugin::users-permissions.role': PluginUsersPermissionsRole;
      'plugin::users-permissions.user': PluginUsersPermissionsUser;
      'plugin::pms.pmsproject': PluginPmsPmsproject;
      'plugin::pms.pmsthread': PluginPmsPmsthread;
      'plugin::pms.pmsversion': PluginPmsPmsversion;
      'plugin::pms.pmsversionresource': PluginPmsPmsversionresource;
      'plugin::pms.versioncommentorrequest': PluginPmsVersioncommentorrequest;
      'plugin::feedbacks.pmsfeedback': PluginFeedbacksPmsfeedback;
      'plugin::feedbacks.pmsfeedbackassesmentformformat': PluginFeedbacksPmsfeedbackassesmentformformat;
      'plugin::i18n.locale': PluginI18NLocale;
      'api::account-deletion-checklist.account-deletion-checklist': ApiAccountDeletionChecklistAccountDeletionChecklist;
      'api::asset.asset': ApiAssetAsset;
      'api::asset-handover.asset-handover': ApiAssetHandoverAssetHandover;
      'api::asset-submission-checklist.asset-submission-checklist': ApiAssetSubmissionChecklistAssetSubmissionChecklist;
      'api::asset-vendor.asset-vendor': ApiAssetVendorAssetVendor;
      'api::billable-status.billable-status': ApiBillableStatusBillableStatus;
      'api::buzz-feed.buzz-feed': ApiBuzzFeedBuzzFeed;
      'api::checklist-enum.checklist-enum': ApiChecklistEnumChecklistEnum;
      'api::country.country': ApiCountryCountry;
      'api::department.department': ApiDepartmentDepartment;
      'api::designation.designation': ApiDesignationDesignation;
      'api::document.document': ApiDocumentDocument;
      'api::employee.employee': ApiEmployeeEmployee;
      'api::employee-asset.employee-asset': ApiEmployeeAssetEmployeeAsset;
      'api::employee-bank.employee-bank': ApiEmployeeBankEmployeeBank;
      'api::employee-bond.employee-bond': ApiEmployeeBondEmployeeBond;
      'api::employee-education.employee-education': ApiEmployeeEducationEmployeeEducation;
      'api::employee-experience.employee-experience': ApiEmployeeExperienceEmployeeExperience;
      'api::employee-family.employee-family': ApiEmployeeFamilyEmployeeFamily;
      'api::employee-group-clearance.employee-group-clearance': ApiEmployeeGroupClearanceEmployeeGroupClearance;
      'api::employee-resignation-clearance.employee-resignation-clearance': ApiEmployeeResignationClearanceEmployeeResignationClearance;
      'api::employee-skill.employee-skill': ApiEmployeeSkillEmployeeSkill;
      'api::exit-form-checklist.exit-form-checklist': ApiExitFormChecklistExitFormChecklist;
      'api::feedback.feedback': ApiFeedbackFeedback;
      'api::finance-clearance.finance-clearance': ApiFinanceClearanceFinanceClearance;
      'api::form-progress.form-progress': ApiFormProgressFormProgress;
      'api::it-asset-clearance.it-asset-clearance': ApiItAssetClearanceItAssetClearance;
      'api::level.level': ApiLevelLevel;
      'api::project.project': ApiProjectProject;
      'api::qualification-list.qualification-list': ApiQualificationListQualificationList;
      'api::reasons-enum.reasons-enum': ApiReasonsEnumReasonsEnum;
      'api::region.region': ApiRegionRegion;
      'api::resignation.resignation': ApiResignationResignation;
      'api::resignation-checklist-account-enum.resignation-checklist-account-enum': ApiResignationChecklistAccountEnumResignationChecklistAccountEnum;
      'api::resignation-checklist-asset-enum.resignation-checklist-asset-enum': ApiResignationChecklistAssetEnumResignationChecklistAssetEnum;
      'api::resignation-checklist-exit-form-enum.resignation-checklist-exit-form-enum': ApiResignationChecklistExitFormEnumResignationChecklistExitFormEnum;
      'api::resignation-checklist-progress.resignation-checklist-progress': ApiResignationChecklistProgressResignationChecklistProgress;
      'api::resignation-exit-clearance-progress.resignation-exit-clearance-progress': ApiResignationExitClearanceProgressResignationExitClearanceProgress;
      'api::resignation-experience-letter-progress.resignation-experience-letter-progress': ApiResignationExperienceLetterProgressResignationExperienceLetterProgress;
      'api::resignation-nda-progress.resignation-nda-progress': ApiResignationNdaProgressResignationNdaProgress;
      'api::roles-screen.roles-screen': ApiRolesScreenRolesScreen;
      'api::screen.screen': ApiScreenScreen;
      'api::shift.shift': ApiShiftShift;
      'api::skill.skill': ApiSkillSkill;
      'api::tech-stack.tech-stack': ApiTechStackTechStack;
      'api::thread.thread': ApiThreadThread;
      'api::tl-task-clearance.tl-task-clearance': ApiTlTaskClearanceTlTaskClearance;
      'api::version-comment-or-request.version-comment-or-request': ApiVersionCommentOrRequestVersionCommentOrRequest;
      'api::version-detail.version-detail': ApiVersionDetailVersionDetail;
      'api::version-resource.version-resource': ApiVersionResourceVersionResource;
    }
  }
}
