# Stage 1: Build
FROM alpine:3.19 AS build

#USE 18.19.0 for ERR_INTERNAL_ASSERTION issue on 20.11.0 STABLE version
ENV NODE_VERSION 18.19.0

RUN apk add --no-cache nodejs yarn

WORKDIR /usr/src/sankey-hrms-backend

COPY ./package.json ./yarn.lock ./

RUN yarn config set network-timeout 600000 -g && yarn install --production

COPY . .

RUN yarn build

# Stage 2: Production
FROM alpine:3.19

# Install Node.js, Yarn, LibreOffice, and Microsoft TrueType core fonts
RUN apk add --no-cache nodejs yarn && \
    apk add --no-cache libreoffice && \
    apk add --no-cache msttcorefonts-installer fontconfig && \
    update-ms-fonts && \
    fc-cache -f

WORKDIR /usr/src/sankey-hrms-backend

COPY --from=build /usr/src/sankey-hrms-backend ./

EXPOSE 1337

CMD ["yarn", "develop"]