'use strict';

module.exports = ({ strapi }) => ({
  async findAll(query) {
    return await strapi.entityService.findMany(
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      query
    );
  },
  async findOne(id) {
    return await strapi.entityService.findOne(
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      id
    );
  },

  async delete(id) {
    return await strapi.entityService.delete(
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      id
    );
  },

  async create(data) {
    const type = data.type;
    const entry = await strapi.db
      .query('plugin::feedbacks.pmsfeedbackassesmentformformat')
      .findOne({ where: { type: type } });

    if (entry) {
      return 'data already present for the given type';
    } else {
      // Create new data if not found
      return await strapi
        .query('plugin::feedbacks.pmsfeedbackassesmentformformat')
        .create({ data: data });
    }
  },

  async update(id, data) {
    return await strapi.entityService.update(
      'plugin::feedbacks.pmsfeedbackassesmentformformat',
      id,
      { data: data }
    );
  },
});