'use strict';

module.exports = ({ strapi }) => ({
  async findAll(query) {
    query.populate = {
      versionResource: {
        populate: ['employee', 'versionDls']
      },
      feedbackAssesmentForm: {
        populate: 'version_comment_requests'
      }
    };

    // Continue with the findMany call
    const res = await strapi.entityService.findMany(
      'plugin::feedbacks.pmsfeedback',
      query
    );

    const result = res.map(data => {
      return {
        feedbackId: data.id,
        resourceId: data.versionResource[0].id,
        resourceName:
          data.versionResource[0].employee.firstName +
          ' ' +
          data.versionResource[0].employee.lastName,
        employeeId: data.versionResource[0].employee.empId,
        projectName: data.versionResource[0].projectName,
        threadName: data.versionResource[0].threadName,
        versionName: data.versionResource[0].versionDls[0].versionName,
        typeOfFeedback: data.feedbackAssesmentForm.type,
        isAssesment: data.feedbackAssesmentForm.isAssesment,
        feedbackDescription: data.feedbackAssesmentForm.description,
        createdAt: data.feedbackAssesmentForm.createdAt
      };
    });
    return result;
  },

  async findOne(id) {
    const data = await strapi.entityService.findOne(
      'plugin::feedbacks.pmsfeedback',
      id,
      {
        populate: {
          versionResource: {
            populate: ['employee', 'versionDls']
          },
          feedbackAssesmentForm: {
            populate: 'version_comment_requests'
          }
        }
      }
    );
    const result = {
      feedbackId: data.id,
      feedbackTitle: data.feedbackAssesmentForm.title,
      resourceId: data.versionResource[0].id,
      resourceName:
        data.versionResource[0].employee.firstName +
        ' ' +
        data.versionResource[0].employee.lastName,
      employeeId: data.versionResource[0].employee.empId,
      projectName: data.versionResource[0].projectName,
      threadName: data.versionResource[0].threadName,
      versionName: data.versionResource[0].versionDls[0].versionName,
      versionId: data.versionResource[0].versionDls[0].id,
      typeOfFeedback: data.feedbackAssesmentForm.type,
      isAssesment: data.feedbackAssesmentForm.isAssesment,
      feedbackDescription: data.feedbackAssesmentForm.description,
      formParameters: data.feedbackAssesmentForm.FormParameters,
      versionIsDelayed: data.versionResource[0].versionDls[0].isDelayed,
      versionDescription: data.versionResource[0].versionDls[0].description,
      createdAt: data.feedbackAssesmentForm.createdAt
    };
    return result;
  },

  async delete(id) {
    return await strapi.entityService.delete(
      'plugin::feedbacks.pmsfeedback',
      id
    );
  },

  async create(data) {
    return await strapi.entityService.create('plugin::feedbacks.pmsfeedback', {
      data: data
    });
  },

  async update(id, data) {
    return await strapi.entityService.update(
      'plugin::feedbacks.pmsfeedback',
      id,
      { data: data }
    );
  }
});
