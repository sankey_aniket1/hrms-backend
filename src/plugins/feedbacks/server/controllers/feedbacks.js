'use strict';

module.exports = {
  async findOne(ctx) {
    try {
      return await strapi
        .plugin('feedbacks')
        .service('feedbacks')
        .findOne(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async findAll(ctx) {
    try {
      return await strapi
        .plugin('feedbacks')
        .service('feedbacks')
        .findAll(ctx.query);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async create(ctx) {
    try {
      ctx.body = await strapi
        .plugin('feedbacks')
        .service('feedbacks')
        .create(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async update(ctx) {
    try {
      ctx.body = await strapi
        .plugin('feedbacks')
        .service('feedbacks')
        .update(ctx.params.id, ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
};
