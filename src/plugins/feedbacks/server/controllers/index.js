'use strict';

const feedbacks = require('./feedbacks');
const pmsfeedbackassesmentformformat = require('./pmsfeedbackassesmentformformat');
module.exports = {
  feedbacks,
  pmsfeedbackassesmentformformat,
};
