"use strict";

module.exports = {
  async findOne(ctx) {
    try {
      return await strapi.plugin("feedbacks").service("pmsfeedbackassesmentformformat").findOne(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  // async find(ctx) {
  //   try {
  //     return await strapi.plugin("feedbacks").service("pmsfeedbackassesmentformformat").find(ctx.query);
  //   } catch (err) {
  //     ctx.throw(500, err);
  //   }
  // },

  async findAll(ctx) {
    try {
      return await strapi.plugin("feedbacks").service("pmsfeedbackassesmentformformat").findAll(ctx.query);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async delete(ctx) {
    try {
      ctx.body = await strapi
        .plugin("feedbacks")
        .service("pmsfeedbackassesmentformformat")
        .delete(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async create(ctx) {
    try {
      ctx.body = await strapi
        .plugin("feedbacks")
        .service("pmsfeedbackassesmentformformat")
        .create(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async update(ctx) {
    try {
      ctx.body = await strapi
        .plugin("feedbacks")
        .service("pmsfeedbackassesmentformformat")
        .update(ctx.params.id, ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
};
