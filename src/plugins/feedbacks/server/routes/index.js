module.exports = [
  // for Feedback
  {
    method: 'GET',
    path: '/feedback/findAll',
    handler: 'feedbacks.findAll',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'GET',
    path: '/feedback/findone/:id',
    handler: 'feedbacks.findOne',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'POST',
    path: '/feedback/create',
    handler: 'feedbacks.create',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'PUT',
    path: '/feedback/update/:id',
    handler: 'feedbacks.update',
    config: {
      policies: [],
      auth: false,
    },
  },

  // for Feedback Format
  {
    method: 'GET',
    path: '/feedback-format/findAll',
    handler: 'pmsfeedbackassesmentformformat.findAll',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'GET',
    path: '/feedback-format/findone/:id',
    handler: 'pmsfeedbackassesmentformformat.findOne',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'POST',
    path: '/feedback-format/create',
    handler: 'pmsfeedbackassesmentformformat.create',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'DELETE',
    path: '/feedback-format/delete/:id',
    handler: 'pmsfeedbackassesmentformformat.delete',
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: 'PUT',
    path: '/feedback-format/update/:id',
    handler: 'pmsfeedbackassesmentformformat.update',
    config: {
      policies: [],
      auth: false,
    },
  },
];
