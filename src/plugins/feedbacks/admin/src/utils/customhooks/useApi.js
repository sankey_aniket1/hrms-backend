import { useState, useEffect, useCallback } from 'react';

const useApi = (url, initialOptions = {}) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [options, setOptions] = useState(initialOptions);

  const fetchData = useCallback(async () => {
    setLoading(true);
    setError(null);
    try {
      const response = await fetch(url, options);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const result = await response.json();
      setData(result);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  }, [url, options]);

  const doFetch = (newOptions = {}) => {
    setOptions((prevOptions) => ({
      ...prevOptions,
      ...newOptions,
    }));
  };

  useEffect(() => {
    if (options.method === 'GET') {
      fetchData();
    }
  }, [fetchData, options]);

  return { data, loading, error, doFetch };
};

export default useApi;
