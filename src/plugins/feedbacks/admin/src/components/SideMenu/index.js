import React, { useState } from 'react';
import { Sidebar, Menu, MenuItem } from 'react-pro-sidebar';
import { BsThreads } from 'react-icons/bs';
import { NavLink } from 'react-router-dom'; // Changed from Link to NavLink
import pluginId from '../../pluginId';
import { FcTreeStructure } from 'react-icons/fc';
import './index.css';

const SideMenu = () => {
  const [projectActive, setProjectActive] = useState(true);

  return (
    <>
      <div id="header">
        <Sidebar>
          <Menu>
            <NavLink
              to={`/plugins/${pluginId}`}
              activeClassName={projectActive ? 'activated' : ''}
            >
              <MenuItem
                icon={<FcTreeStructure />}
                onClick={() => setProjectActive(true)}
              >
                Feedback
              </MenuItem>
            </NavLink>
            <NavLink
              to={`/plugins/${pluginId}/feedback-formats`}
              activeClassName="activated"
            >
              <MenuItem
                icon={<BsThreads />}
                onClick={() => setProjectActive(false)}
              >
                Feedback Formats
              </MenuItem>
            </NavLink>
          </Menu>
        </Sidebar>
      </div>
    </>
  );
};

export default SideMenu;
