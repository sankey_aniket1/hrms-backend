import React,{useState} from 'react'
import { Button } from '@strapi/design-system/Button';
import Plus from '@strapi/icons/Plus';
import { Box, Link } from '@strapi/design-system';

export default function index({addSection}) {
  const [formAttribute,setFormAttribute]=useState([]);
  return (
    <div>
      <div
          style={{ display: 'flex', justifyContent: 'end', margin: '10px 0' }}
        >
          
          <Button
            startIcon={<Plus />}
            padding={4}
            background="#999"
            onClick={() => addSection()}
          >
            Add Section
          </Button>
        </div>
    </div>
  )
}
