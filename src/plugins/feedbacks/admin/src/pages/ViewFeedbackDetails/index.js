import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  Card,
  CardBody,
  Textarea,
  HeaderLayout,
  RadioGroup,
  Radio,
  TextInput,
} from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import { Link } from '@strapi/design-system';
import './index.css';
require('dotenv').config();

const ViewFeedbackDetails = () => {
  const { id } = useParams();
  const [feedbackDetails, setFeedbackDetails] = useState([]);
  const [formData, setFormData] = useState([]);


  const fetchTableData = async() =>{
    console.log('value--------->',id)
    try {
      const response = await fetch(`${process.env.URL}/feedbacks/feedback/findone/${parseInt(id)}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      console.log(data);
      setFeedbackDetails(data);
      setFormData(data.formParameters)

    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchTableData()
  }, [id]);

  if (!feedbackDetails) {
    return <div>Loading...</div>;
  }

  const renderFormParameter = (param) => {
    return param.fields.map((parameter, index) => {
      switch (parameter.type) {
        case 'text':
          return (
            <div key={index} style={{ marginTop: '20px', width: '200%' }}>
              <label>
                {index + 1}. {parameter.question}
              </label>
              <TextInput
                // @ts-ignore
                label={' '}
                id={index}
                placeholder=""
                onChange={() => {}}
                disabled
              />
            </div>
          );
        case 'textarea':
          return (
            <div style={{ marginTop: '20px', width: '200%' }}>
              <label>
                {index + 1}. {parameter.question}
              </label>
              <Textarea
                key={index}
                name={parameter.title}
                id={parameter.title}
                value={parameter.value}
                disabled
              />
            </div>
          );
        case 'radio':
          return (
            <div key={index} style={{ marginTop: '20px' }}>
              <label>
                {index + 1}. {parameter.question}
              </label>
              <RadioGroup
                name={parameter.question}
                value={
                  parameter.options.find((option) => option.selected)?.question
                }
              >
                {parameter.options.map((option, optionIndex) => (
                  <Radio
                    key={optionIndex}
                    name={parameter.title}
                    value={option.title}
                    checked={option.selected}
                    disabled
                  >
                    {option.title}
                  </Radio>
                ))}
              </RadioGroup>
            </div>
          );
        case 'checkbox':
            return (
              <div key={index} style={{ marginTop: '20px' }}>
                <label>
                  {index + 1}. {parameter.question}
                </label>
                <RadioGroup
                  name={parameter.question}
                  value={
                    parameter.options.find((option) => option.selected)?.question
                  }
                >
                  {parameter.options.map((option, optionIndex) => (
                    <Radio
                      key={optionIndex}
                      name={parameter.title}
                      value={option.title}
                      checked={option.selected}
                      disabled
                    >
                      {option.title}
                    </Radio>
                  ))}
                </RadioGroup>
              </div>
            );
        default:
          return null;
      }
    });
  };

  return (
    <div className="main-page">
      <HeaderLayout
        navigationAction={
          <Link startIcon={<ArrowLeft />} to="/plugins/feedbacks">
            Go back
          </Link>
        }
        title={'View Feedback Details'}
        as="h6"
      />
      <div className="page-container">
        <div className='card-details'>
          <div className="card-title">Resource Details</div>
          <CardBody>
            <div className="row">
              <div className="column">
                <div>
                  <label>Resource ID</label>
                  <span>{feedbackDetails.resourceId}</span>
                </div>
              </div>
              <div className="column">
                <div>
                  <label>Resource Name</label>
                  <span>{feedbackDetails.resourceName}</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="column">
                <div>
                  <label>Project Name</label>
                  <span>{feedbackDetails.projectName}</span>
                </div>
              </div>
              <div className="column">
                <div>
                  <label>Thread Name</label>
                  <span>{feedbackDetails.threadName}</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="column">
                <div>
                  <label>Version Name</label>
                  <span>{feedbackDetails.versionName}</span>
                </div>
              </div>
              <div className="column">
                <div>
                  <label>Created At</label>
                  <span>{feedbackDetails.createdAt}</span>
                </div>
              </div>
            </div>
          </CardBody>
        </div>

        <div className='card-details'>
          <div className='card-title'>
              Feedback Details
          </div>
          <div className='card-body'>
            <div className="row">
              <div className="column">
                <div>
                  <label>Title</label>
                  <span>{feedbackDetails.feedbackTitle || '-' }</span>
                </div>
              </div>
              <div className="column">
                <div>
                  <label>Description</label>
                  <span>{ feedbackDetails.feedbackDescription || '-'}</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div>
                <label>Type of Feedback</label>
                <span>{feedbackDetails.typeOfFeedback}</span>
              </div>
              <div className="column">
                <div>
                  <label>Is Assessment</label>
                  <span>{feedbackDetails.isAssesment ? 'Yes' : 'No'}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        {formData.map((param, index) => (
          <div className='card-details'>
            <CardBody>
              <div>
                <div>
                  <div
                    className="section"
                    key={index}
                    style={{ marginBottom: '20px' }}
                  >
                    <h2
                      style={{
                        fontWeight: 'bold',
                        padding: '1rem',
                        transition: 'color 150ms ease-out',
                        color: '#4945ff',
                        fontSize: '0.875rem',
                        lineHeight: '1.43',
                      }}
                    >
                      {param?.formName}
                    </h2>
                    <div className="column">{renderFormParameter(param)}</div>
                  </div>
                </div>
              </div>
            </CardBody>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ViewFeedbackDetails;
