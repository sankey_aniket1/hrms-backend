import React, { memo, useState,useEffect } from 'react';
import {
  Layout,
  ContentLayout,
  HeaderLayout,
  // @ts-ignore
} from '@strapi/design-system/Layout';
import { Box, Link } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import FeedbackTable from '../FeedbackTable';
require('dotenv').config();

const FeedbackPage = () => {
  const [feedbackData,setFeedbackData] = useState([]);


  const tableHeadingsAndKeys = [
    { heading: 'Resource Id', key: 'resourceId' },
    { heading: 'Resource Name', key: 'resourceName' },
    { heading: 'Project Name', key: 'projectName' },
    { heading: 'Thread Name', key: 'threadName' },
    { heading: 'Version Name', key: 'versionName' },
    { heading: 'Type Of Feedback', key: 'typeOfFeedback' },
    { heading: 'Created At', key: 'createdAt' },
  ];

  useEffect(()=>{
    fetchTableData()
  },[])

  const fetchTableData = async() =>{
    try {
      const response = await fetch(`${process.env.URL}/feedbacks/feedback/findAll`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      console.log(data,"this is api data")
      setFeedbackData(data)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link startIcon={<ArrowLeft />} to="/">
              Go back
            </Link>
          }
          title={'Feedbacks'}
          subtitle={`${feedbackData.length} entries found`}
          as="h2"
        />
      </Box>

      <ContentLayout>
        {feedbackData?.length === 0 ? (
          <span>You don't have any feedbacks yet...</span>
        ) : (
          <>
            <FeedbackTable
              tableHeadingsAndKeys={tableHeadingsAndKeys}
              tableRowsData={feedbackData}
            />
          </>
        )}
      </ContentLayout>
    </Layout>
  );
};

export default memo(FeedbackPage);
