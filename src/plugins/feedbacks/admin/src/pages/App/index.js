/**
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { AnErrorOccurred, CheckPagePermissions } from '@strapi/helper-plugin';
import pluginId from '../../pluginId';
import './index.css';
import SideMenu from '../../components/SideMenu';
import pluginPermissions from '../../../../../pms/admin/src/permissions';
import FeedbackPage from '../FeedbackPage';
import ViewFeedbackDetails from '../ViewFeedbackDetails';
import FeedbackFormats from '../FeedbackFormats';
import FeedbackFormatPage from '../FeedbackFormatPage';

const App = () => {
  return (
    <CheckPagePermissions permissions={pluginPermissions.main}>
      <div className="myCustomLayout">
        <div className="header">
          <SideMenu />
        </div>
        {/* <div> */}
        <Switch>
          <Route path={`/plugins/${pluginId}`} component={FeedbackPage} exact />
          {/* <Route
            path={`/plugins/${pluginId}/feedback-formats/add`}
            component={FeedbackFormats}
            exact
          /> */}
          <Route
            path={`/plugins/${pluginId}/feedback-formats`}
            component={FeedbackFormatPage}
            exact
          />
          <Route
            path={`/plugins/${pluginId}/:id`}
            component={ViewFeedbackDetails}
            exact
          />
          <Route component={AnErrorOccurred} />
        </Switch>
        {/* </div> */}
      </div>
    </CheckPagePermissions>
  );
};

export default App;
