import React, { useState, useEffect } from 'react';
import {
  Table,
  Thead,
  TFooter,
  Tbody,
  Tr,
  Td,
  Th,

} from '@strapi/design-system/Table';
import { Box } from '@strapi/design-system/Box';
import { Typography } from '@strapi/design-system/Typography';
// import DeleteConfirmationPopup from '../DeletePopup';
import { useHistory } from 'react-router-dom';
import pluginId from '../../pluginId';
import Pagination from '../../components/Pagination';
import './index.css'

export default function FeedbackTable(props) {
  const [editStates, setEditStates] = useState({});
  const [currentPage, setCurrentPage] = useState(0);
  const history = useHistory();

  const itemsPerPage = 10;
  const pageCount = Math.ceil(props.tableRowsData.length / itemsPerPage);
  const handlePageClick = (event) => {
    setCurrentPage(event.selected);
  };

  const itemsForCurrentPage = props.tableRowsData.slice(
    currentPage * itemsPerPage,
    (currentPage + 1) * itemsPerPage
  );

  const handleViewDetails = (id) => {
    history.push(`/plugins/${pluginId}/${id}`);
  };

  useEffect(() => {
    // Initialize editStates with tableRowsData
    const initialEditStates = {};
    props.tableRowsData.forEach((rowData) => {
      initialEditStates[rowData.id] = {
        inputValue: rowData.name,
        isEdit: false,
      };
    });
    setEditStates(initialEditStates);
  }, [props.tableRowsData]);

  return (
    <Box
      background="neutral0"
      hasRadius={true}
      shadow="filterShadow"
      padding={8}
      style={{ marginTop: '30px' }}
    >
      <Table
        colCount={props.tableHeadingsAndKeys.length}
        rowCount={props.tableRowsData.length}
      >
        <Thead>
          <Tr>
            {props.tableHeadingsAndKeys.map((e) => {
              return (
                <Th>
                  <Typography variant="sigma">{e.heading}</Typography>
                </Th>
              );
            })}
          </Tr>
        </Thead>

        <Tbody>
          {itemsForCurrentPage?.map((rowData) => {
            return (
              <Tr
                key={rowData.id}
                onClick={() => handleViewDetails(rowData.feedbackId)}
                className="clickable-row"
              >
                {props.tableHeadingsAndKeys.map((heading) => (
                  <Td>
                    <Typography textColor="neutral800">
                      {rowData[heading.key]}
                    </Typography>
                  </Td>
                ))}
              </Tr>
            );
          })}
        </Tbody>
      </Table>
      <Pagination pageCount={pageCount} onPageChange={handlePageClick} />
    </Box>
  );
}
