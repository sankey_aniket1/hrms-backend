import React, { memo, useState,useEffect } from 'react';
import {
  Layout,
  ContentLayout,
  HeaderLayout,
} from '@strapi/design-system/Layout';
import { Box, Button, Link } from '@strapi/design-system';
import { ArrowLeft, Plus } from '@strapi/icons';
import FeedbackTable from '../FeedbackTable';
import FeedbackFormats from '../FeedbackFormats';
import './index.css';
require('dotenv').config();

const FeedbackFormatPage = () => {

  const [feedbackFormatData,setFeedbackFormatData ] = useState([
  ]);

  const [showFeedbackFormats, setShowFeedbackFormats] = useState(false);

  const tableHeadings = [
    {key:'title',heading:'Title'},
    {key:'type',heading:'Type'},
    {key:'triggerAction',heading:'Trigger Action'},
    {key:'triggerActionPeriod',heading:'Trigger Action Period'},
    {key:'cooldownPriod',heading:'Cooldown Priod'},
    {key:'createdAt',heading:'Created At'},
  ];

  useEffect(()=>{
    fetchTableData()
  },[])

  const fetchTableData = async() =>{
    try {
      const response = await fetch(`${process.env.URL}/feedbacks/feedback-format/findAll`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      console.log(data,"this is api data")
      setFeedbackFormatData(data)
    } catch (error) {
      console.log(error)
    }
  }


  return (
    <Layout>
      {!showFeedbackFormats && (
        <Box background="neutral100">
          <HeaderLayout
            navigationAction={
              <Link startIcon={<ArrowLeft />} to="/">
                Go back
              </Link>
            }
            primaryAction={
              <div className='primaryAction'>
                <Button
                  startIcon={<Plus />}
                  onClick={() => setShowFeedbackFormats(true)}
                >
                  Add New Feedback Formats
                </Button>
              </div>
            }
            title={'Feedback Formats'}
            subtitle={`${feedbackFormatData.length} entries found`}
            as="h2"
          />
        </Box>
      )}
      <ContentLayout>
        {showFeedbackFormats ? (
          <FeedbackFormats />
        ) : feedbackFormatData?.length === 0 ? (
          <span>You don't have any feedback formats yet...</span>
        ) : (
          <FeedbackTable
            tableHeadingsAndKeys={tableHeadings}
            tableRowsData={feedbackFormatData}
          />
        )}
      </ContentLayout>
    </Layout>
  );
};

export default memo(FeedbackFormatPage);
