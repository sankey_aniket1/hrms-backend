export const FORM_FIELD_FORMATS = {
  section: {
    sectionTitle: '',
    formParameters: [],
  },
  textInput: {
    type: 'Text',
    title: '',
    isRequired: true,
    value: '',
  },
  textArea: {
    type: 'TextArea',
    title: '',
    isRequired: true,
    value: '',
  },
  choiceField: {
    type: 'Choice',
    title: '',
    options: [
      {
        title: '',
        selected: false,
      },
    ],
    isRadioOrCheckBox: false,
    isRequired: true,
  },
  option: {
    title: '',
    selected: false,
  },
};
