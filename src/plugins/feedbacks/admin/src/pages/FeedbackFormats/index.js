import React, { memo, useState } from 'react';
import {
  Layout,
  HeaderLayout,
  ContentLayout,
} from '@strapi/design-system/Layout';
import {
  TextInput,
  Textarea,
  Checkbox,
  Typography,
} from '@strapi/design-system';
import { Box, Link } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import Select from 'react-select';
import { Button } from '@strapi/design-system/Button';
import './index.css';
// import DeleteIcon from '../../../../../../assets/icons/deleteIcon';
require('dotenv').config();

const customStyles = {
  control: (base) => ({
    ...base,
    fontFamily:
      "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    fontSize: '0.875rem',
    fontWeight: 400,
    paddingLeft: '5px',
    paddingRight: '5px',
    paddingBottom: '0.05625rem',
    paddingTop: '0.05625rem',
  }),
  option: (provided, state) => ({
    ...provided,
    fontFamily:
      "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    fontSize: '0.875rem',
    fontWeight: 400,
  }),
};

const customComponentStyles = {
  textInputStyle: { margin: '20px 0' },
  parameterComponentMainDivStyle: { paddingLeft: '2rem' },
  labelFontSize: { fontSize: '0.75rem' },
  formFormatBasicDetailSection: { width:'100%',padding: '5px 20px' },
  formFormatBasicDetailFlexBox: {
    display: 'flex',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  formFormatBasicDetailFlexBoxElement: { width: '48%' },
  formFormatBasicDetailFlexBoxElementLabel: { marginBottom: '4px' },
  parameterComponentParameterTextStyle: {
    marginBottom: '4px',
    marginTop: '15px',
    fontWeight: 'bold',
  },
  parameterComponentParameterTypeTextStyle: {
    marginBottom: '4px',
    marginTop: '15px',
  },
  parameterComponentParameterTypeOptionsMarginBottom: { marginBottom: '4px' },
  addSectionParameterButtonsBox: {
    display: 'flex',
    justifyContent: 'end',
    margin: '10px 0',
  },
  addSectionParameterButtons: { marginLeft: '4px' },
  formFormatBasicDetailSectionFormSection: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    padding: '5px 20px',
  },
};

const FeedbackFormats = () => {
  const [formSections, setFormSections] = useState([
    {
      id: Date.now(),
      sectionTitle: '',
      formParameters: [],
    },
  ]);

  const [formData, setFormData] = useState({
    title: '',
    description: '',
    type: 'PIP',
    triggerAction: 'Add Resource',
    triggerActionPeriod: '',
    coolDownPeriod: '',
    isAssessment: false,
    formSections: formSections,
  });

  const [errors, setErrors] = useState({});
  let otherFields = []


  const handleChangeOutSide = (e) => {
    const { name, value, type, checked } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: type === 'checkbox' ? checked : value,
    }));
  };

  const handleSelectChange = (name, selectedOption) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: selectedOption.value,
    }));
  };

  const handleTextareaChange = (value) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      description: value,
    }));
  };

  const [forms, setForms] = useState([
    {
      formName: '',
      fields: [{ id: Date.now(), type: 'text', value: '' }],
      errors: {},
    },
  ]);

  const addForm = () => {
    setForms([
      ...forms,
      {
        formName: '',
        fields: [{ id: Date.now(), type: 'text', value: '' }],
        errors: {},
      },
    ]);
  };

  const removeForm = (formIndex) => {
    if (forms.length <= 1) return;
    const updatedForms = forms.filter((_, index) => index !== formIndex);
    setForms(updatedForms);
  };

  const addField = (formIndex, type) => {
    const newFields = [...forms[formIndex].fields];
    let newField = {
      id: Date.now(),
      type,
      value: { question: '', options: [''] },
    };
    if (type === 'radio' || type === 'checkbox') {
      newField.value.options.push('');
    }
    newFields.push(newField);
    const updatedForms = [...forms];
    updatedForms[formIndex].fields = newFields;
    setForms(updatedForms);
  };

  const removeField = (formIndex, fieldId) => {
    const updatedForms = [...forms];
    updatedForms[formIndex].fields = updatedForms[formIndex].fields.filter(
      (field) => field.id !== fieldId
    );
    setForms(updatedForms);
  };

  const addOption = (formIndex, fieldIndex) => {
    const newFields = [...forms[formIndex].fields];
    newFields[fieldIndex].value.options.push('');
    const updatedForms = [...forms];
    updatedForms[formIndex].fields = newFields;
    setForms(updatedForms);
  };

  const handleChange = (formIndex, fieldId, key, value) => {
    const updatedForms = [...forms];
    updatedForms[formIndex].fields = updatedForms[formIndex].fields.map(
      (field) =>
        field.id === fieldId
          ? { ...field, value: { ...field.value, [key]: value } }
          : field
    );
    setForms(updatedForms);
    setErrors((prevErrors) => ({
      ...prevErrors,
      [fieldId]: '',
    }));
  };

  const handleSubmitAllForms = async() => {
    let isValid = true;
    const formDataSection = [];
    const formErrors = {};
    const otherFieldErrors = {};

    // Validate the main form fields
    const mainFormFields = ['title', 'description', 'type', 'triggerAction', 'triggerActionPeriod', 'coolDownPeriod'];
    mainFormFields.forEach(field => {
      if (formData[field].trim() === '') {
        isValid = false;
        formErrors[field] = `${field.charAt(0).toUpperCase() + field.slice(1)} is required`;
      }
    });

    // Validate otherFields array
    if (otherFields.length > 0) {
      otherFields.forEach(field => {
        otherFieldErrors[field] = `${field.charAt(0).toUpperCase() + field.slice(1)} is required`;
      });
    }

    // Validate form sections and fields
    const updatedForms = forms.map((form, formIndex) => {
      const formErrorsObj = {};
      if (!form || !form.formName || form.formName.trim() === '') {
        isValid = false;
        formErrorsObj.formName = 'Form name is required';
      }

      form.fields.forEach((field) => {
        if (!field || !field.value || !field.value.question || field.value.question.trim() === '') {
          isValid = false;
          formErrorsObj[field.id] = 'Question/Option is required';
        }
      });

      if (Object.keys(formErrorsObj).length > 0) {
        formErrors[formIndex] = formErrorsObj;
      }

      const formDataItem = {
        formName: form.formName,
        fields: form.fields.map((field) => ({
          question: field.value.question,
          type: field.type,
          options: field.value.options || [],
        })),
      };
      formDataSection.push(formDataItem);

      return form;
    });

    if (isValid) {
      const requestBody = {
        title: formData.title,
        description: formData.description,
        isAssesment:formData.isAssessment,
        type:formData.type,
        triggerAction:formData.triggerAction,
        triggerActionPeriod:formData.triggerActionPeriod,
        cooldownPriod:formData.coolDownPeriod,
        FormParameters:formDataSection
      };

      try {
        const response = await fetch(`${process.env.URL}/feedbacks/feedback-format/create`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(requestBody),
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        const data = await response.json();
        console.log(data)
      } catch (error) {
        console.log(error)
      }
    } else {
      console.log('Form validation failed:', formErrors);
      setErrors({...formErrors, ...otherFieldErrors});
    }

  };



  const renderFields = (formIndex) => {
    return forms[formIndex].fields.map((field, index) => (
      <div
        className={
          field.type === 'radio' || field.type === 'checkbox'
            ? 'radiofileds'
            : 'subFormFields'
        }
        key={field.id}
      >
        <div className="inputfileds">
          <TextInput
            label={`Parameter : ${ field.type }`}
            placeholder="Enter Question"
            type="text"
            value={field.value.question}
            onChange={(e) =>
              handleChange(formIndex, field.id, 'question', e.target.value)
            }
            style={{ width: '600px' }}
            error={errors[formIndex]?.[field.id] || ''}
          />
          <Button
            width={'3rem'}
            style={{ color:'#db4444' }}
            // startIcon={<DeleteIcon color={'#db4444'} />}
            onClick={() => removeField(formIndex, field.id)}
          >
          </Button>
        </div>

        {field.type === 'radio' && (
          <div className="mainOptions">
            {field.value.options.map((option, optionIndex) => (
              <div className={'radiosClass'} key={optionIndex}>
                <TextInput
                  label={`Option ${optionIndex+1}`}
                  type="text"
                  value={option}
                  error={errors[formIndex]?.[field.id] || ''}
                  onChange={(e) => {
                    const newOptions = [...field.value.options];
                    newOptions[optionIndex] = e.target.value;
                    handleChange(formIndex, field.id, 'options', newOptions);
                  }}
                />
                <Button
                  onClick={() => {
                    const newOptions = [...field.value.options];
                    newOptions.splice(optionIndex, 1);
                    handleChange(formIndex, field.id, 'options', newOptions);
                  }}
                  width={'2.9rem'}
                  style={{ color: field.value.options.length === 1 ? 'white' : '#db4444' }}
                  // startIcon={<DeleteIcon color={field.value.options.length === 1 ? 'white' : '#db4444'} />}
                  disabled={field.value.options.length === 1}
                >
                </Button>
              </div>
            ))}
            <Button width={'8rem'} onClick={() => addOption(formIndex, index)}>
              Add Option
            </Button>
          </div>
        )}
        {field.type === 'checkbox' && (
          <div className="mainOptions">
            {field.value.options.map((option, optionIndex) => (
              <div className={'radiosClass'} key={optionIndex}>
                <TextInput
                  label={`Option ${optionIndex+1}`}
                  type="text"
                  value={option}
                  error={errors[formIndex]?.[field.id] || ''}
                  onChange={(e) => {
                    const newOptions = [...field.value.options];
                    newOptions[optionIndex] = e.target.value;
                    handleChange(formIndex, field.id, 'options', newOptions);
                  }}
                />
                <Button
                  onClick={() => {
                    const newOptions = [...field.value.options];
                    newOptions.splice(optionIndex, 1);
                    handleChange(formIndex, field.id, 'options', newOptions);
                  }}
                  width={'2.8rem'}
                  style={{ color: field.value.options.length === 1 ? 'white' : '#db4444' }}
                  // startIcon={<DeleteIcon color={field.value.options.length === 1 ? 'white' : '#db4444'} />}
                  disabled={field.value.options.length === 1}
                >
                </Button>
              </div>
            ))}
            <Button width={'8rem'} onClick={() => addOption(formIndex, index)}>
              Add Option
            </Button>
          </div>
        )}
      </div>
    ));
  };

  return (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link startIcon={<ArrowLeft />} to="/">
              Go back
            </Link>
          }
          title={'Feedback formats'}
          as="h2"
        />
      </Box>

      <div className="sectionsBox">
        <Box
          background="neutral0"
          hasRadius={true}
          shadow="filterShadow"
          style={customComponentStyles.formFormatBasicDetailSection}
        >
          <div style={customComponentStyles.textInputStyle}>
            <TextInput
              label="Title"
              name="title"
              id="title"
              placeholder="Enter form title"
              onChange={handleChangeOutSide}
              value={formData.title}
              error={errors.title}
            />
          </div>

          <div style={customComponentStyles.textInputStyle}>
            <Textarea
              label="Description"
              name="description"
              id="description"
              placeholder="Provide description for the form"
              onChange={(e) => handleTextareaChange(e.target.value)}
              value={formData.description}
              error={errors.description}
            />
          </div>

          <div style={customComponentStyles.textInputStyle}>
            <div style={customComponentStyles.formFormatBasicDetailFlexBox}>
              <div
                style={
                  customComponentStyles.formFormatBasicDetailFlexBoxElement
                }
              >
                <div
                  style={
                    customComponentStyles.formFormatBasicDetailFlexBoxElementLabel
                  }
                >
                  <Typography
                    variant="pi"
                    fontWeight="bold"
                    marginBottom="10px"
                  >
                    Type
                  </Typography>
                </div>
                <Select
                  id="type"
                  styles={customStyles}
                  options={['Evaluation', 'PIP', 'Quarterly assessment'].map(
                    (option) => ({
                      value: option,
                      label: option,
                    })
                  )}
                  placeholder="Select type of form"
                  name="type"
                  onChange={(selectedOption) =>
                    handleSelectChange('type', selectedOption)
                  }
                  value={{ value: formData.type, label: formData.type }}
                  error={errors.type}
                />
              </div>
              <div
                style={
                  customComponentStyles.formFormatBasicDetailFlexBoxElement
                }
              >
                <div
                  style={
                    customComponentStyles.formFormatBasicDetailFlexBoxElementLabel
                  }
                >
                  <Typography variant="pi" fontWeight="bold">
                    Trigger Action
                  </Typography>
                </div>
                <Select
                  id="triggerAction"
                  styles={customStyles}
                  options={['Add Resource', 'Remove Resource'].map(
                    (option) => ({
                      value: option,
                      label: option,
                    })
                  )}
                  placeholder="Select trigger action"
                  name="triggerAction"
                  onChange={(selectedOption) =>
                    handleSelectChange('triggerAction', selectedOption)
                  }
                  value={{
                    value: formData.triggerAction,
                    label: formData.triggerAction,
                  }}
                  error={errors.triggerAction}
                />
              </div>
            </div>
          </div>

          <div style={customComponentStyles.textInputStyle}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignContent: 'center',
              }}
            >
              <div style={{ width: '48%' }}>
                <TextInput
                  label="Trigger Period"
                  name="triggerActionPeriod"
                  type="number"
                  id="triggerActionPeriod"
                  placeholder="Enter trigger period"
                  onChange={handleChangeOutSide}
                  value={formData.triggerActionPeriod}
                  error={errors.triggerActionPeriod}
                />
              </div>
              <div style={{ width: '48%' }}>
                <TextInput
                  label="Cooldown Period"
                  name="coolDownPeriod"
                  type="number"
                  id="coolDownPeriod"
                  placeholder="Enter cooldown period"
                  onChange={handleChangeOutSide}
                  value={formData.coolDownPeriod}
                  error={errors.coolDownPeriod}
                />
              </div>
            </div>
          </div>

          <div
            style={{
              margin: '20px 0',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Checkbox
              name="isAssessment"
              id="isAssessment"
              onChange={handleChangeOutSide}
              checked={formData.isAssessment}
            />
            <span style={customComponentStyles.labelFontSize}>Assessment</span>
          </div>
        </Box>

        {forms.map((form, formIndex) => (
          <Box
            background="neutral0"
            hasRadius={true}
            shadow="filterShadow"
            style={
              customComponentStyles.formFormatBasicDetailSectionFormSection
            }
          >
            <div className="formSection" key={formIndex}>
              <div className="inputfileds">
                <TextInput
                  label="Section Name:"
                  placeholder="Enter Section Name"
                  onChange={(e) => {
                    const updatedForms = [...forms];
                    updatedForms[formIndex].formName = e.target.value;
                    setForms(updatedForms);
                  }}
                  value={form.formName}
                  style={{ width: '600px' }}
                  error={errors[formIndex]?.formName || ''}
                />
                <Button
                  disabled={forms.length <= 1}
                  onClick={() => removeForm(formIndex)}
                >
                  Remove Form
                </Button>
              </div>
              <div className='actualSection'>
                {renderFields(formIndex)}
                <div className="formButtons">
                  <Button onClick={() => addField(formIndex, 'text')}>
                    Add Text Field
                  </Button>
                  <Button onClick={() => addField(formIndex, 'textarea')}>
                    Add Text Area
                  </Button>
                  <Button onClick={() => addField(formIndex, 'radio')}>
                    Add Radio Button
                  </Button>
                  <Button onClick={() => addField(formIndex, 'checkbox')}>
                    Add Checkbox
                  </Button>
                </div>
              </div>
            </div>
          </Box>
        ))}
        <Button
          width={'7rem'}
          justifyContent="center"
          alignContent="center"
          onClick={addForm}
        >
          Add Form
        </Button>
        <Button width={'9rem'} onClick={handleSubmitAllForms}>
          Submit All Forms
        </Button>
      </div>
    </Layout>
  );
};

export default memo(FeedbackFormats);
