import React, { useState, useEffect } from 'react';
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Td,
  Th,
  // @ts-ignore
} from '@strapi/design-system/Table';
// @ts-ignore
import { Box } from '@strapi/design-system/Box';
// @ts-ignore
import { Flex } from '@strapi/design-system/Flex';
// @ts-ignore
import { Typography } from '@strapi/design-system/Typography';
// @ts-ignore
import { IconButton } from '@strapi/design-system/IconButton';
// @ts-ignore
import { TextInput } from '@strapi/design-system/TextInput';
// @ts-ignore
import Pencil from '@strapi/icons/Pencil'; // @ts-ignore
import DeleteConfirmationPopup from '../DeletePopup';
import { FaTrash } from 'react-icons/fa';
import Pagination from '../Pagination';

function Input({ value, onChange }) {
  return (
    <TextInput
      type="text"
      aria-label="todo-input"
      name="rowData-input"
      error={value?.length > 40 ? 'Text should be less than 40 characters' : ''}
      onChange={onChange}
      value={value}
    />
  );
}

export default function PmsTable(props) {
  const [editStates, setEditStates] = useState({});
  const [showPopup, setShowPopup] = useState(false);
  const [selectedProject, setSelectedProject] = useState(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [expandedProjects, setExpandedProjects] = useState({});

  const itemsPerPage = 10;
  const pageCount = Math.ceil(props.tableRowsData.length / itemsPerPage);
  const handlePageClick = (event) => {
    setCurrentPage(event.selected);
  };

  const itemsForCurrentPage = props.tableRowsData.slice(
    currentPage * itemsPerPage,
    (currentPage + 1) * itemsPerPage
  );

  const handleDeleteClick = (rowData) => {
    setShowPopup(true);
    setSelectedProject(rowData);
  };

  const handleConfirmDelete = (rowDataId) => {
    props.deleteTableData(rowDataId);
    setShowPopup(false);
  };

  useEffect(() => {
    // Initialize editStates with tableRowsData
    const initialEditStates = {};
    props.tableRowsData.forEach((rowData) => {
      initialEditStates[rowData.id] = {
        inputValue: rowData.name,
        isEdit: false,
      };
    });
    setEditStates(initialEditStates);
  }, [props.tableRowsData]);

  return (
    <Box
      background="neutral0"
      hasRadius={true}
      shadow="filterShadow"
      style={{ marginTop: '0.1rem' }}
    >
      <Table
        colCount={props.tableHeadings.length}
        rowCount={props.tableRowsData.length}
      >
        <Thead>
          <Tr>
            {props.tableHeadings.map((e) => {
              return (
                <Th>
                  <Typography variant="sigma">{e}</Typography>
                </Th>
              );
            })}
          </Tr>
        </Thead>

        <Tbody style={{ justifyContent: 'center' }}>
          {itemsForCurrentPage?.map((rowData) => {
            return (
              <Tr
                key={rowData.id}
                onClick={() => {
                  let val = true;
                  props.viewDetails(val, rowData);
                  props.setShowModal(true);
                }}
                className="clickable-row"
              >
                {props.projectInputs.map((e) => {
                  return (
                    <Td key={e.inputType}>
                      {editStates[rowData.id]?.isEdit ? (
                        <Input
                          value={editStates[rowData.id]?.inputValue}
                          onChange={(e) =>
                            setEditStates({
                              ...editStates,
                              [rowData.id]: {
                                ...editStates[rowData.id],
                                inputValue: e.target.value,
                              },
                            })
                          }
                        />
                      ) : (
                        <Typography textColor="neutral800">
                          {expandedProjects[rowData.id]
                            ? rowData[e.inputType]
                            : typeof rowData[e.inputType] === 'string'
                            ? rowData[e.inputType].length > 20
                              ? rowData[e.inputType].substring(0, 20) + '...'
                              : rowData[e.inputType]
                            : typeof rowData[e.inputType] === 'boolean'
                            ? rowData[e.inputType].toString()
                            : e.inputType === 'project'
                            ? rowData[e.inputType].projectName
                            : e.inputType === 'projectThreads'
                            ? rowData['threads'].length
                            : e.inputType === 'thread'
                            ? rowData[e.inputType].threadName
                            : e.inputType === 'versions'
                            ? rowData[e.inputType].length
                            : e.inputType === 'versioncomment'
                            ? rowData[e.inputType]
                            : e.inputType === 'versionDls'
                            ? rowData[e.inputType][0].versionName
                            : e.inputType === 'employee'
                            ? rowData[e.inputType].firstName +
                              ' ' +
                              rowData[e.inputType].lastName
                            : e.inputType === 'version'
                            ? rowData[e.inputType].versionName
                            : rowData[e.inputType]}
                        </Typography>
                      )}
                    </Td>
                  );
                })}

                {props.title !== 'Version Comments and Requests' && (
                  <Td>
                    <Flex>
                      <IconButton
                        onClick={(event) => {
                          event.stopPropagation();
                          let edit = true;
                          props.setShowModal(true);
                          props.changeEditValue(edit, rowData);
                        }}
                        label="Edit"
                        noBorder
                        icon={<Pencil />}
                      />
                      <Box paddingLeft={1}>
                        <IconButton
                          onClick={(event) => {
                            event.stopPropagation();
                            handleDeleteClick(rowData);
                          }}
                          label="Delete"
                          noBorder
                          icon={<FaTrash />}
                        />
                      </Box>
                    </Flex>
                    {showPopup && (
                      <>
                        <div
                          className="backdrop"
                          onClick={() => setShowPopup(false)}
                        ></div>
                        <DeleteConfirmationPopup
                          rowData={selectedProject}
                          onDeleteConfirm={handleConfirmDelete}
                          onCancel={(event) => {
                            event.stopPropagation();
                            setShowPopup(false);
                          }}
                        />
                      </>
                    )}
                  </Td>
                )}
              </Tr>
            );
          })}
        </Tbody>
      </Table>
      <Pagination pageCount={pageCount} onPageChange={handlePageClick} />
    </Box>
  );
}
