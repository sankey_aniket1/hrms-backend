import React, { useState } from 'react';
import { Sidebar, Menu, MenuItem } from 'react-pro-sidebar';
import { BsThreads } from 'react-icons/bs';
import { NavLink } from 'react-router-dom';
import pluginId from '../../pluginId';
import {
  FcTreeStructure,
  FcGenealogy,
  FcAssistant,
  FcComments,
} from 'react-icons/fc';
import './index.css';

const SideMenu = () => {
  const [activeItem, setActiveItem] = useState('projects');

  const handleMenuClick = (item) => {
    setActiveItem(item);
  };

  return (
    <div id="header">
      <Sidebar>
        <Menu>
          <NavLink
            to={`/plugins/${pluginId}`}
            className={({ isActive }) =>
              isActive || activeItem === 'projects' ? 'activated' : ''
            }
            onClick={() => handleMenuClick('projects')}
          >
            <MenuItem icon={<FcTreeStructure />}>Projects</MenuItem>
          </NavLink>
          <NavLink
            to={`/plugins/${pluginId}/threads`}
            className={({ isActive }) =>
              isActive || activeItem === 'threads' ? 'activated' : ''
            }
            onClick={() => handleMenuClick('threads')}
          >
            <MenuItem icon={<BsThreads />}>Threads</MenuItem>
          </NavLink>
          <NavLink
            to={`/plugins/${pluginId}/versions`}
            className={({ isActive }) =>
              isActive || activeItem === 'versions' ? 'activated' : ''
            }
            onClick={() => handleMenuClick('versions')}
          >
            <MenuItem icon={<FcGenealogy />}>Versions</MenuItem>
          </NavLink>
          <NavLink
            to={`/plugins/${pluginId}/version-comments-requests`}
            className={({ isActive }) =>
              isActive || activeItem === 'comments-requests' ? 'activated' : ''
            }
            onClick={() => handleMenuClick('comments-requests')}
          >
            <MenuItem icon={<FcComments />}>Comments & Requests</MenuItem>
          </NavLink>
          <NavLink
            to={`/plugins/${pluginId}/version-resources`}
            className={({ isActive }) =>
              isActive || activeItem === 'resources' ? 'activated' : ''
            }
            onClick={() => handleMenuClick('resources')}
          >
            <MenuItem icon={<FcAssistant />}>Version Resources</MenuItem>
          </NavLink>
        </Menu>
      </Sidebar>
    </div>
  );
};

export default SideMenu;
