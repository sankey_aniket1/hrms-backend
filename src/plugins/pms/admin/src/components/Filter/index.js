import React, { useState, useEffect } from 'react';
import { SingleSelect, SingleSelectOption } from '@strapi/design-system';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';
import { GET } from '../../../../constants/constant';

const FilterComponent = ({ setSearchParams }) => {
  const [projects, setProjects] = useState([]);
  const [allThreads, setAllThreads] = useState([]);
  const [allVersions, setAllVersions] = useState([]);
  const [filteredThreads, setFilteredThreads] = useState([]);
  const [filteredVersions, setFilteredVersions] = useState([]);
  const [selectedProject, setSelectedProject] = useState('');
  const [selectedThread, setSelectedThread] = useState('');
  const [selectedVersion, setSelectedVersion] = useState('');

  useEffect(() => {
    const fetchProjects = async () => {
      try {
        const data = await makeRequest(endpoints.GET_ALL_PROJECTS, GET);
        setProjects(data || []);
      } catch (error) {
        console.error('Error fetching projects:', error);
      }
    };

    const fetchThreads = async () => {
      try {
        const data = await makeRequest(endpoints.GET_ALL_THREADS, GET);
        setAllThreads(data || []);
      } catch (error) {
        console.error('Error fetching threads:', error);
      }
    };

    const fetchVersions = async () => {
      try {
        const data = await makeRequest(endpoints.GET_ALL_VERSIONS, GET);
        setAllVersions(data || []);
      } catch (error) {
        console.error('Error fetching versions:', error);
      }
    };

    fetchProjects();
    fetchThreads();
    fetchVersions();
  }, []);

  useEffect(() => {
    if (selectedProject) {
      const filtered = allThreads.filter(
        (thread) => thread.project.projectName === selectedProject
      );
      setFilteredThreads(filtered);
      setFilteredVersions([]);
      setSelectedThread('');
      setSelectedVersion('');
    }
  }, [selectedProject, allThreads]);

  useEffect(() => {
    if (selectedThread) {
      const filtered = allVersions.filter(
        (version) => version.thread.threadName === selectedThread
      );
      setFilteredVersions(filtered);
      setSelectedVersion('');
    }
  }, [selectedThread, allVersions]);

  useEffect(() => {
    setSearchParams({
      selectedProject,
      selectedThread,
      selectedVersion,
    });
  }, [selectedProject, selectedThread, selectedVersion, setSearchParams]);

  return (
    <div
      style={{
        display: 'flex',
        gap: '1rem',
        alignItems: 'center',
        paddingBottom: '1rem',
      }}
    >
      <SingleSelect
        placeholder="Select a project"
        value={selectedProject}
        onChange={(value) => setSelectedProject(value)}
        aria-label="Select project"
      >
        {projects.map((project) => (
          <SingleSelectOption key={project.id} value={project.projectName}>
            {project.projectName}
          </SingleSelectOption>
        ))}
      </SingleSelect>
      <SingleSelect
        placeholder="Select a thread"
        value={selectedThread}
        onChange={(value) => setSelectedThread(value)}
        aria-label="Select thread"
        disabled={!selectedProject}
      >
        {filteredThreads.map((thread) => (
          <SingleSelectOption key={thread.id} value={thread.threadName}>
            {thread.threadName}
          </SingleSelectOption>
        ))}
      </SingleSelect>
      <SingleSelect
        placeholder="Select a version"
        value={selectedVersion}
        onChange={(value) => setSelectedVersion(value)}
        aria-label="Select version"
        disabled={!selectedThread}
      >
        {filteredVersions.map((version) => (
          <SingleSelectOption key={version.id} value={version.id}>
            {version.versionName}
          </SingleSelectOption>
        ))}
      </SingleSelect>
    </div>
  );
};

export default FilterComponent;