/**
 *
 * PluginIcon
 *
 */

import React from 'react';
import { PmsIcon } from '../../assets';

const PluginIcon = () => <PmsIcon />;

export default PluginIcon;
