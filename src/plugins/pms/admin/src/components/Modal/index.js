import React, { useEffect, useState } from 'react';
import {
  ModalLayout,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Typography,
  Button,
  TextInput,
  Textarea,
  MultiSelect,
  MultiSelectOption,
  SingleSelect,
  SingleSelectOption,
  Switch,
} from '@strapi/design-system';
import './index.css';
import {
  TEXTINPUT,
  TEXTAREA,
  SELECT,
  DATE,
  MULTISELECT,
} from '../../../../constants/constant';

const customStyles = {
  control: (base) => ({
    ...base,
    fontFamily:
      "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    fontSize: '0.875rem',
    fontWeight: 400,
    paddingLeft: '1rem',
    paddingRight: '1rem',
    paddingBottom: '0.05625rem',
    paddingTop: '0.05625rem',
  }),
  option: (provided, state) => ({
    ...provided,
    fontFamily:
      "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    fontSize: '0.875rem',
    fontWeight: 400,
  }),
};

export default function Modal(props) {
  const [startDatePicker, setStartDatePicker] = useState();
  const [endDatePicker, setEndDatePicker] = useState();
  const [error, setError] = useState(false);
  const [changeStatus, setChangeStatus] = useState(false);
  const [message, setMessage] = useState({
    keyName: '',
    error: '',
  });

  useEffect(() => {
    // @ts-ignore
    setStartDatePicker(document.querySelector('#startDate'));
    setChangeStatus(
      props.viewMode.isRequestApproved == 'Rejected' ? false : true
    );
    // @ts-ignore
    setEndDatePicker(document.querySelector('#endDate'));
  }, []);

  const updateDateLimits = (e) => {
    const startDate = new Date(e.target.value);
    const endDate = new Date(e.target.value);
    if (startDate) {
      endDatePicker.setAttribute('min', startDate.toISOString().split('T')[0]);
    }
    if (endDate) {
      startDatePicker.setAttribute('max', endDate.toISOString().split('T')[0]);
    }
  };

  const handleInputChange = (e, actionMeta) => {
    if (props.tabName == 'Requests') {
      setChangeStatus(!changeStatus);
      props.setViewMode((prevViewMode) => ({
        ...prevViewMode,
        isRequestApproved:
          changeStatus === true
            ? 'Rejected'
            : changeStatus === false
            ? 'Approved'
            : 'Pending',
      }));
    } else {
      const { id, value } = e.target;
      const { name } = actionMeta;
      const maxLength = id === 'description' ? 2000 : 20;
      setError(false);
      setMessage({
        keyName: '',
        error: '',
      });

      if (allFieldsFilled()) {
        setError(true);
        setMessage({
          keyName: name,
          error: 'This field is required',
        });
      } else if (value.length > maxLength) {
        setError(true);
        setMessage({
          keyName: name,
          error: `Length should not be more than ${maxLength}`,
        });
      }

      if (id === 'startDate' || id === 'endDate') {
        updateDateLimits(e);
      }
      if (props.editMode.edit) {
        props.setEditMode((prevEditMode) => ({
          ...prevEditMode,
          [id]: value,
        }));
      } else {
        props.setFormData((prevFormData) => ({
          ...prevFormData,
          [id]: value,
        }));
      }
    }
  };

  const handleMultiSelectChange = (selectedValues, name) => {
    if (props.editMode.edit) {
      props.setEditMode((prev) => ({
        ...prev,
        [name]: JSON.stringify(selectedValues),
      }));
    } else {
      props.setFormData((prev) => ({
        ...prev,
        [name]: selectedValues,
      }));
    }
  };

  const allFieldsFilled = () => {
    return Object.values(props.formData).every(
      (value) => (Array.isArray(value) ? value.join('') : value.trim()) !== ''
    );
  };

  const resetForm = () => {
    props.setFormData({
      keyName: '',
      error: '',
    });
    setError(false);
    setMessage({
      keyName: '',
      error: '',
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (props.editMode.edit === true) {
      try {
        await props.editTableData(props.editMode.id, props.editMode);
        props.setShowModal(false);
        props.setEditMode((prev) => ({
          ...prev,
          edit: !props.editMode.edit,
        }));
      } catch (e) {
        console.log('error', e);
      }
    } else {
      try {
        await props.addTableData(props.formData);
        props.setShowModal(false);
      } catch (e) {
        console.log('error', e);
      }
    }
    resetForm();
  };

  const handleCloseModal = () => {
    props.setShowModal(false);
    props.setViewMode((prev) => ({
      ...prev,
      view: false,
    }));
    props.setEditMode((prev) => ({
      ...prev,
      edit: false,
    }));

    if (props.tabName == 'Requests') {
      props?.setUpdatedStatus((prev) => !prev);
    } else {
      console.error('Unable to get updated status!');
    }
    resetForm();
  };

  return (
    <ModalLayout
      onClose={handleCloseModal}
      labelledBy="title"
      as="form"
      onSubmit={handleSubmit}
    >
      <ModalHeader>
        <Typography fontWeight="bold" textColor="neutral800" as="h2" id="title">
          {props.title === 'Version Comments and Requests'
            ? 'Version Comments and Requests'
            : props.editMode.edit === true
            ? `Update ${props.title} Details`
            : props.viewMode.view === true
            ? `View ${props.title} Details`
            : `Add New ${props.title} Details`}
        </Typography>
      </ModalHeader>

      <ModalBody>
        <div style={{ overflowY: 'auto' }}>
          {props.modelInputs.map((e) => {
            return e.type === TEXTINPUT ? (
              <div style={{ margin: '1rem 0' }} key={e.name}>
                <TextInput
                  // @ts-ignore
                  label={e.label}
                  name={e.name}
                  id={e.name}
                  placeholder={e.placeholder}
                  error={
                    error === true && message.keyName === e.name
                      ? message.error
                      : ''
                  }
                  onChange={(event) =>
                    handleInputChange(event, { name: e.name })
                  }
                  value={
                    props.editMode.edit
                      ? props.editMode[e.name]
                      : props.viewMode.view
                      ? props.viewMode[e.name]
                      : props.formData[e.name]
                  }
                  disabled={props.viewMode.view}
                  required
                />
              </div>
            ) : e.type === TEXTAREA ? (
              <div style={{ margin: '1rem 0' }} key={e.name}>
                <Textarea
                  label={e.label}
                  name={e.name}
                  id={e.name}
                  placeholder={e.placeholder}
                  required
                  error={
                    error === true && message.keyName === e.name
                      ? message.error
                      : ''
                  }
                  onChange={(event) =>
                    handleInputChange(event, { name: e.name })
                  }
                  disabled={props.viewMode.view}
                >
                  {props.editMode.edit
                    ? props.editMode[e.name]
                    : props.viewMode.view
                    ? props.viewMode[e.name]
                    : props.formData[e.name]}
                </Textarea>
              </div>
            ) : e.type === SELECT ? (
              <div style={{ margin: '1rem 0' }} key={e.name}>
                <SingleSelect
                  label={e.label}
                  id={e.name}
                  styles={customStyles}
                  placeholder={e.placeholder}
                  name={e.name}
                  disabled={props.viewMode.view || e.disabled}
                  required
                  error={
                    error === true && message.keyName === e.name
                      ? message.error
                      : ''
                  }
                  onChange={(selectedOption) => {
                    props.handleSelectChange(selectedOption, { name: e.name });
                  }}
                  value={
                    props.editMode.edit
                      ? props.editMode[e.name]
                      : props.viewMode.view
                      ? props.viewMode[e.name]
                      : props.formData[e.name]
                  }
                >
                  {e.options.map((option) => (
                    <SingleSelectOption key={option.value} value={option.value}>
                      {option.label}
                    </SingleSelectOption>
                  ))}
                </SingleSelect>
              </div>
            ) : e.type === MULTISELECT ? (
              <div style={{ margin: '1rem 0' }} key={e.name}>
                <MultiSelect
                  label={e.label}
                  placeholder={e.placeholder}
                  required
                  error={
                    error === true && message.keyName === e.name
                      ? message.error
                      : ''
                  }
                  onChange={(selectedValues) =>
                    handleMultiSelectChange(selectedValues, e.name)
                  }
                  value={
                    props.editMode.edit
                      ? JSON.parse(props.editMode[e.name])
                      : props.viewMode.view
                      ? JSON.parse(props.viewMode[e.name])
                      : props.formData[e.name]
                  }
                  disabled={props.viewMode.view}
                  withTags
                >
                  {e.options.map((option) => (
                    <MultiSelectOption key={option.value} value={option.value}>
                      {option.label}
                    </MultiSelectOption>
                  ))}
                </MultiSelect>
              </div>
            ) : e.type === DATE ? (
              <div style={{ margin: '1rem 0' }} key={e.name}>
                <TextInput
                  // @ts-ignore
                  label={e.label}
                  name={e.name}
                  id={e.name}
                  required
                  placeholder={e.placeholder}
                  type={e.type}
                  onChange={(event) =>
                    handleInputChange(event, { name: e.name })
                  }
                  value={
                    props.editMode.edit
                      ? props.editMode[e.name]
                      : props.viewMode.view
                      ? props.viewMode[e.name]
                      : props.formData[e.name]
                  }
                  disabled={props.viewMode.view}
                />
              </div>
            ) : null;
          })}
        </div>
      </ModalBody>

      {props.tabName === 'Requests' ? (
        <ModalFooter
          endActions={
            <div className="switch">
              <div
                className={
                  props.viewMode.isRequestApproved == 'Rejected'
                    ? 'rejected'
                    : 'approved'
                }
              >
                {props.viewMode.isRequestApproved}
              </div>
              <Switch
                label="Status"
                onChange={handleInputChange}
                selected={changeStatus}
              />
            </div>
          }
        ></ModalFooter>
      ) : !props.viewMode.view ? (
        <ModalFooter
          startActions={
            <Button onClick={handleCloseModal} variant="tertiary">
              Cancel
            </Button>
          }
          endActions={
            <Button
              type="submit"
              disabled={!props.editMode.edit && !allFieldsFilled()}
            >
              {props.editMode.edit === true ? 'Update' : 'Save'}
            </Button>
          }
        />
      ) : null}
    </ModalLayout>
  );
}
