import React, { useState, useEffect } from 'react';
import {
  Tabs,
  Tab,
  TabGroup,
  TabPanels,
  TabPanel,
  Box,
} from '@strapi/design-system';
import {
  ContentLayout,
  // @ts-ignore
} from '@strapi/design-system/Layout';
// @ts-ignore
import { EmptyStateLayout } from '@strapi/design-system/EmptyStateLayout';
import { Illo } from '../../components/Illo';
import Modal from '../../components/Modal';
import PmsTable from '../../components/Table';
import {
  TEXTAREA,
  TEXTINPUT,
  SELECT,
  PUT,
} from '../../../../constants/constant';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';

const TableTabs = (props) => {
  const [showModal, setShowModal] = useState(false);
  const [editMode, setEditMode] = useState({
    edit: false,
    id: '',
    comments: '',
    isRequested: '',
    isRequestApproved: '',
    version: '',
    createdAt: '',
  });

  const [formData, setFormData] = useState({
    comments: '',
    isRequested: '',
    isRequestApproved: '',
    version: '',
    createdAt: '',
    status: '',
  });

  const viewDetails = (view, value) => {
    props.setViewMode({
      view: view,
      id: value.id,
      comments: value.comments,
      isRequested: value.isRequested,
      isRequestApproved: value.isRequestApproved,
      version: value.version.id,
      createdAt: value.createdAt,
    });
  };

  const commentsTableHeadings = ['Comments', 'Version', 'Created At'];
  const requestsTableHeadings = ['Comments', 'Version', 'Status', 'Created At'];

  const versionOptions = props.commentData?.map((request) => ({
    value: request.version.id,
    label: request.version.versionName,
  }));

  const commentsInputs = [
    { inputType: 'comments' },
    { inputType: 'version' },
    { inputType: 'createdAt' },
  ];

  const requestsInputs = [
    { inputType: 'comments' },
    { inputType: 'version' },
    { inputType: 'isRequestApproved' },
    { inputType: 'createdAt' },
  ];

  const commentsModelInputs = [
    {
      type: TEXTAREA,
      label: 'Comments',
      name: 'comments',
      placeholder: '',
    },
    {
      type: TEXTINPUT,
      label: 'Requests',
      name: 'isRequested',
      placeholder: '',
    },
    {
      type: SELECT,
      label: 'Version',
      options: versionOptions,
      name: 'version',
      placeholder: '',
    },
    {
      type: TEXTINPUT,
      label: 'Created At',
      name: 'createdAt',
      placeholder: '',
    },
  ];

  const requestsModelInputs = [
    {
      type: TEXTAREA,
      label: 'Comments',
      name: 'comments',
      placeholder: '',
    },
    {
      type: TEXTINPUT,
      label: 'Requests',
      name: 'isRequested',
      placeholder: '',
    },
    {
      type: TEXTINPUT,
      label: 'Status',
      name: 'isRequestApproved',
      placeholder: '',
    },
    {
      type: SELECT,
      label: 'Version',
      options: versionOptions,
      name: 'version',
      placeholder: '',
    },
    {
      type: TEXTINPUT,
      label: 'Created At',
      name: 'createdAt',
      placeholder: '',
    },
  ];

  const changeStatus = async (id, data) => {
    try {
      await makeRequest(
        endpoints.UPDATE_STATUS(id),
        PUT,
        props.accessKey,
        data
      );
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    if (props.viewMode?.id) {
      changeStatus(props.viewMode.id, props.viewMode);
    }
  }, [props.viewMode]);

  let commentsData;
  let requestsData;
  if (props.searchParams.selectedVersion == '') {
    commentsData = props.commentData.filter((item) => !item.isRequested);
    requestsData = props.commentData.filter((item) => item.isRequested);
  } else {
    commentsData = props.commentData
      .filter((item) => !item.isRequested)
      .filter(
        (item) =>
          item.version.id.toString() === props.searchParams.selectedVersion
      );
    requestsData = props.commentData
      .filter((item) => item.isRequested)
      .filter(
        (item) =>
          item.version.id.toString() === props.searchParams.selectedVersion
      )
      .map((item) => ({
        ...item,
        isRequestApproved: item.isRequestApproved,
      }));
  }

  return (
    <Box padding={8} background="primary100">
      <TabGroup
        label="Version Comments and Requests"
        id="tabs"
        onTabChange={(selected) => console.log(selected)}
      >
        <Tabs>
          <Tab>Comments</Tab>
          <Tab>Requests</Tab>
        </Tabs>
        <TabPanels>
          <TabPanel>
            <Box color="neutral800" padding={4} background="neutral0">
              <ContentLayout>
                {commentsData?.length === 0 ? (
                  <EmptyStateLayout
                    icon={<Illo />}
                    content="No comments found."
                  />
                ) : (
                  <>
                    <PmsTable
                      setShowModal={setShowModal}
                      viewDetails={viewDetails}
                      title={'Version Comments and Requests'}
                      tableHeadings={commentsTableHeadings}
                      tableRowsData={commentsData}
                      projectInputs={commentsInputs}
                    />
                  </>
                )}
              </ContentLayout>
              {showModal && (
                <Modal
                  setShowModal={setShowModal}
                  editMode={editMode}
                  setEditMode={setEditMode}
                  viewMode={props.viewMode}
                  setViewMode={props.setViewMode}
                  modelInputs={commentsModelInputs}
                  title={'Version Comments and Requests'}
                  formData={formData}
                  setFormData={setFormData}
                />
              )}
            </Box>
          </TabPanel>
          <TabPanel>
            <Box color="neutral800" padding={4} background="neutral0">
              <ContentLayout>
                {requestsData?.length === 0 ? (
                  <EmptyStateLayout
                    icon={<Illo />}
                    content="No requests found."
                  />
                ) : (
                  <>
                    <PmsTable
                      setShowModal={setShowModal}
                      viewDetails={viewDetails}
                      title={'Version Comments and Requests'}
                      tableHeadings={requestsTableHeadings}
                      tableRowsData={requestsData}
                      projectInputs={requestsInputs}
                    />
                  </>
                )}
              </ContentLayout>
              {showModal && (
                <Modal
                  setShowModal={setShowModal}
                  editMode={editMode}
                  setEditMode={setEditMode}
                  viewMode={props.viewMode}
                  setViewMode={props.setViewMode}
                  modelInputs={requestsModelInputs}
                  title={'Version Comments and Requests'}
                  formData={formData}
                  setFormData={setFormData}
                  tabName={'Requests'}
                  setUpdatedStatus={props.setUpdatedStatus}
                />
              )}
            </Box>
          </TabPanel>
        </TabPanels>
      </TabGroup>
    </Box>
  );
};

export default TableTabs;
