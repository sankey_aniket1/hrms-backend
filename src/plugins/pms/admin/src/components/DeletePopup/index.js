import React from 'react';
import './index.css';

const DeleteConfirmationPopup = ({ rowData, onDeleteConfirm, onCancel }) => {
  return (
    <div className="delete-confirmation-popup">
      <p>Are you sure you want to delete ?</p>
      <div className="button-container">
        <button
          onClick={(event) => {
            event.stopPropagation();
            onDeleteConfirm(rowData.id);
          }}
        >
          Yes
        </button>
        <button onClick={onCancel}>Cancel</button>
      </div>
    </div>
  );
};

export default DeleteConfirmationPopup;