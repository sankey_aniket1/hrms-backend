const pluginPermissions = {
  main: [{ action: 'plugin::pms.read', subject: null }],
};

export default pluginPermissions;