import { endpoints } from '../../../../constants/endpoints';

const makeRequest = async (endpoint, method, accessKey, data = null) => {
  const url = `${endpoints.API_BASE_URL}${endpoint}`;
  const options = {
    method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessKey}`,
    },
  };

  if (data) {
    options.body = JSON.stringify(data);
  }

  try {
    const response = await fetch(url, options);
    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(
        errorData.message || `HTTP error! status: ${response.status}`
      );
    }
    return await response.json();
  } catch (error) {
    console.error('Error making API call:', error);
    throw error;
  }
};

export default makeRequest;
