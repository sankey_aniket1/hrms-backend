import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';

const useAxios = (initialConfig = {}) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [config, setConfig] = useState(initialConfig);

  const fetchData = useCallback(async () => {
    setLoading(true);
    setError(null);
    try {
      const response = await axios(config);
      setData(response.data);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  }, [config]);

  const doFetch = (newConfig = {}) => {
    setConfig((prevConfig) => ({
      ...prevConfig,
      ...newConfig,
    }));
  };

  useEffect(() => {
    if (config.method === 'GET' || !config.method) {
      fetchData();
    }
  }, [fetchData, config]);

  return { data, loading, error, doFetch };
};

export default useAxios;
