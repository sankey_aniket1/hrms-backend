/*
 *
 * HomePage
 *
 */

import React, { memo, useState, useEffect } from 'react';
import {
  Layout,
  ContentLayout,
  HeaderLayout,
  // @ts-ignore
} from '@strapi/design-system/Layout';
// @ts-ignore
import { EmptyStateLayout } from '@strapi/design-system/EmptyStateLayout';
// @ts-ignore
import { Button } from '@strapi/design-system/Button';
// @ts-ignore
import Plus from '@strapi/icons/Plus';
import { Illo } from '../../components/Illo';
import Modal from '../../components/Modal';
import PmsTable from '../../components/Table';
import { Box, Link, Loader } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';
import {
  GET,
  POST,
  PUT,
  DELETE,
  TEXTINPUT,
  SELECT,
  TEXTAREA,
  DATE,
} from '../../../../constants/constant';
import { useHistory } from 'react-router-dom';

const ProjectPage = (props) => {
  const history = useHistory();
  const [projectData, setProjectData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [isProjectAdded, setIsProjectAdded] = useState(false);
  const [editMode, setEditMode] = useState({
    edit: false,
    id: '',
    projectName: '',
    description: '',
    projectOwner: '',
    region: '',
    country: '',
    clientName: '',
    status: '',
    startDate: '',
    endDate: '',
  });
  const [viewMode, setViewMode] = useState({
    view: false,
    id: '',
    projectName: '',
    description: '',
    projectOwner: '',
    region: '',
    country: '',
    clientName: '',
    status: '',
    startDate: '',
    endDate: '',
  });
  const [formData, setFormData] = useState({
    projectName: '',
    description: '',
    projectOwner: '',
    region: '',
    country: '',
    clientName: '',
    status: '',
    startDate: '',
    endDate: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [getRegions, setGetRegions] = useState();
  const [getCountries, SetGetCountries] = useState();
  const [getEmployees, setGetEmployees] = useState([]);

  const viewDetails = (view, value) => {
    setViewMode({
      view: view,
      id: value.id,
      projectName: value.projectName,
      description: value.description,
      projectOwner: value.projectOwner,
      region: value.region,
      country: value.country,
      clientName: value.clientName,
      status: value.status,
      startDate: value.startDate,
      endDate: value.endDate,
    });
  };

  const changeEditValue = (edit, value) => {
    setEditMode({
      edit: edit,
      id: value.id,
      projectName: value.projectName,
      description: value.description,
      projectOwner: value.projectOwner,
      region: value.region,
      country: value.country,
      clientName: value.clientName,
      status: value.status,
      startDate: value.startDate,
      endDate: value.endDate,
    });
  };

  const tableHeadings = [
    'Project Name',
    'Project Owner',
    'Client Name',
    'Country',
    'Status',
    'Actions',
  ];

  const projectInputs = [
    { inputType: 'projectName' },
    { inputType: 'projectOwner' },
    { inputType: 'clientName' },
    { inputType: 'country' },
    { inputType: 'status' },
  ];

  const fetchRegions = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_REGIONS,
        GET,
        props.accessKey
      );
      setGetRegions(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchCountries = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_COUNTRIES,
        GET,
        props.accessKey
      );
      SetGetCountries(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchEmployees = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_EMPLOYEES,
        GET,
        props.accessKey
      );
      const Tr_List = [];
      data.map((el) => {
        if (el.isTeamLead === true) {
          Tr_List.push(el);
        }
      });
      setGetEmployees(Tr_List);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    fetchCountries();
    fetchRegions();
    fetchEmployees();
  }, []);

  const handleSelectChange = (selectedOption, actionMeta) => {
    const name = actionMeta.name;
    if (editMode.edit === true) {
      setEditMode({ ...editMode, [name]: selectedOption });
    } else {
      setFormData({ ...formData, [name]: selectedOption });
    }
  };

  // project modal data structuring
  const status = ['Active', 'Inactive', 'On Hold'];

  const ownerOptions = getEmployees.map((employees) => ({
    value: `${employees.firstName} ${employees.lastName}`,
    label: `${employees.firstName} ${employees.lastName}`,
  }));

  let regionOptions;
  if (getRegions && Array.isArray(getRegions.data)) {
    regionOptions = getRegions.data.map((region) => ({
      value: region.attributes.region,
      label: region.attributes.region,
    }));
  } else {
    console.error(
      'getRegions or getRegions.data is not defined or not an array'
    );
  }

  let countryOptions;
  if (getCountries && Array.isArray(getCountries.data)) {
    countryOptions = getCountries.data.map((country) => ({
      value: country.attributes.country,
      label: country.attributes.country,
    }));
  } else {
    console.error(
      'getCountries or getCountries.data is not defined or not an array'
    );
  }

  const statusOptions = status.map((status) => ({
    value: status,
    label: status,
  }));

  const modelInputs = [
    {
      type: TEXTINPUT,
      label: 'Project Name',
      name: 'projectName',
      placeholder: 'Enter the project name',
    },
    {
      type: TEXTAREA,
      label: 'Description',
      name: 'description',
      placeholder: 'Enter the description',
    },
    {
      type: SELECT,
      label: 'Project Owner',
      options: ownerOptions,
      name: 'projectOwner',
      placeholder: 'Select the project owner',
    },
    {
      type: TEXTINPUT,
      label: 'Client Name',
      name: 'clientName',
      placeholder: 'Enter the client name',
    },
    {
      type: SELECT,
      label: 'Region',
      options: regionOptions,
      name: 'region',
      placeholder: 'Select the region',
    },
    {
      type: SELECT,
      label: 'Country',
      options: countryOptions,
      name: 'country',
      placeholder: 'Select the country ',
    },
    {
      type: SELECT,
      label: 'Status',
      options: statusOptions,
      name: 'status',
      placeholder: 'Slecte the status',
    },
    {
      type: DATE,
      label: 'Start Date',
      name: 'startDate',
      placeholder: 'Select the start date',
    },
    {
      type: DATE,
      label: 'End Date',
      name: 'endDate',
      placeholder: 'Select the end date',
    },
  ];

  useEffect(() => {
    const fetchProjects = async () => {
      setIsLoading(true);
      try {
        const data = await makeRequest(
          endpoints.GET_ALL_PROJECTS,
          GET,
          props.accessKey
        );
        setProjectData(data);
      } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
      } finally {
        setIsLoading(false); // Set loading state to false regardless of success or failure
      }
    };
    fetchProjects();
  }, [isProjectAdded]);

  const addProject = async (data) => {
    try {
      setIsLoading(true);
      await makeRequest(endpoints.ADD_PROJECT, POST, props.accessKey, data);
      setIsProjectAdded((prev) => !prev); // Toggle isProjectAdded to trigger re-fetch
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const deleteProject = async (id) => {
    try {
      setIsLoading(true);
      await makeRequest(endpoints.DELETE_PROJECT(id), DELETE, props.accessKey);
      setIsProjectAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const updateProject = async (id, data) => {
    try {
      setIsLoading(true);
      await makeRequest(endpoints.EDIT_PROJECT(id), PUT, props.accessKey, data);
      setIsProjectAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  return !isLoading || props.accessKey == null ? (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault();
                history.goBack();
              }}
              startIcon={<ArrowLeft />}
            >
              Go back
            </Link>
          }
          primaryAction={
            <Button startIcon={<Plus />} onClick={() => setShowModal(true)}>
              Add New Project
            </Button>
          }
          title={'Projects'}
          subtitle={`${projectData.length} entries found`}
          as="h2"
        />
      </Box>

      <ContentLayout>
        {projectData?.length === 0 ? (
          <EmptyStateLayout
            icon={<Illo />}
            content="You don't have any projects yet..."
            action={
              <Button
                onClick={() => setShowModal(true)}
                variant="primary"
                startIcon={<Plus />}
              >
                Add your first project
              </Button>
            }
          />
        ) : (
          <>
            <PmsTable
              setShowModal={setShowModal}
              deleteTableData={deleteProject}
              editTableData={updateProject}
              changeEditValue={changeEditValue}
              viewDetails={viewDetails}
              title={'Add New Project'}
              tableHeadings={tableHeadings}
              tableRowsData={projectData}
              projectInputs={projectInputs}
            />
          </>
        )}
      </ContentLayout>
      {showModal && (
        <Modal
          setShowModal={setShowModal}
          editMode={editMode}
          editTableData={updateProject}
          setEditMode={setEditMode}
          addTableData={addProject}
          viewMode={viewMode}
          setViewMode={setViewMode}
          modelInputs={modelInputs}
          title={'Project'}
          formData={formData}
          setFormData={setFormData}
          handleSelectChange={handleSelectChange}
        />
      )}
    </Layout>
  ) : (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        alignContent: 'center',
        marginLeft: '30%',
      }}
    >
      <Loader />
    </div>
  );
};

export default memo(ProjectPage);
