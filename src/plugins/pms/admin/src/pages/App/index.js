/**
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import { AnErrorOccurred, CheckPagePermissions } from '@strapi/helper-plugin';
import pluginId from '../../pluginId';
import pluginPermissions from '../../permissions';
import ProjectPage from '../ProjectPage';
import ThreadPage from '../ThreadPage';
import SideMenu from '../../components/SideMenu';
import VersionPage from '../VersionPage';
import './index.css';
import VersionResourcePage from '../VersionResourcePage';
import VersionCommentPage from '../VersionCommentPage';
import { POST, GET, DELETE } from '../../../../constants/constant';
import { endpoints } from '../../../../constants/endpoints';
import {
  FULL_ACCESS,
  REQUEST_BODY_FACCESS,
} from '../../../../constants/constant';
import { Loader } from '@strapi/design-system';

const AccessKeyWrapper = ({ component: Component, accessKey, ...rest }) => (
  <Route
    {...rest}
    render={(props) => <Component {...props} accessKey={accessKey} />}
  />
);

const App = () => {
  const [accessKey, setAccessKey] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const jwtToken = sessionStorage.getItem('jwtToken')?.replace(/"/g, '');
  const loggedInUser = JSON.parse(localStorage.getItem('userInfo'));

  const fetchUsers = async () => {
    try {
      const response = await fetch(
        `${endpoints.API_BASE_URL}${endpoints.GET_USER_ROLES}`,
        {
          method: GET,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      const data = await response.json();
      return data.data.results;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchTokenData = async () => {
    try {
      const response = await fetch(
        `${endpoints.API_BASE_URL}${endpoints.GET_ACCESS_TOKEN}`,
        {
          method: GET,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      const data = await response.json();
      return data.data;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const createAccessKey = async () => {
    try {
      const response = await fetch(
        `${endpoints.API_BASE_URL}${endpoints.CREATE_ACCESS_TOKEN}`,
        {
          method: POST,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${jwtToken}`,
          },
          body: JSON.stringify(REQUEST_BODY_FACCESS),
        }
      );
      const responseData = await response.json();
      if (responseData.data?.accessKey) {
        setAccessKey(responseData.data.accessKey);
      } else {
        console.log('Access key generation is one-time only');
      }
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const createToken = async (users, tokenData) => {
    const userRole = users
      ?.find((user) => user.id === loggedInUser.id)
      ?.roles.find((role) => role.name === 'Super Admin');

    const fullAccessToken = tokenData.filter(
      (token) => token.type === FULL_ACCESS
    );

    if (userRole && userRole.length !== 0) {
      if (fullAccessToken.length === 0) {
        await createAccessKey();
      } else {
        try {
          const response = await fetch(
            `${endpoints.API_BASE_URL}${endpoints.DELETE_TOKEN(
              fullAccessToken[0].id
            )}`,
            {
              method: DELETE,
              headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${jwtToken}`,
              },
              body: JSON.stringify(REQUEST_BODY_FACCESS),
            }
          );
          const responseData = await response.json();
        } catch (error) {
          console.error('There was a problem with the fetch operation:', error);
        }
        await createAccessKey();
      }
      setIsLoading(false);
    } else {
      console.log('Access key is already present');
    }
  };

  useEffect(() => {
    fetchUsers().then(async (users) => {
      let tokenData = await fetchTokenData();
      createToken(users, tokenData);
    });
  }, []);

  return isLoading ? (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        alignContent: 'center',
        marginLeft: '30%',
      }}
    >
      <Loader />
    </div>
  ) : (
    <CheckPagePermissions permissions={pluginPermissions.main}>
      <div className="myCustomLayout">
        <div className="header">
          <SideMenu />
        </div>
        <div className="emptyScreen">
          <Switch>
            <AccessKeyWrapper
              path={`/plugins/${pluginId}`}
              exact
              component={ProjectPage}
              accessKey={accessKey}
            />
            <AccessKeyWrapper
              path={`/plugins/${pluginId}/threads`}
              component={ThreadPage}
              exact
              accessKey={accessKey}
            />
            <AccessKeyWrapper
              path={`/plugins/${pluginId}/versions`}
              component={VersionPage}
              exact
              accessKey={accessKey}
            />
            <AccessKeyWrapper
              path={`/plugins/${pluginId}/version-resources`}
              component={VersionResourcePage}
              exact
              accessKey={accessKey}
            />
            <AccessKeyWrapper
              path={`/plugins/${pluginId}/version-comments-requests`}
              component={VersionCommentPage}
              exact
              accessKey={accessKey}
            />
            <Route component={AnErrorOccurred} />
          </Switch>
        </div>
      </div>
    </CheckPagePermissions>
  );
};

export default App;
