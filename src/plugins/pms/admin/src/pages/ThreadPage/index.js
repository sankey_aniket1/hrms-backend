/*
 *
 * HomePage
 *
 */

import React, { memo, useState, useEffect } from 'react';
import {
  Layout,
  HeaderLayout,
  ContentLayout,
  // @ts-ignore
} from '@strapi/design-system/Layout';
// @ts-ignore
import { EmptyStateLayout } from '@strapi/design-system/EmptyStateLayout';
// @ts-ignore
import { Button } from '@strapi/design-system/Button';
// @ts-ignore
import Plus from '@strapi/icons/Plus';
import { Illo } from '../../components/Illo';
import Modal from '../../components/Modal';
import PmsTable from '../../components/Table';
import { Box, Link, Loader } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';
import {
  GET,
  POST,
  DELETE,
  PUT,
  SELECT,
  TEXTAREA,
  TEXTINPUT,
  MULTISELECT,
} from '../../../../constants/constant';
import { useHistory } from 'react-router-dom';

const ThreadPage = (props) => {
  const history = useHistory();
  const [threadData, setThreadData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [isThreadAdded, setIsThreadAdded] = useState(false);

  const [editMode, setEditMode] = useState({
    edit: false,
    id: '',
    threadName: '',
    threadLead: '',
    description: '',
    domainName: '',
    techStack: '',
    type: '',
    project: '',
  });

  const [viewMode, setViewMode] = useState({
    view: false,
    id: '',
    threadName: '',
    threadLead: '',
    description: '',
    domainName: '',
    techStack: '',
    type: '',
    project: '',
  });

  const [formData, setFormData] = useState({
    threadName: '',
    threadLead: '',
    description: '',
    domainName: '',
    techStack: '',
    type: '',
    project: '',
  });

  const [projectsForThread, setProjectsForThread] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [getEmployees, setGetEmployees] = useState([]);
  const [getTechStack, setGetTechStack] = useState([]);

  const viewDetails = (view, value) => {
    setViewMode({
      view: view,
      id: value.id,
      threadName: value.threadName,
      threadLead: value.threadLead,
      description: value.description,
      domainName: value.domainName,
      techStack: value.techStack,
      type: value.type,
      project: value.project.id,
    });
  };

  const changeEditValue = (edit, value) => {
    setEditMode({
      edit: edit,
      id: value.id,
      threadName: value.threadName,
      threadLead: value.threadLead,
      description: value.description,
      domainName: value.domainName,
      techStack: value.techStack,
      type: value.type,
      project: value.project.id,
    });
  };

  const tableHeadings = [
    'Thread Name',
    'Thread Lead',
    'Project',
    'Domain',
    'Type',
    'Actions',
  ];

  const threadInputs = [
    { inputType: 'threadName' },
    { inputType: 'threadLead' },
    { inputType: 'project' },
    { inputType: 'domainName' },
    { inputType: 'type' },
  ];

  const fetchProjects = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_PROJECTS,
        GET,
        props.accessKey
      );
      setProjectsForThread(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchEmployees = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_EMPLOYEES,
        GET,
        props.accessKey
      );
      setGetEmployees(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchTechStacks = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_TECH_STACKS,
        GET,
        props.accessKey
      );
      setGetTechStack(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    fetchProjects();
    fetchEmployees();
    fetchTechStacks();
  }, []);

  const handleSelectChange = (selectedOption, actionMeta) => {
    const name = actionMeta.name;
    if (editMode.edit === true) {
      setEditMode({ ...editMode, [name]: selectedOption });
    } else {
      setFormData({ ...formData, [name]: selectedOption });
    }
  };

  // thread modal data structuring
  const types = ['Fixed', 'Recurring'];

  const typeOptions = types.map((type) => ({ value: type, label: type }));

  const projectOptions = projectsForThread?.map((project) => ({
    value: project.id,
    label: project.projectName,
  }));

  const employeesOptions = getEmployees.map((employees) => ({
    value: `${employees.firstName} ${employees.lastName}`,
    label: `${employees.firstName} ${employees.lastName}`,
  }));

  let techStackOptions;
  if (getTechStack && Array.isArray(getTechStack.data)) {
    techStackOptions = getTechStack.data.map((techStack) => ({
      value: techStack.attributes.techStack,
      label: techStack.attributes.techStack,
    }));
  } else {
    console.error(
      'getTechStack or getTechStack.data is not defined or not an array'
    );
  }

  const modelInputs = [
    {
      type: TEXTINPUT,
      label: 'Thread Name',
      name: 'threadName',
      placeholder: 'Enter the thread name',
    },
    {
      type: SELECT,
      label: 'Thread Lead',
      options: employeesOptions,
      name: 'threadLead',
      placeholder: 'Select the thread lead',
    },
    {
      type: TEXTAREA,
      label: 'Description',
      name: 'description',
      placeholder: 'Enter the description',
    },
    {
      type: TEXTINPUT,
      label: 'Domain Name',
      name: 'domainName',
      placeholder: 'Enter the domain name',
    },
    {
      type: MULTISELECT,
      label: 'Tech Stack',
      options: techStackOptions,
      name: 'techStack',
      placeholder: 'Select the tech stack',
    },
    {
      type: SELECT,
      label: 'Type',
      options: typeOptions,
      name: 'type',
      placeholder: 'Select the type',
    },
    {
      type: SELECT,
      label: 'Project',
      options: projectOptions,
      name: 'project',
      placeholder: 'Select the project',
    },
  ];

  useEffect(() => {
    const fetchThreads = async () => {
      setIsLoading(true);
      try {
        const data = await makeRequest(
          endpoints.GET_ALL_THREADS,
          GET,
          props.accessKey
        );
        setThreadData(data);
      } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
      } finally {
        setIsLoading(false); // Set loading state to false regardless of success or failure
      }
    };
    fetchThreads();
  }, [isThreadAdded]);

  const addThread = async (data) => {
    const result = { ...data, ['techStack']: JSON.stringify(data.techStack) };
    try {
      setIsLoading(true);
      await makeRequest(endpoints.ADD_THREAD, POST, props.accessKey, result);
      setIsThreadAdded((prev) => !prev); // Toggle isThreadAdded to trigger re-fetch
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const deleteThread = async (id) => {
    try {
      setIsLoading(true);
      await makeRequest(endpoints.DELETE_THREAD(id), DELETE, props.accessKey);
      setIsThreadAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const updateThread = async (id, data) => {
    try {
      setIsLoading(true);
      await makeRequest(endpoints.EDIT_THREAD(id), PUT, props.accessKey, data);
      setIsThreadAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  return !isLoading ? (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault();
                history.goBack();
              }}
              startIcon={<ArrowLeft />}
            >
              Go back
            </Link>
          }
          primaryAction={
            <Button startIcon={<Plus />} onClick={() => setShowModal(true)}>
              Add New Thread
            </Button>
          }
          title={'Threads'}
          subtitle={`${threadData.length} entries found`}
          as="h2"
        />
      </Box>

      <ContentLayout>
        {threadData?.length === 0 ? (
          <EmptyStateLayout
            icon={<Illo />}
            content="You don't have any threads yet..."
            action={
              <Button
                onClick={() => setShowModal(true)}
                variant="primary"
                startIcon={<Plus />}
              >
                Add your first thread
              </Button>
            }
          />
        ) : (
          <>
            <PmsTable
              setShowModal={setShowModal}
              deleteTableData={deleteThread}
              editTableData={updateThread}
              changeEditValue={changeEditValue}
              viewDetails={viewDetails}
              title={'Add New Thread'}
              tableHeadings={tableHeadings}
              tableRowsData={threadData}
              projectInputs={threadInputs}
            />
          </>
        )}
      </ContentLayout>
      {showModal && (
        <Modal
          setShowModal={setShowModal}
          editMode={editMode}
          editTableData={updateThread}
          setEditMode={setEditMode}
          addTableData={addThread}
          viewMode={viewMode}
          setViewMode={setViewMode}
          modelInputs={modelInputs}
          title={'Thread'}
          formData={formData}
          setFormData={setFormData}
          handleSelectChange={handleSelectChange}
        />
      )}
    </Layout>
  ) : (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        alignContent: 'center',
        marginLeft: '30%',
      }}
    >
      <Loader />
    </div>
  );
};

export default memo(ThreadPage);
