/*
 *
 * HomePage
 *
 */

import React, { memo, useState, useEffect } from 'react';
import {
  Layout,
  HeaderLayout,
  ContentLayout,
  // @ts-ignore
} from '@strapi/design-system/Layout';
// @ts-ignore
import { EmptyStateLayout } from '@strapi/design-system/EmptyStateLayout';
// @ts-ignore
import { Button } from '@strapi/design-system/Button';
// @ts-ignore
import Plus from '@strapi/icons/Plus';
import { Illo } from '../../components/Illo';
import Modal from '../../components/Modal';
import PmsTable from '../../components/Table';
import { Box, Link, Loader } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';
import {
  GET,
  POST,
  PUT,
  DELETE,
  SELECT,
  TEXTAREA,
  TEXTINPUT,
  DATE,
} from '../../../../constants/constant';
import { useHistory } from 'react-router-dom';

const VersionPage = (props) => {
  const history = useHistory();
  const [versionData, setVersionData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [isVersionAdded, setIsVersionAdded] = useState(false);

  const [editMode, setEditMode] = useState({
    edit: false,
    id: '',
    versionName: '',
    description: '',
    status: '',
    startDate: '',
    endDate: '',
    isDelayed: '',
    thread: '',
    projectName: '',
  });

  const [viewMode, setViewMode] = useState({
    view: false,
    id: '',
    versionName: '',
    description: '',
    status: '',
    startDate: '',
    endDate: '',
    isDelayed: '',
    thread: '',
    projectName: '',
  });

  const [formData, setFormData] = useState({
    versionName: '',
    description: '',
    status: '',
    startDate: '',
    endDate: '',
    isDelayed: '',
    thread: '',
    projectName: '',
  });

  const [threadsForVersion, setThreadsForVersion] = useState([]);
  const [projectsForVersion, setProjectsForVersion] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const viewDetails = (view, value) => {
    setViewMode({
      view: view,
      id: value.id,
      versionName: value.versionName,
      description: value.description,
      status: value.status,
      startDate: value.startDate,
      endDate: value.endDate,
      isDelayed: value.isDelayed,
      thread: value.thread.id,
      projectName: value.projectName,
    });
  };

  const changeEditValue = (edit, value) => {
    setEditMode({
      edit: edit,
      id: value.id,
      versionName: value.versionName,
      description: value.description,
      status: value.status,
      startDate: value.startDate,
      endDate: value.endDate,
      isDelayed: value.isDelayed,
      thread: value.thread.id,
      projectName: value.projectName,
    });
  };

  const tableHeadings = [
    'Version Name',
    'Projects',
    'Threads',
    'Status',
    'Is Delayed',
    'Start Date',
    'End Date',
    'Actions',
  ];

  const versionInputs = [
    { inputType: 'versionName' },
    { inputType: 'projectName' },
    { inputType: 'thread' },
    { inputType: 'status' },
    { inputType: 'isDelayed' },
    { inputType: 'startDate' },
    { inputType: 'endDate' },
  ];

  const fetchProjects = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_PROJECTS,
        GET,
        props.accessKey
      );
      setProjectsForVersion(data);
      return data;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchThreads = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_THREADS,
        GET,
        props.accessKey
      );
      setThreadsForVersion(data);
      return data;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    fetchProjects();
    fetchThreads();
  }, []);

  const handleSelectChange = (selectedOption, actionMeta) => {
    const name = actionMeta.name;
    if (name === 'projectName') {
      fetchThreads().then((allThreads) => {
        const threadFilter = allThreads.filter(
          (e) => e.project.projectName === selectedOption
        );
        setThreadsForVersion(threadFilter);
        setFormData({
          ...formData,
          thread: '',
          [name]: selectedOption,
        });
      });

      if (editMode.edit === true) {
        setEditMode({ ...editMode, [name]: selectedOption });
      } else {
        setFormData({ ...formData, [name]: selectedOption });
      }
    } else {
      if (editMode.edit === true) {
        setEditMode({ ...editMode, [name]: selectedOption });
      } else {
        setFormData({ ...formData, [name]: selectedOption });
      }
    }
  };

  // version modal data structuring
  const status = ['QA', 'Dev', 'Staging', 'Prod'];
  const isDelayed = ['Yes', 'No'];

  const statusOptions = status.map((status) => ({
    value: status,
    label: status,
  }));

  const threadOptions = threadsForVersion?.map((thread) => ({
    value: thread.id,
    label: thread.threadName,
  }));

  const isDelayedOptions = isDelayed.map((isDelayed) => ({
    value: isDelayed,
    label: isDelayed,
  }));

  const projectOptions = projectsForVersion.map((project) => ({
    value: project.projectName,
    label: project.projectName,
  }));

  const modelInputs = [
    {
      type: TEXTINPUT,
      label: 'Version Name',
      name: 'versionName',
      placeholder: 'Enter the version name',
    },
    {
      type: TEXTAREA,
      label: 'Description',
      name: 'description',
      placeholder: 'Enter the description',
    },
    {
      type: SELECT,
      label: 'Projects',
      options: projectOptions,
      name: 'projectName',
      placeholder: 'Select the project name',
    },
    {
      type: SELECT,
      label: 'Threads',
      options: threadOptions,
      name: 'thread',
      placeholder: 'Select the thread name',
      disabled: formData.projectName == '' ? true : false,
    },
    {
      type: SELECT,
      label: 'Status',
      options: statusOptions,
      name: 'status',
      placeholder: 'Select the status',
    },
    {
      type: SELECT,
      label: 'Is Delayed',
      options: isDelayedOptions,
      name: 'isDelayed',
      placeholder: 'Is your verion delayed ?',
    },
    {
      type: DATE,
      label: 'Start Date',
      name: 'startDate',
      placeholder: 'Select the start date',
    },
    {
      type: DATE,
      label: 'End Date',
      name: 'endDate',
      placeholder: 'Select the end date',
    },
  ];

  useEffect(() => {
    const fetchVersions = async () => {
      setIsLoading(true);
      try {
        const data = await makeRequest(
          endpoints.GET_ALL_VERSIONS,
          GET,
          props.accessKey
        );
        data.map((e) => {
          if (e.isDelayed === 'true') {
            e.isDelayed = 'Yes';
          } else {
            e.isDelayed = 'No';
          }
        });
        setVersionData(data);
      } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
      } finally {
        setIsLoading(false); // Set loading state to false regardless of success or failure
      }
    };
    fetchVersions();
  }, [isVersionAdded]);

  const addVersion = async (data) => {
    try {
      let newData;
      if (data.isDelayed === 'Yes') {
        newData = { ...data, isDelayed: 'true' };
      } else {
        newData = { ...data, isDelayed: 'false' };
      }
      setIsLoading(true);
      await makeRequest(endpoints.ADD_VERSION, POST, props.accessKey, newData);
      setIsVersionAdded((prev) => !prev); // Toggle isVersionAdded to trigger re-fetch
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const deleteVersion = async (id) => {
    try {
      setIsLoading(true);
      await makeRequest(endpoints.DELETE_VERSION(id), DELETE, props.accessKey);
      setIsVersionAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const updateVersion = async (id, data) => {
    try {
      let newData;
      if (data.isDelayed === 'Yes') {
        newData = { ...data, isDelayed: 'true' };
      } else {
        newData = { ...data, isDelayed: 'false' };
      }
      setIsLoading(true);
      await makeRequest(
        endpoints.EDIT_VERSION(id),
        PUT,
        props.accessKey,
        newData
      );
      setIsVersionAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  return !isLoading ? (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault();
                history.goBack();
              }}
              startIcon={<ArrowLeft />}
            >
              Go back
            </Link>
          }
          primaryAction={
            <Button startIcon={<Plus />} onClick={() => setShowModal(true)}>
              Add New Version
            </Button>
          }
          title={'Versions'}
          subtitle={`${versionData.length} entries found`}
          as="h2"
        />
      </Box>

      <ContentLayout>
        {versionData?.length === 0 ? (
          <EmptyStateLayout
            icon={<Illo />}
            content="You don't have any threads yet..."
            action={
              <Button
                onClick={() => setShowModal(true)}
                variant="primary"
                startIcon={<Plus />}
              >
                Add your first version
              </Button>
            }
          />
        ) : (
          <>
            <PmsTable
              setShowModal={setShowModal}
              deleteTableData={deleteVersion}
              editTableData={updateVersion}
              changeEditValue={changeEditValue}
              viewDetails={viewDetails}
              title={'Add New Version'}
              tableHeadings={tableHeadings}
              tableRowsData={versionData}
              projectInputs={versionInputs}
            />
          </>
        )}
      </ContentLayout>
      {showModal && (
        <Modal
          setShowModal={setShowModal}
          editMode={editMode}
          editTableData={updateVersion}
          setEditMode={setEditMode}
          addTableData={addVersion}
          viewMode={viewMode}
          setViewMode={setViewMode}
          modelInputs={modelInputs}
          title={'Version'}
          formData={formData}
          setFormData={setFormData}
          handleSelectChange={handleSelectChange}
        />
      )}
    </Layout>
  ) : (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        alignContent: 'center',
        marginLeft: '30%',
      }}
    >
      <Loader />
    </div>
  );
};

export default memo(VersionPage);
