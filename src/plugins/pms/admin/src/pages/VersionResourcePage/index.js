import React, { memo, useState, useEffect, version } from 'react';
import {
  Layout,
  HeaderLayout,
  ContentLayout,
  // @ts-ignore
} from '@strapi/design-system/Layout';
// @ts-ignore
import { EmptyStateLayout } from '@strapi/design-system/EmptyStateLayout';
// @ts-ignore
import { Button } from '@strapi/design-system/Button';
// @ts-ignore
import Plus from '@strapi/icons/Plus';
import { Illo } from '../../components/Illo';
import Modal from '../../components/Modal';
import PmsTable from '../../components/Table';
import { Box, Link, Loader } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';
import {
  GET,
  POST,
  PUT,
  DELETE,
  DATE,
  SELECT,
} from '../../../../constants/constant';
import { useHistory } from 'react-router-dom';

const VersionResourcePage = (props) => {
  const history = useHistory();
  const [versionResourceData, setVersionResourceData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [isVersionResourceAdded, setIsVersionResourceAdded] = useState(false);
  const [editMode, setEditMode] = useState({
    edit: false,
    id: '',
    projectName: '',
    threadName: '',
    versionDls: '',
    employee: '',
    billable: '',
    type: '',
    startDate: '',
    endDate: '',
  });
  const [viewMode, setViewMode] = useState({
    view: false,
    id: '',
    projectName: '',
    threadName: '',
    versionDls: '',
    employee: '',
    billable: '',
    type: '',
    startDate: '',
    endDate: '',
  });

  const [formData, setFormData] = useState({
    projectName: '',
    threadName: '',
    versionDls: '',
    employee: '',
    billable: '',
    type: '',
    startDate: '',
    endDate: '',
  });

  const [projectsForVersionResource, setProjectsForVersionResource] = useState(
    []
  );
  const [threadsForVersionResource, setThreadsForVersionResource] = useState(
    []
  );
  const [versionsForVersionResource, setVersionsForVersionResource] = useState(
    []
  );
  const [getEmployees, setGetEmployees] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const viewDetails = (view, value) => {
    setViewMode({
      view: view,
      id: value.id,
      projectName: value.projectName,
      threadName: value.threadName,
      versionDls: value.versionDls[0].id,
      employee: value.employee.id,
      billable: value.billable,
      type: value.type,
      startDate: value.startDate,
      endDate: value.endDate,
    });
  };

  const changeEditValue = (edit, value) => {
    setEditMode({
      edit: edit,
      id: value.id,
      projectName: value.projectName,
      threadName: value.threadName,
      versionDls: value.versionDls[0].id,
      employee: value.employee.id,
      billable: value.billable,
      type: value.type,
      startDate: value.startDate,
      endDate: value.endDate,
    });
  };

  const tableHeadings = [
    'Project Name',
    'Thread Name',
    'Version Name',
    'Employee Name',
    'Billable',
    'Type',
    'Start Date',
    'Actions',
  ];

  const threadInputs = [
    { inputType: 'projectName' },
    { inputType: 'threadName' },
    { inputType: 'versionDls' },
    { inputType: 'employee' },
    { inputType: 'billable' },
    { inputType: 'type' },
    { inputType: 'startDate' },
  ];

  const fetchProjects = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_PROJECTS,
        GET,
        props.accessKey
      );
      setProjectsForVersionResource(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchThreads = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_THREADS,
        GET,
        props.accessKey
      );
      setThreadsForVersionResource(data);
      return data;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchVersions = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_VERSIONS,
        GET,
        props.accessKey
      );
      setVersionsForVersionResource(data);
      return data;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchEmployees = async () => {
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_EMPLOYEES,
        GET,
        props.accessKey
      );
      setGetEmployees(data);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  useEffect(() => {
    fetchProjects();
    fetchEmployees();
    fetchThreads();
    fetchVersions();
  }, []);

  const handleSelectChange = (selectedOption, actionMeta) => {
    const name = actionMeta.name;
    if (name === 'projectName') {
      fetchThreads().then((allThreads) => {
        const threadFilter = allThreads.filter(
          (e) => e.project.projectName === selectedOption
        );
        setThreadsForVersionResource(threadFilter);
        setFormData({
          ...formData,
          threadName: '',
          versionDls: '',
          [name]: selectedOption,
        });
      });
    } else if (name === 'threadName') {
      fetchVersions().then((allVersions) => {
        const versionFilter = allVersions.filter(
          (e) => e.thread.threadName === selectedOption
        );
        setVersionsForVersionResource(versionFilter);
        setFormData({ ...formData, versionDls: '', [name]: selectedOption });
      });
    } else {
      if (editMode.edit === true) {
        setEditMode({ ...editMode, [name]: selectedOption });
      } else {
        setFormData({ ...formData, [name]: selectedOption });
      }
    }
  };

  // version resource modal data structuring
  const types = ['Fixed', 'Assessment', 'PIP', 'Removed'];
  const billable = ['Yes', 'No'];

  const typesOptions = types.map((type) => ({
    value: type,
    label: type,
  }));

  const employeesOptions = getEmployees.map((employees) => ({
    value: employees.id,
    label: `${employees.firstName} ${employees.lastName}`,
  }));

  const billableOptions = billable.map((billable) => ({
    value: billable,
    label: billable,
  }));

  const projectOptions = projectsForVersionResource?.map((project) => ({
    value: project.projectName,
    label: project.projectName,
  }));

  const threadOptions = threadsForVersionResource?.map((thread) => ({
    value: thread.threadName,
    label: thread.threadName,
  }));

  const versionOptions = versionsForVersionResource?.map((versions) => ({
    value: versions.id,
    label: versions.versionName,
  }));

  const modelInputs = [
    {
      type: SELECT,
      label: 'Project Name',
      options: projectOptions,
      name: 'projectName',
      placeholder: 'Select the project name',
    },
    {
      type: SELECT,
      label: 'Thread Name',
      options: threadOptions,
      name: 'threadName',
      placeholder: 'Select the thread name',
      disabled: formData.projectName == '' ? true : false,
    },
    {
      type: SELECT,
      label: 'Versions',
      options: versionOptions,
      name: 'versionDls',
      placeholder: 'Select the version name',
      disabled: formData.threadName == '' ? true : false,
    },
    {
      type: SELECT,
      label: 'Employee Name',
      options: employeesOptions,
      name: 'employee',
      placeholder: 'Select the employee name',
    },
    {
      type: SELECT,
      label: 'Billable',
      options: billableOptions,
      name: 'billable',
      placeholder: 'Select the billable status',
    },
    {
      type: DATE,
      label: 'Start Date',
      name: 'startDate',
      placeholder: 'Select the start date',
    },
    {
      type: DATE,
      label: 'End Date',
      name: 'endDate',
      placeholder: 'Select the end date',
    },
    {
      type: SELECT,
      label: 'Type',
      options: typesOptions,
      name: 'type',
      placeholder: 'Select the type',
    },
  ];

  useEffect(() => {
    const fetchVersionResources = async () => {
      setIsLoading(true);
      try {
        const data = await makeRequest(
          endpoints.GET_ALL_VERSION_RESOURCES,
          GET,
          props.accessKey
        );
        data.map((e) => {
          if (e.billable === 'true') {
            e.billable = 'Yes';
          } else {
            e.billable = 'No';
          }
        });
        setVersionResourceData(data);
      } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
      } finally {
        setIsLoading(false); // Set loading state to false regardless of success or failure
      }
    };
    fetchVersionResources();
  }, [isVersionResourceAdded]);

  const addVersionResource = async (data) => {
    try {
      setIsLoading(true);
      let newData;
      if (data.billable === 'Yes') {
        newData = { ...data, billable: 'true' };
      } else {
        newData = { ...data, billable: 'false' };
      }
      await makeRequest(
        endpoints.ADD_VERSION_RESOURCE,
        POST,
        props.accessKey,
        newData
      );
      setIsVersionResourceAdded((prev) => !prev); // Toggle isVersionResourcesAdded to trigger re-fetch
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const deleteVersionResource = async (id) => {
    try {
      setIsLoading(true);
      await makeRequest(
        endpoints.DELETE_VERSION_RESOURCE(id),
        DELETE,
        props.accessKey
      );
      setIsVersionResourceAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const updateVersionResource = async (id, data) => {
    try {
      setIsLoading(true);
      let newData;
      if (data.isDelayed === 'Yes') {
        newData = { ...data, billable: 'true' };
      } else {
        newData = { ...data, billable: 'false' };
      }
      await makeRequest(
        endpoints.EDIT_VERSION_RESOURCE(id),
        PUT,
        props.accessKey,
        newData
      );
      setIsVersionResourceAdded((prev) => !prev);
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  return !isLoading ? (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault();
                history.goBack();
              }}
              startIcon={<ArrowLeft />}
            >
              Go back
            </Link>
          }
          primaryAction={
            <Button startIcon={<Plus />} onClick={() => setShowModal(true)}>
              Add New Version Resources
            </Button>
          }
          title={'Version Resources'}
          subtitle={`${versionResourceData.length} entries found`}
          as="h2"
        />
      </Box>

      <ContentLayout>
        {versionResourceData?.length === 0 ? (
          <EmptyStateLayout
            icon={<Illo />}
            content="You don't have any version resources yet..."
            action={
              <Button
                onClick={() => setShowModal(true)}
                variant="primary"
                startIcon={<Plus />}
              >
                Add your first version resources
              </Button>
            }
          />
        ) : (
          <>
            <PmsTable
              setShowModal={setShowModal}
              deleteTableData={deleteVersionResource}
              editTableData={updateVersionResource}
              changeEditValue={changeEditValue}
              viewDetails={viewDetails}
              title={'Add New Version Resource'}
              tableHeadings={tableHeadings}
              tableRowsData={versionResourceData}
              projectInputs={threadInputs}
            />
          </>
        )}
      </ContentLayout>
      {showModal && (
        <Modal
          setShowModal={setShowModal}
          editMode={editMode}
          editTableData={updateVersionResource}
          setEditMode={setEditMode}
          addTableData={addVersionResource}
          viewMode={viewMode}
          setViewMode={setViewMode}
          modelInputs={modelInputs}
          title={'Version Resource'}
          formData={formData}
          setFormData={setFormData}
          handleSelectChange={handleSelectChange}
        />
      )}
    </Layout>
  ) : (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        alignContent: 'center',
        marginLeft: '30%',
      }}
    >
      <Loader />
    </div>
  );
};

export default memo(VersionResourcePage);
