import React, { memo, useState, useEffect } from 'react';
// @ts-ignore
import { Layout, HeaderLayout } from '@strapi/design-system/Layout';
import { Box, Link, Loader } from '@strapi/design-system';
import { ArrowLeft } from '@strapi/icons';
import TableTabs from '../../components/Tabs';
import { endpoints } from '../../../../constants/endpoints';
import makeRequest from '../../services/apis';
import { GET } from '../../../../constants/constant';
import { useHistory } from 'react-router-dom';
import FilterComponent from '../../components/Filter';

const VersionCommentPage = (props) => {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [commentData, setCommentData] = useState([]);
  const [viewMode, setViewMode] = useState({
    view: false,
    id: '',
    comments: '',
    isRequested: '',
    isRequestApproved: '',
    version: '',
    createdAt: '',
  });
  const [updatedStatus, setUpdatedStatus] = useState(false);
  const [searchParams, setSearchParams] = useState({
    selectedProject: '',
    selectedThread: '',
    selectedVersion: '',
  });

  const fetchCommentsAndRequests = async () => {
    setIsLoading(true);
    try {
      const data = await makeRequest(
        endpoints.GET_ALL_COMMENTS,
        GET,
        props.accessKey
      );
      setCommentData(data || []);
    } catch (error) {
      console.error('Error fetching comments:', error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchCommentsAndRequests();
  }, [updatedStatus]);

  return !isLoading ? (
    <Layout>
      <Box background="neutral100">
        <HeaderLayout
          navigationAction={
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault();
                history.goBack();
              }}
              startIcon={<ArrowLeft />}
            >
              Go back
            </Link>
          }
          title={'Version Comments and Requests'}
          subtitle={`${commentData.length} entries found`}
          as="h2"
        />
      </Box>
      <FilterComponent setSearchParams={setSearchParams} />
      <TableTabs
        commentData={commentData}
        setViewMode={setViewMode}
        setUpdatedStatus={setUpdatedStatus}
        searchParams={searchParams}
        viewMode={viewMode}
      />
    </Layout>
  ) : (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        alignContent: 'center',
        marginLeft: '30%',
      }}
    >
      <Loader />
    </div>
  );
};

export default memo(VersionCommentPage);
