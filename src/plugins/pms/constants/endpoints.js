require('dotenv').config();

export const endpoints = {
  API_BASE_URL: process.env.URL,

  //for projects
  GET_ALL_PROJECTS: '/pms/find',
  ADD_PROJECT: '/pms/create',
  EDIT_PROJECT: (id) => `/pms/update/${id}`,
  DELETE_PROJECT: (id) => `/pms/delete/${id}`,
  GET_REGIONS: '/api/regions',
  GET_COUNTRIES: '/api/countries',

  //for threads
  GET_ALL_THREADS: '/pms/thread/findAll',
  ADD_THREAD: '/pms/thread/create',
  EDIT_THREAD: (id) => `/pms/thread/update/${id}`,
  DELETE_THREAD: (id) => `/pms/thread/delete/${id}`,
  GET_TECH_STACKS: '/api/tech-stacks',

  //for versions
  GET_ALL_VERSIONS: '/pms/version/findAll',
  ADD_VERSION: '/pms/version/create',
  EDIT_VERSION: (id) => `/pms/version/update/${id}`,
  DELETE_VERSION: (id) => `/pms/version/delete/${id}`,

  //for version resources
  GET_ALL_VERSION_RESOURCES: '/pms/version-resource/findAll',
  ADD_VERSION_RESOURCE: '/pms/version-resource/create',
  EDIT_VERSION_RESOURCE: (id) => `/pms/version-resource/update/${id}`,
  DELETE_VERSION_RESOURCE: (id) => `/pms/version-resource/delete/${id}`,

  //for employees
  GET_ALL_EMPLOYEES: '/api/employees/getAllemployees',

  //for comments and requests
  GET_ALL_COMMENTS: '/pms/version-comments/findAll',
  UPDATE_STATUS: (id) => `/pms/version-comments/update/${id}`,

  //API Authorization
  CREATE_ACCESS_TOKEN: '/admin/api-tokens',
  GET_ACCESS_TOKEN: '/admin/api-tokens',
  GET_USER_ROLES: '/admin/users',
  DELETE_TOKEN: (id) => `/admin/api-tokens/${id}`,
};
