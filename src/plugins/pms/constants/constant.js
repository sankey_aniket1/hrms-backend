//Http Methods
export const GET = 'GET';
export const POST = 'POST';
export const PUT = 'PUT';
export const DELETE = 'DELETE';
export const PATCH = 'PATCH';

//Constants of UI fields textInputs, select, textarea...
export const TEXTINPUT = 'TextInput';
export const SELECT = 'select';
export const TEXTAREA = 'Textarea';
export const DATE = 'date';
export const MULTISELECT = 'multiSelect';

//access-type
export const FULL_ACCESS = 'full-access';

//request body for creating access key
export const REQUEST_BODY_FACCESS = {
  lifespan: null,
  description: 'FULL ACCESS',
  type: FULL_ACCESS,
  name: 'FULL ACCESS',
  permissions: null,
};
