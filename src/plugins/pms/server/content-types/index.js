'use strict';
const pmsproject = require('./pmsproject');
const pmsthread = require('./pmsthread');
const pmsversion = require('./pmsversion');
const pmsversionresource = require('./pmsversionresource');
const versioncommentorrequest = require('./versioncommentorrequest');
module.exports = {
  pmsproject,
  pmsthread,
  pmsversion,
  pmsversionresource,
  versioncommentorrequest,
};
