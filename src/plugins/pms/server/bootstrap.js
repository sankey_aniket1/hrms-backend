"use strict";

module.exports = ({ strapi }) => {
  const actions = [
    {
      section: 'plugins',
      displayName: 'Read',
      uid: 'read',
      pluginName: 'pms',
    },
  ];
  strapi.admin.services.permission.actionProvider.registerMany(actions);
}