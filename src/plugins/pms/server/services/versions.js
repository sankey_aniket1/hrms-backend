"use strict";

module.exports = ({ strapi }) => ({
  async find(query) {
    const threadId = query.threadId;
    query.populate = ['versionComment','thread']; 
    return await strapi.entityService.findMany("plugin::pms.pmsversion", {
      filters: {
        $and: [
          { 'versions.id': threadId }
        ],
      },
    });
  },

  async findAll(query) {
    query.populate = ['versionComment','thread']; // Add this line to include the 'project' relatio
    return await strapi.entityService.findMany("plugin::pms.pmsversion",query);
  },
  async findOne(id) {
    return await strapi.entityService.findOne("plugin::pms.pmsversion", id);
  },

  async delete(id) {
    return await strapi.entityService.delete("plugin::pms.pmsversion", id);
  },

  async create(data) {
    return await strapi.entityService.create("plugin::pms.pmsversion",{ data: data });
  },

  async update(id, data) {
    return await strapi.entityService.update("plugin::pms.pmsversion", id, { data:data });
  },
});
