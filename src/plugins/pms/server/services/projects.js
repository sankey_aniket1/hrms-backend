"use strict";

module.exports = ({ strapi }) => ({
  async find(query) {
    query.populate = ['threads'];
    return await strapi.entityService.findMany("plugin::pms.pmsproject",query);
  },

  async findOne(id) {
    return await strapi.entityService.findOne("plugin::pms.pmsproject", id);
  },

  async delete(id) {
    return await strapi.entityService.delete("plugin::pms.pmsproject", id);
  },

  async create(data) {
    return await strapi.entityService.create("plugin::pms.pmsproject",{data: data} );
  },

  async update(id, data) {
    return await strapi.entityService.update("plugin::pms.pmsproject", id, {data:data});
  },
});
