"use strict";

module.exports = ({ strapi }) => ({
  async findAll(query) {
    query.populate = ['versionDls','employee'];
    return await strapi.entityService.findMany("plugin::pms.pmsversionresource",query);
  },
  async findOne(id) {
    return await strapi.entityService.findOne("plugin::pms.pmsversionresource", id);
  },

  async delete(id) {
    return await strapi.entityService.delete("plugin::pms.pmsversionresource", id);
  },

  async create(data) {
    return await strapi.entityService.create("plugin::pms.pmsversionresource",{ data: data });
  },

  async update(id, data) {
    return await strapi.entityService.update("plugin::pms.pmsversionresource", id, { data:data });
  },
});
