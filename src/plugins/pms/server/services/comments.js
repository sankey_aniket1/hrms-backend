"use strict";

module.exports = ({ strapi }) => ({
  async findAll(query) {
    query.populate = ['version'];
    return await strapi.entityService.findMany("plugin::pms.versioncommentorrequest", query);
  },
  async findOne(id) {
    return await strapi.entityService.findOne("plugin::pms.versioncommentorrequest", id);
  },

  async update(id, data) {
    return await strapi.entityService.update("plugin::pms.versioncommentorrequest", id, { data:data });
  },
});
