'use strict';

const projects = require('./projects');
const threads = require('./threads');
const versions = require('./versions');
const versionresource = require('./version-resource');
const comments = require('./comments');
module.exports = {
  projects,
  threads,
  versions,
  versionresource,
  comments,
};
