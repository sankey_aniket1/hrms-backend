'use strict';

module.exports = ({ strapi }) => ({
  async findAll(query) {
    query.populate = ['project', 'versions']; // Add this line to include the 'project' relation
    return await strapi.entityService.findMany("plugin::pms.pmsthread", query);
  },
  
  async findOne(id) {
    return await strapi.entityService.findOne('plugin::pms.pmsthread', id);
  },

  async delete(id) {
    return await strapi.entityService.delete('plugin::pms.pmsthread', id);
  },

  async create(data) {
    return await strapi.entityService.create("plugin::pms.pmsthread", { data: data });
  },

  async update(id, data) {
    return await strapi.entityService.update("plugin::pms.pmsthread", id, { data: data });
  },
});
