module.exports = [
  // for Projects
  {
    method: "GET",
    path: "/find",
    handler: "projects.find",
    config: {
      policies: [],
      auth: false,
    },
  },
  {
    method: "GET",
    path: "/findOne/:id",
    handler: "projects.findOne",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "POST",
    path: "/create",
    handler: "projects.create",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "DELETE",
    path: "/delete/:id",
    handler: "projects.delete",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "PUT",
    path: "/update/:id",
    handler: "projects.update",
    config: {
      policies: [],
      auth: false,
    },
  },

  // for Threads
  {
    method: "GET",
    path: "/thread/findAll",
    handler: "threads.findAll",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "GET",
    path: "/thread/findOne/:id",
    handler: "threads.findOne",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "POST",
    path: "/thread/create",
    handler: "threads.create",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "DELETE",
    path: "/thread/delete/:id",
    handler: "threads.delete",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "PUT",
    path: "/thread/update/:id",
    handler: "threads.update",
    config: {
      policies: [],
      auth: false,
    },
  },

  // for Versions
  {
    method: "GET",
    path: "/version/findAll",
    handler: "versions.findAll",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "GET",
    path: "/version/findone/:id",
    handler: "versions.findOne",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "POST",
    path: "/version/create",
    handler: "versions.create",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "DELETE",
    path: "/version/delete/:id",
    handler: "versions.delete",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "PUT",
    path: "/version/update/:id",
    handler: "versions.update",
    config: {
      policies: [],
      auth: false,
    },
  },

  // for Version-resource
  {
    method: "GET",
    path: "/version-resource/findAll",
    handler: "versionresource.findAll",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "GET",
    path: "/version-resource/findone/:id",
    handler: "versionresource.findOne",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "POST",
    path: "/version-resource/create",
    handler: "versionresource.create",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "DELETE",
    path: "/version-resource/delete/:id",
    handler: "versionresource.delete",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "PUT",
    path: "/version-resource/update/:id",
    handler: "versionresource.update",
    config: {
      policies: [],
      auth: false,
    },
  },

  // Version Comments API
  {
    method: "GET",
    path: "/version-comments/findAll",
    handler: "comments.findAll",
    config: {
      policies: [],
      auth: false,
    },
  },

  {
    method: "GET",
    path: "/version-comments/findone/:id",
    handler: "comments.findOne",
    config: {
      policies: [],
      auth: false,
    },
  },
  {
    method: "PUT",
    path: "/version-comments/update/:id",
    handler: "comments.update",
    config: {
      policies: [],
      auth: false,
    },
  },
];
