"use strict";

module.exports = {
  async findOne(ctx) {
    try {
      return await strapi.plugin("pms").service("versionresource").findOne(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
  async findAll(ctx) {
    try {
      return await strapi.plugin("pms").service("versionresource").findAll(ctx.query);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async delete(ctx) {
    try {
      ctx.body = await strapi
        .plugin("pms")
        .service("versionresource")
        .delete(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async create(ctx) {
    try {
      ctx.body = await strapi
        .plugin("pms")
        .service("versionresource")
        .create(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async update(ctx) {
    try {
      ctx.body = await strapi
        .plugin("pms")
        .service("versionresource")
        .update(ctx.params.id, ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
};
