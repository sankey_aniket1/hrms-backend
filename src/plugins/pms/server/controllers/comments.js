"use strict";

module.exports = {
  async findOne(ctx) {
    try {
      return await strapi.plugin("pms").service("comments").findOne(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async find(ctx) {
    try {
      return await strapi.plugin("pms").service("comments").find(ctx.query);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
  async findAll(ctx) {
    try {
      return await strapi.plugin("pms").service("comments").findAll(ctx.query);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async delete(ctx) {
    try {
      ctx.body = await strapi.plugin("pms").service("comments").delete(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async create(ctx) {
    try {
      ctx.body = await strapi.plugin("pms").service("comments").create(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async update(ctx) {
    try {
      ctx.body = await strapi.plugin("pms").service("comments").update(ctx.params.id, ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
};
