"use strict";

module.exports = {
  async findOne(ctx) {
    try {
      return await strapi.plugin("pms").service("versions").findOne(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
  async findAll(ctx) {
    try {
      return await strapi.plugin("pms").service("versions").findAll(ctx.query);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async delete(ctx) {
    try {
      ctx.body = await strapi
        .plugin("pms")
        .service("versions")
        .delete(ctx.params.id);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async create(ctx) {
    try {
      ctx.body = await strapi
        .plugin("pms")
        .service("versions")
        .create(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },

  async update(ctx) {
    try {
      ctx.body = await strapi
        .plugin("pms")
        .service("versions")
        .update(ctx.params.id, ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
};
