'use strict';

require('dotenv').config();
console.log('JWT_SECRET:', process.env.JWT_SECRET);
console.log('JWT_SECRET_EXPIRES:', process.env.JWT_SECRET_EXPIRES);
module.exports = {
  jwtSecret: process.env.JWT_SECRET,
  jwt: {
    expiresIn: process.env.JWT_SECRET_EXPIRES,
  },
};
