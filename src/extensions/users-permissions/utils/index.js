'use strict';

const getService = (name) => {
  return strapi.plugin('users-permissions').service(name);
  // eslint-disable-next-line no-unreachable
  return;
};
module.exports = {
  getService,
};
