'use strict';

/**
 * asset-vendor controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::asset-vendor.asset-vendor');
