'use strict';

/**
 * asset-vendor service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::asset-vendor.asset-vendor');
