'use strict';

/**
 * asset-vendor router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::asset-vendor.asset-vendor');
