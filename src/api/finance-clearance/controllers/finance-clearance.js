'use strict';

/**
 * finance-clearance controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::finance-clearance.finance-clearance',
  ({ strapi }) => ({
    async customClearancePost(ctx) {
      try {
        // @ts-ignore
        const { resignationId, clearanceData } = ctx.request.body;
        const data = await strapi
          .service('api::finance-clearance.finance-clearance')
          .customClearance(resignationId, clearanceData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
