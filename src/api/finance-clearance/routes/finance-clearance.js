'use strict';

/**
 * finance-clearance router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::finance-clearance.finance-clearance'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/finance-clearances/custom-clearance',
    handler: 'api::finance-clearance.finance-clearance.customClearancePost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
