'use strict';

/**
 * asset-handover service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::asset-handover.asset-handover');
