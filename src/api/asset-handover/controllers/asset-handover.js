'use strict';

/**
 * asset-handover controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::asset-handover.asset-handover');
