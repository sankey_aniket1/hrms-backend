'use strict';

/**
 * asset-handover router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::asset-handover.asset-handover');
