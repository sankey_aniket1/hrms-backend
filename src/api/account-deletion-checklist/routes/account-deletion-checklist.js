'use strict';

/**
 * account-deletion-checklist router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter(
  'api::account-deletion-checklist.account-deletion-checklist'
);
