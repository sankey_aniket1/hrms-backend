'use strict';

/**
 * account-deletion-checklist service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::account-deletion-checklist.account-deletion-checklist'
);
