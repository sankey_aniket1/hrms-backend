'use strict';

/**
 * account-deletion-checklist controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::account-deletion-checklist.account-deletion-checklist'
);
