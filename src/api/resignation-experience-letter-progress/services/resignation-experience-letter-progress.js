'use strict';

/**
 * resignation-experience-letter-progress service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-experience-letter-progress.resignation-experience-letter-progress');
