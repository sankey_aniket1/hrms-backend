'use strict';

/**
 * resignation-experience-letter-progress router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-experience-letter-progress.resignation-experience-letter-progress');
