'use strict';

/**
 * resignation-experience-letter-progress controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-experience-letter-progress.resignation-experience-letter-progress');
