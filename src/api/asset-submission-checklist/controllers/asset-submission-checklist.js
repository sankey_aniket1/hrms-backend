'use strict';

/**
 * asset-submission-checklist controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::asset-submission-checklist.asset-submission-checklist'
);
