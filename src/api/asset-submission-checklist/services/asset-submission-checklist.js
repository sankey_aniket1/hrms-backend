'use strict';

/**
 * asset-submission-checklist service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::asset-submission-checklist.asset-submission-checklist'
);
