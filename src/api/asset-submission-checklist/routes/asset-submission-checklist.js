'use strict';

/**
 * asset-submission-checklist router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter(
  'api::asset-submission-checklist.asset-submission-checklist'
);
