'use strict';

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::employee.employee', ({ strapi }) => ({
  async getEmployeeId(id) {
    return await strapi.entityService.findOne(
      'plugin::users-permissions.user',
      id,
      {
        fields: ['email', 'username'],
        populate: '*',
      }
    );
  },
  async getEmployeeDetailsService(user) {
    const userId = await strapi
      .service('api::employee.employee')
      .getEmployeeId(user.id);
    if (userId.employee !== null) {
      return await strapi.entityService.findOne(
        'api::employee.employee',
        userId.employee.id,
        {
          select: [
            'empId',
            'firstName',
            'middleName',
            'lastName',
            'maritalStatus',
            'gender',
            'bloodGroup',
            'dateOfBirth',
            'permanentAddress',
            'correspondenceAddress',
            'contactNumber',
            'personalEmailId',
            'alternativeContactNumber',
            'nationality',
            'adharCardNumber',
            'panCardNumber',
            'uan',
            'eppfNo',
            'passport',
            'dateOfJoining',
          ],
          populate: {
            designation: {},
            profilePicture: {},
            user: { populate: { role: {} } },
          },
        }
      );
    } else {
      return {};
    }
  },
  async getNewEmployees() {
    return await strapi.entityService.findMany(
      'plugin::users-permissions.user',
      {
        filters: { employee: null },
      }
    );
  },
  async getNewEmployeeID() {
    const latestEmployee = await strapi.db
      .query('api::employee.employee')
      .findOne({
        select: ['empId'],
        orderBy: { publishedAt: 'DESC' },
      });

    return { empId: latestEmployee ? Number(latestEmployee.empId) + 1 : 5000 };
  },
  async getEmployeeDocuments(ids) {
    const results = await Promise.all(
      ids.map(async (id) => {
        const { results: employeeResults } =
          await strapi.entityService.findPage('api::employee.employee', {
            filters: { id },
            populate: { documents: { populate: { document: {} } } },
          });
        return employeeResults[0];
      })
    );

    const finalList = results.map(({ id, documents }) => {
      const finalListOfDocuments = documents.map((element) =>
        element.document &&
        element.document.name
          .toLowerCase()
          .match(/\.(jpg|jpeg|png|gif|svg|tiff|dvu)$/i)
          ? element.document.formats.small || null
          : element.document || null
      );

      return {
        id,
        documents: finalListOfDocuments,
      };
    });

    return finalList;
  },
  async postEmployeeRole(id, roleId) {
    await strapi.db.connection.raw(
      `UPDATE up_users_role_links SET role_id = ${roleId} WHERE user_id = ${id}`
    );

    return await strapi.entityService.findOne(
      'plugin::users-permissions.user',
      id
    );
  },

  async getEmployees() {
    return await strapi.entityService.findMany('api::employee.employee');
  },
}));
