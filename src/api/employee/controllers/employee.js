'use strict';

/**
 * employee controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::employee.employee',
  ({ strapi }) => ({
    // Get list of users who are yet to be onboarded
    async getNewEmployees(ctx) {
      try {
        const data = await strapi
          .service('api::employee.employee')
          .getNewEmployees();
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
    async getNewEmployeeID(ctx) {
      try {
        const data = await strapi
          .service('api::employee.employee')
          .getNewEmployeeID();
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
    async getEmployeeDocumnets(ctx) {
      try {
        // @ts-ignore
        const { ids } = ctx.request.body;
        const data = await strapi
          .service('api::employee.employee')
          .getEmployeeDocuments(ids);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
    async getEmployeeDetails(ctx) {
      try {
        const user = ctx.state.user;

        const employeeDetails = await strapi
          .service('api::employee.employee')
          .getEmployeeDetailsService(user);
        ctx.body = employeeDetails;
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
    async postEmployeeRole(ctx) {
      try {
        // @ts-ignore
        const { id, roleId } = ctx.request.body;
        const data = await strapi
          .service('api::employee.employee')
          .postEmployeeRole(id, roleId);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async getEmployees(ctx) {
      try {
        // @ts-ignore
        const data = await strapi.service('api::employee.employee').getEmployees();
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
