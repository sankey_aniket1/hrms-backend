'use strict';

/**
 * employee router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter('api::employee.employee');
const myExtraRoutes = [
  {
    method: 'GET',
    path: '/employees/new-employees',
    handler: 'api::employee.employee.getNewEmployees',
  },
  {
    method: 'GET',
    path: '/employees/get-id',
    handler: 'api::employee.employee.getNewEmployeeID',
  },
  {
    method: 'POST',
    path: '/employees/documents',
    handler: 'api::employee.employee.getEmployeeDocumnets',
  },
  {
    method: 'GET',
    path: '/employees/employee-details',
    handler: 'api::employee.employee.getEmployeeDetails',
  },
  {
    method: 'POST',
    path: '/employees/set-role',
    handler: 'api::employee.employee.postEmployeeRole',
  },
  {
    method: 'GET',
    path: '/employees/getAllemployees',
    handler: 'api::employee.employee.getEmployees',
    config: {
      policies: [],
      auth: false,
    },
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
