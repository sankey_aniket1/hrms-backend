'use strict';

/**
 * `employee` middleware
 */

module.exports = (config, { strapi }) => {
  return async (ctx, next) => {
    strapi.log.info('In employee middleware.');
    const responseData = ctx.response.body;

    responseData.data.forEach((item) => {
      item.modifiedProperty = 'new value';
    });

    await next();
  };
};
