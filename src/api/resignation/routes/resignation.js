'use strict';

/**
 * resignation router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

const defaultRouter = createCoreRouter('api::resignation.resignation');

const { customRouter } = require('../../../../config/common');

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/resignations/get-template-document',
    handler: 'api::resignation.resignation.getTemplateDocument',
  },
  {
    method: 'POST',
    path: '/resignations/nda-cron',
    handler: 'api::resignation.resignation.ndaCron',
  },
  //   {
  //     method: 'POST',
  //     path: '/resignations/experience-cron',
  //     handler: 'api::resignation.resignation.experienceCron',
  //   },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
