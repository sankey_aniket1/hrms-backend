'use strict';

/**
 * resignation service
 */

let fs = require('fs');
let cron = require('node-cron');
const PizZip = require('pizzip');
const { createCoreService } = require('@strapi/strapi').factories;
const Docxtemplater = require('docxtemplater');
const libre = require('libreoffice-convert');
// @ts-ignore
libre.convertAsync = require('util').promisify(libre.convert);

module.exports = createCoreService(
  'api::resignation.resignation',
  ({ strapi }) => ({
    async getTemplateDocument(employeeId, type, scheduledDate) {
      function getFormattedDate(date) {
        if (date === null) {
          return null;
        }
        return new Date(date).toLocaleDateString('en-US', {
          day: 'numeric',
          month: 'long',
          year: 'numeric',
        });
      }
      const employeeDetails = await strapi.entityService.findOne(
        'api::employee.employee',
        employeeId,
        {
          select: [
            'firstName',
            'lastName',
            'dateOfBirth',
            'permanentAddress',
            'dateOfJoining',
            'dateOfLeaving',
          ],
          populate: {
            designation: {},
            resignation: {},
          },
        }
      );

      const employeeResignationDetails = await strapi.entityService.findOne(
        'api::resignation.resignation',
        employeeId,
        {
          populate: {
            employee: {
              filters: {
                empId: employeeId,
              },
            },
          },
        }
      );

      const executed_date = getFormattedDate(scheduledDate);
      const first_name = employeeDetails.firstName;
      const last_name = employeeDetails.lastName;
      const employee_name = `${first_name} ${last_name}`;

      const residingAddress = 'Mumbai, Maharashtra';
      const designation = employeeDetails.designation.name;

      const dobDate = new Date(employeeDetails.dateOfBirth);
      const currentDate = new Date();
      const age = currentDate.getFullYear() - dobDate.getFullYear();

      const date_of_joining = employeeDetails.dateOfJoining;
      const date_of_joining_converted = getFormattedDate(date_of_joining);

      const date_of_leaving = employeeResignationDetails.exitDate;
      const date_of_leaving_converted = getFormattedDate(date_of_leaving);

      let template_path;
      let dataPatchPayload = {};

      switch (type) {
        case 'nda-1':
          template_path =
            '/sankey-hrms-backend/src/assets/non-disclosure-agreement.docx';
          //! for deployment
          // template_path =
          //   '/usr/src/sankey-hrms-backend/src/assets/non-disclosure-agreement.docx';
          dataPatchPayload = {
            executed_date: executed_date,
            employee_name: employee_name,
            residing_address: residingAddress,
            designation: designation,
            age: age,
          };
          break;
        case 'exp-1':
          template_path =
            '/sankey-hrms-backend/src/assets/experience-letter.docx';
          //! for deployment
          // template_path =
          //   '/usr/src/sankey-hrms-backend/src/assets/experience-letter.docx';
          dataPatchPayload = {
            executed_date: executed_date,
            employee_name: employee_name,
            designation: designation,
            date_of_joining: date_of_joining_converted,
            date_of_leaving: date_of_leaving_converted,
          };
          break;
      }

      // Use this while testing on windows machine
      const documentContent = await fs.promises.readFile(template_path);

      const unzippedDocument = new PizZip(documentContent);

      const document = new Docxtemplater(unzippedDocument, {
        paragraphLoop: true,
        linebreaks: true,
      });

      document.render(dataPatchPayload);

      const documetBuffer = document.getZip().generate({
        type: 'nodebuffer',
        compression: 'DEFLATE',
      });

      // @ts-ignore
      let pdfBuffer = await libre.convertAsync(documetBuffer, 'pdf', undefined);

      return pdfBuffer;
    },

    async ndaCron(employeeId, scheduledDate) {
      // ! Please check this code - Gaurang
      const javaDate = new Date(scheduledDate);

      const dayOfMonth = javaDate.getDate();
      const month = javaDate.getMonth() + 1;

      const cronExpression = `22 18 ${dayOfMonth} ${month} * *`;
      strapi.log.info(cronExpression);
      strapi.log.info(new Date());

      cron.schedule(cronExpression, () => {
        strapi.log.info(employeeId);
      });
    },
  })
);
