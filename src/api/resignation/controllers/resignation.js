'use strict';

/**
 * resignation controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::resignation.resignation',
  ({ strapi }) => ({
    async getTemplateDocument(ctx) {
      try {
        // @ts-ignore
        const { employeeId, type } = ctx.request.body;
        const data = await strapi
          .service('api::resignation.resignation')
          .getTemplateDocument(employeeId, type);
        ctx.set('Content-Type', 'application/pdf');
        ctx.set(
          'Content-Disposition',
          'attachment; filename="non-disclosure-agreement.pdf"'
        );
        ctx.body = data;
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async ndaCron(ctx) {
      try {
        // @ts-ignore
        const { employeeId, scheduledDate } = ctx.request.body;
        const data = await strapi
          .service('api::resignation.resignation')
          .ndaCron(employeeId, scheduledDate);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
