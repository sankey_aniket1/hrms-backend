'use strict';

/**
 * it-asset-clearance controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::it-asset-clearance.it-asset-clearance',
  ({ strapi }) => ({
    async customClearancePost(ctx) {
      try {
        // @ts-ignore
        const { resignationId, clearanceData } = ctx.request.body;
        const data = await strapi
          .service('api::it-asset-clearance.it-asset-clearance')
          .customClearance(resignationId, clearanceData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
