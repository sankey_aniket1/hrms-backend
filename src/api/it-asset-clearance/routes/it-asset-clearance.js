'use strict';

/**
 * it-asset-clearance router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::it-asset-clearance.it-asset-clearance'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/it-asset-clearances/custom-clearance',
    handler: 'api::it-asset-clearance.it-asset-clearance.customClearancePost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
