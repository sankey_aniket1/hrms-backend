'use strict';

/**
 * employee-resignation-clearance controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::employee-resignation-clearance.employee-resignation-clearance',
  ({ strapi }) => ({
    async customClearancePost(ctx) {
      try {
        // @ts-ignore
        const { resignationId, clearanceData } = ctx.request.body;
        const data = await strapi
          .service(
            'api::employee-resignation-clearance.employee-resignation-clearance'
          )
          .customClearance(resignationId, clearanceData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
