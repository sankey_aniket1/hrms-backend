'use strict';

/**
 * employee-resignation-clearance service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::employee-resignation-clearance.employee-resignation-clearance',
  ({ strapi }) => ({
    async customClearance(resignationId, clearanceData) {
      let responseEntries = [];
      let createEntries = [];
      let updateEntries = [];

      for (const element of clearanceData) {
        if (element.id === null) {
          delete element.id;
          createEntries.push({
            ...element,
            publishedAt: new Date().toISOString().split('T')[0],
            resignation: resignationId,
          });
        } else {
          updateEntries.push(element);
        }
      }

      const createPromises = createEntries.map(async (element) => {
        const createdEntry = await strapi.entityService.create(
          'api::employee-resignation-clearance.employee-resignation-clearance',
          { data: element }
        );
        if (createdEntry) {
          const {
            // eslint-disable-next-line no-unused-vars
            createdAt,
            // eslint-disable-next-line no-unused-vars
            updatedAt,
            // eslint-disable-next-line no-unused-vars
            publishedAt,
            ...createdEntryWithoutMetadata
          } = createdEntry;
          responseEntries.push(createdEntryWithoutMetadata);
        }
      });

      await Promise.all(createPromises);

      const updatePromises = updateEntries.map(async (element) => {
        let dataWithoutId = { ...element };
        delete dataWithoutId.id;

        const updatedEntry = await strapi.entityService.update(
          'api::employee-resignation-clearance.employee-resignation-clearance',
          element.id,
          { data: dataWithoutId }
        );
        if (updatedEntry) {
          const {
            // eslint-disable-next-line no-unused-vars
            createdAt,
            // eslint-disable-next-line no-unused-vars
            updatedAt,
            // eslint-disable-next-line no-unused-vars
            publishedAt,
            ...updatedEntryWithoutMetadata
          } = updatedEntry;
          responseEntries.push(updatedEntryWithoutMetadata);
        }
      });

      await Promise.all(updatePromises);

      const allRecordsForEmployee = await strapi.db.connection.raw(`
      SELECT erc.*
      FROM resignations r
      JOIN resignations_employee_resignation_clearances_links rercl ON r.id = rercl.resignation_id
      JOIN employee_resignation_clearances erc ON rercl.employee_resignation_clearance_id = erc.id
      WHERE r.id = ${resignationId}
    `);

      const employeeResignationData = allRecordsForEmployee.rows;

      const idsNotInBothArrays = employeeResignationData
        .filter(
          (data) => !responseEntries.some((entry) => entry.id === data.id)
        )
        .map((data) => data.id);

      if (idsNotInBothArrays.length > 0) {
        const deletePromises = idsNotInBothArrays.map(async (element) => {
          await strapi.entityService.delete(
            'api::employee-resignation-clearance.employee-resignation-clearance',
            element
          );
        });
        await Promise.all(deletePromises);
      }

      const finalPayload = responseEntries.map((element) => {
        const { id, ...attributes } = element;
        return { id, attributes };
      });

      return { data: finalPayload };
    },
  })
);
