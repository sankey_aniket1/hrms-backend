'use strict';

/**
 * employee-resignation-clearance router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::employee-resignation-clearance.employee-resignation-clearance'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/employee-resignation-clearances/custom-clearance',
    handler:
      'api::employee-resignation-clearance.employee-resignation-clearance.customClearancePost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
