'use strict';

/**
 * billable-status router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::billable-status.billable-status');
