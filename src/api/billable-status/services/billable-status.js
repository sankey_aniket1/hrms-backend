'use strict';

/**
 * billable-status service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::billable-status.billable-status');
