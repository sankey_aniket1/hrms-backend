'use strict';

/**
 * billable-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::billable-status.billable-status');
