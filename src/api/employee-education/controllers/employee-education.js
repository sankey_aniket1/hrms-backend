'use strict';

/**
 * employee-education controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::employee-education.employee-education',
  // @ts-ignore
  ({ strapi }) => ({
    async customEducationPost(ctx) {
      try {
        // @ts-ignore
        const { employee, educationData } = ctx.request.body;
        const data = await strapi
          .service('api::employee-education.employee-education')
          .customEducation(employee, educationData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
