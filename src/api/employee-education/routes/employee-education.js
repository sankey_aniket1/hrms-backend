'use strict';

/**
 * employee-education router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::employee-education.employee-education'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/employee-educations/custom-education',
    handler: 'api::employee-education.employee-education.customEducationPost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
