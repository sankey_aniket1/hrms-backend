'use strict';

/**
 * employee-family controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::employee-family.employee-family',
  ({ strapi }) => ({
    async customFamilyPost(ctx) {
      try {
        // @ts-ignore
        const { employee, familyData } = ctx.request.body;
        const data = await strapi
          .service('api::employee-family.employee-family')
          .customFamily(employee, familyData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
