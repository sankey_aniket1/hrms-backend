/* eslint-disable indent */
'use strict';

/**
 * employee-family service
 */

const { createCoreService } = require('@strapi/strapi').factories;
module.exports = createCoreService(
  'api::employee-family.employee-family',
  ({ strapi }) => ({
    async customFamily(employee, familyData) {
      let responseEntries = [];
      let createEntries = [];
      let updateEntries = [];

      for (const element of familyData) {
        if (element.id === null) {
          delete element.id;
          createEntries.push({
            ...element,
            publishedAt: new Date().toISOString().split('T')[0],
            employee: employee,
          });
        } else {
          updateEntries.push(element);
        }
      }

      const createPromises = createEntries.map(async (element) => {
        const createdEntry = await strapi.entityService.create(
          'api::employee-family.employee-family',
          { data: element }
        );
        if (createdEntry) {
          const {
            // eslint-disable-next-line no-unused-vars
            createdAt,
            // eslint-disable-next-line no-unused-vars
            updatedAt,
            // eslint-disable-next-line no-unused-vars
            publishedAt,
            ...createdEntryWithoutMetadata
          } = createdEntry;
          responseEntries.push(createdEntryWithoutMetadata);
        }
      });

      await Promise.all(createPromises);

      const updatePromises = updateEntries.map(async (element) => {
        let dataWithoutId = { ...element };
        delete dataWithoutId.id;

        const updatedEntry = await strapi.entityService.update(
          'api::employee-family.employee-family',
          element.id,
          { data: dataWithoutId }
        );
        if (updatedEntry) {
          const {
            // eslint-disable-next-line no-unused-vars
            createdAt,
            // eslint-disable-next-line no-unused-vars
            updatedAt,
            // eslint-disable-next-line no-unused-vars
            publishedAt,
            ...updatedEntryWithoutMetadata
          } = updatedEntry;
          responseEntries.push(updatedEntryWithoutMetadata);
        }
      });

      await Promise.all(updatePromises);

      const allRecordsForEmployee = await strapi.db.connection.raw(`
        SELECT ef.*
        FROM employees e
        JOIN employee_families_employee_links efl ON e.id = efl.employee_id
        JOIN employee_families ef ON efl.employee_family_id = ef.id
        WHERE e.id = ${employee}
      `);

      const employeeFamilyData = allRecordsForEmployee.rows;

      const idsNotInBothArrays = employeeFamilyData
        .filter(
          (data) => !responseEntries.some((entry) => entry.id === data.id)
        )
        .map((data) => data.id);

      if (idsNotInBothArrays.length > 0) {
        const deletePromises = idsNotInBothArrays.map(async (element) => {
          await strapi.entityService.delete(
            'api::employee-family.employee-family',
            element
          );
        });
        await Promise.all(deletePromises);
      }

      const finalPayload = responseEntries.map((element) => {
        const { id, ...attributes } = element;
        return { id, attributes };
      });

      return { data: finalPayload };
    },
  })
);
