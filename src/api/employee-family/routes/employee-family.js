'use strict';

/**
 * employee-family router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter('api::employee-family.employee-family');

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/employee-families/custom-family',
    handler: 'api::employee-family.employee-family.customFamilyPost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
