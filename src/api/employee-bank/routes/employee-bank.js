'use strict';

/**
 * employee-bank router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter('api::employee-bank.employee-bank');

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/employee-banks/custom-bank',
    handler: 'api::employee-bank.employee-bank.customBankPost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
