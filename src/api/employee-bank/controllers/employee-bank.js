'use strict';

/**
 * employee-bank controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::employee-bank.employee-bank',
  ({ strapi }) => ({
    async customBankPost(ctx) {
      try {
        // @ts-ignore
        const { employee, bankData } = ctx.request.body;
        const data = await strapi
          .service('api::employee-bank.employee-bank')
          .customBank(employee, bankData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
