'use strict';

/**
 * employee-bond controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::employee-bond.employee-bond');
