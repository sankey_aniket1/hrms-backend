'use strict';

/**
 * employee-bond service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::employee-bond.employee-bond');
