'use strict';

/**
 * exit-form-checklist service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::exit-form-checklist.exit-form-checklist'
);
