'use strict';

/**
 * exit-form-checklist router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter(
  'api::exit-form-checklist.exit-form-checklist'
);
