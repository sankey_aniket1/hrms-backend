'use strict';

/**
 * exit-form-checklist controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::exit-form-checklist.exit-form-checklist'
);
