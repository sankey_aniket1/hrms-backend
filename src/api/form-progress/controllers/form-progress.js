'use strict';

/**
 * form-progress controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::form-progress.form-progress',
  ({ strapi }) => ({
    async customProgressPost(ctx) {
      try {
        // @ts-ignore
        const { employee, progressData } = ctx.request.body;
        const data = await strapi
          .service('api::form-progress.form-progress')
          .customProgress(employee, progressData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
