'use strict';

/**
 * form-progress service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::form-progress.form-progress',
  ({ strapi }) => ({
    async customProgress(employee, progressData) {
      Object.defineProperty(progressData, 'employee', {
        value: employee,
        enumerable: true,
        writable: true,
      });

      Object.defineProperty(progressData, 'publishedAt', {
        value: new Date().toISOString().split('T')[0],
        enumerable: true,
        writable: true,
      });

      const isCreating = progressData.id === null;

      let entry;
      if (isCreating) {
        delete progressData.id;
        entry = await strapi.entityService.create(
          'api::form-progress.form-progress',
          { data: progressData }
        );
      } else {
        const { id, ...dataWithoutId } = progressData;
        entry = await strapi.entityService.update(
          'api::form-progress.form-progress',
          id,
          { data: dataWithoutId }
        );
      }

      if (entry) {
        // eslint-disable-next-line no-unused-vars
        const { createdAt, updatedAt, publishedAt, ...entryWithoutMetadata } =
          entry;
        return { data: entryWithoutMetadata };
      }
    },
  })
);
