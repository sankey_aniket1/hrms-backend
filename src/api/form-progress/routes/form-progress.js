'use strict';

/**
 * form-progress router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter('api::form-progress.form-progress');

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/form-progress/custom-progress',
    handler: 'api::form-progress.form-progress.customProgressPost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
