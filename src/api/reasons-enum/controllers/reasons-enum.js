'use strict';

/**
 * reasons-enum controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::reasons-enum.reasons-enum');
