'use strict';

/**
 * reasons-enum service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::reasons-enum.reasons-enum');
