'use strict';

/**
 * reasons-enum router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::reasons-enum.reasons-enum');
