'use strict';

/**
 * feedback service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::feedback.feedback', ({ strapi }) => ({
  async getAllFeedbacks(query) {
    try {
      // Set default values for pagination
      const page = query.currentPage ? parseInt(query.currentPage, 10) : 1;
      const pageSize = query.pageSize ? parseInt(query.pageSize, 10) : 10;
      const searchText = query.searchText ? query.searchText.trim() : '';

      // Calculate start and limit for pagination
      const start = (page - 1) * pageSize;
      const limit = pageSize;

      // Fetch feedbacks with related threads and resources
      let baseQuery = `
        SELECT f.*, vr.*, e.*, vr.id AS resourceId,
        CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS resourceName, p.version_name AS versionName, pfamff.type AS typeOfFeedback, 
        f.id AS feedbackId 
        FROM pmsversionresources vr
        JOIN pmsfeedbacks_version_resource_links fvr ON vr.id = fvr.pmsversionresource_id
        JOIN pmsversionresources_version_dls_links pvrids ON vr.id = pvrids.pmsversionresource_id
        JOIN pmsversions p ON p.id = pvrids.pmsversion_id
        JOIN pmsfeedbacks f ON f.id = fvr.pmsfeedback_id
        JOIN versioncommentorrequest_version_links vvl ON vvl.pmsversion_id = p.id 
        JOIN versioncommentorrequest v ON v.id = vvl.versioncommentorrequest_id 
        JOIN versioncommentorrequest_feedback_assesment_form_links vfafl ON vfafl.versioncommentorrequest_id = v.id 
        JOIN pmsfeedbackassesmentformformats pfamff ON pfamff.id = vfafl.pmsfeedbackassesmentformformat_id 
        JOIN pmsversionresources_employee_links rel ON rel.pmsversionresource_id = fvr.pmsversionresource_id
        JOIN employees e ON e.id = rel.employee_id
      `;

      //search filter if searchText is provided
      if (searchText) {
        baseQuery += `
          WHERE LOWER(CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name)) LIKE '%${searchText.toLowerCase()}%'
          OR LOWER(f.project_name) LIKE '%${searchText.toLowerCase()}%'
          OR LOWER(f.thread_name) LIKE '%${searchText.toLowerCase()}%'
        `;
      }

      //fetching paginated feedbacks
      const feedbackDetails = await strapi.db.connection.raw(`
        ${baseQuery}
        LIMIT ${limit} OFFSET ${start}
      `);

      const feedbackData = feedbackDetails.rows;

      //get the total count of feedbacks
      const totalCountResult = await strapi.db.connection.raw(`
        SELECT COUNT(*) FROM (${baseQuery}) AS totalFeedbacks
      `);

      const totalCount = parseInt(totalCountResult.rows[0].count, 10);

      const feedbacksWithThreadName = feedbackData.map((feedback) => ({
        id: feedback.feedbackid,
        feedback: JSON.parse(feedback.feedback),
        feedbackTriggerDate: feedback.feedback_trigger_date,
        cooldownPeriodDate: feedback.cooldown_period_date,
        threadName: feedback.thread_name,
        projectName: feedback.project_name,
        resourceId: feedback.resourceid,
        resourceName: feedback.resourcename,
        versionName: feedback.versionname,
        typeOfFeedback: feedback.typeoffeedback,
        createdAt: feedback.created_at,
      }));

      // Calculate total pages
      const totalPages = Math.ceil(totalCount / pageSize);

      // Return data with pagination metadata
      return {
        data: feedbacksWithThreadName,
        meta: {
          pagination: {
            page,
            pageSize,
            pageCount: totalPages,
            total: totalCount,
          },
        },
      };
    } catch (err) {
      strapi.log.error('Error fetching feedbacks data', err);
      throw new Error('Unable to fetch feedbacks data');
    }
  },

  async addFeedbackActions(type, id) {
    let updateType;

    switch (type.toLowerCase()) {
      case 'allocation of resource':
        updateType = 'Fixed';
        break;
      case 'pip':
        updateType = 'PIP';
        break;
      case 'performance evaluation':
        updateType = 'Removed';
        break;
      default:
        throw new Error('Invalid type provided');
    }

    // Update versionResources table
    try {
      await strapi.entityService.update(
        'plugin::pms.pmsversionresource',
        parseInt(id, 10),
        {
          data: { type: updateType },
        }
      );
    } catch (error) {
      strapi.log.error('Error updating resource:', error);
      throw error;
    }

    let formFormat;
    // Get feedback form parameters from feedback_or_assessment_form_formats table
    try {
      formFormat = await strapi.entityService.findMany(
        'plugin::feedbacks.pmsfeedbackassesmentformformat',
        {
          filters: { type: type },
        }
      );

      if (formFormat.length === 0) {
        throw new Error('No form formats found for the specified type');
      }
    } catch (error) {
      strapi.log.error('Error fetching feedback assessment form data:', error);
      throw error;
    }

    const form = formFormat[0];
    const feedbackData = {
      feedback: form.FormParameters,
      feedback_trigger_date: new Date(),
      cooldown_period_date: new Date(
        new Date().getTime() + form.cooldownPriod * 24 * 60 * 60 * 1000
      ),
    };

    try {
      // Insert record into Feedback table
      const feedbackEntry = await strapi.entityService.create(
        'plugin::feedbacks.pmsfeedback',
        {
          data: feedbackData,
        }
      );

      // Insert into linking table 'pmsfeedbacks_feedback_assesment_form_links'
      await strapi.db.connection.raw(
        'INSERT INTO pmsfeedbacks_feedback_assesment_form_links (pmsfeedback_id, pmsfeedbackassesmentformformat_id) VALUES (?, ?)',
        [feedbackEntry.id, form.id]
      );

      // Insert into linking table 'pmsfeedbacks_version_resource_id_links'
      await strapi.db.connection.raw(
        'INSERT INTO pmsfeedbacks_version_resource_links (pmsfeedback_id, pmsversionresource_id) VALUES (?, ?)',
        [feedbackEntry.id, parseInt(id, 10)]
      );

      const response = {
        id: feedbackEntry.id,
        title: form.title,
        description: form.description,
        isAssessment: form.isAssesment,
      };

      return response;
    } catch (error) {
      strapi.log.error('Error updating feedback entry:', error);
      throw error;
    }
  },

  async getFeedbackFormDataByID(feedbackId) {
    // Fetching feedback details by ID
    const result = await strapi.db.connection.raw(
      `
      SELECT 
        f.id AS feedback_id, 
        f.feedback, 
        pfa.title, 
        pfa.description, 
        pfa.is_assesment 
      FROM 
        pmsfeedbacks AS f
      LEFT JOIN 
        pmsfeedbacks_feedback_assesment_form_links AS fal 
        ON fal.pmsfeedback_id = f.id
      LEFT JOIN 
        pmsfeedbackassesmentformformats AS pfa 
        ON pfa.id = fal.pmsfeedbackassesmentformformat_id
      WHERE 
        f.id = ?
    `,
      [feedbackId]
    );

    const rows = result.rows;

    if (rows.length === 0) {
      throw new Error('Feedback not found');
    }

    const feedbackData = rows[0];
    const feedback = JSON.parse(feedbackData.feedback);

    const responseData = {
      id: feedbackData.feedback_id,
      feedback: feedback,
      title: feedbackData.title,
      description: feedbackData.description,
      isAssesment: feedbackData.is_assesment,
    };

    return responseData;
  },

  async submitFeedbackForm(id, formParameters) {
    try {
      // Fetch the existing feedback entry
      const existingFeedback = await strapi.entityService.findOne(
        'plugin::feedbacks.pmsfeedback',
        id
      );

      if (!existingFeedback) {
        throw new Error('Feedback entry not found');
      }

      // Updating the formParameters and submitted_date in the feedback object
      // @ts-ignore
      existingFeedback.feedback.formParameters = formParameters;
      const currentDate = new Date().toISOString();

      // Update the record in the Feedback table
      await strapi.entityService.update('plugin::feedbacks.pmsfeedback', id, {
        data: {
          feedback: existingFeedback.feedback,
          submittedDate: currentDate,
        },
      });

      return { message: 'Feedback Form submitted successfully' };
    } catch (error) {
      strapi.log.error('Error updating feedback entry:', error);
      throw error;
    }
  },
}));
