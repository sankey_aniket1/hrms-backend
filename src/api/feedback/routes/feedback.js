'use strict';

/**
 * feedback router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter('api::feedback.feedback');
const myExtraRoutes = [
  {
    method: 'GET',
    path: '/feedback/:searchText?/:currentPage',
    handler: 'api::feedback.feedback.getAllFeedbacks',
    config: {
      policies: [],
    },
  },
  {
    method: 'POST',
    path: '/feedback-action',
    handler: 'api::feedback.feedback.addFeedbackActions',
    config: {
      policies: [],
    },
  },
  {
    method: 'GET',
    path: '/feedback-form-data/:id',
    handler: 'api::feedback.feedback.getFeedbackFormData',
    config: {
      policies: [],
    },
  },
  {
    method: 'POST',
    path: '/submit-feedback-form',
    handler: 'api::feedback.feedback.submitFeedbackForm',
    config: {
      policies: [],
      auth: false,
    },
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
