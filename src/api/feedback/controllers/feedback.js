'use strict';

/**
 * feedback controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::feedback.feedback',
  ({ strapi }) => ({
    // Get list of all versions
    async getAllFeedbacks(ctx) {
      try {
        // query parameters for pagination
        const { pageSize = 10 } = ctx.query;
        // searchText from the request parameters
        const searchText = ctx.params.searchText || '';
        const currentPage = ctx.params.currentPage || 1;

        const data = await strapi
          .service('api::feedback.feedback')
          .getAllFeedbacks({ currentPage, pageSize, searchText });

        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async addFeedbackActions(ctx) {
      const { type, id } = ctx.query;

      if (!type || !id) {
        return ctx.throw(400, 'type and id query parameters are required');
      }

      try {
        const feedbackResponse = await strapi
          .service('api::feedback.feedback')
          // @ts-ignore
          .addFeedbackActions(type.toLowerCase(), id);
        return ctx.send(feedbackResponse);
      } catch (error) {
        return ctx.throw(400, error.message);
      }
    },

    async getFeedbackFormData(ctx) {
      try {
        const { id } = ctx.params;
        const data = await strapi
          .service('api::feedback.feedback')
          .getFeedbackFormDataByID(id);

        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async submitFeedbackForm(ctx) {
      // @ts-ignore
      const { id, formParameters } = ctx.request.body;

      if (!formParameters || !id) {
        return ctx.throw(
          400,
          'formParameters and id are required in the request body'
        );
      }

      try {
        const submitFeedbackResponse = await strapi
          .service('api::feedback.feedback')
          .submitFeedbackForm(id, formParameters);
        return ctx.send(submitFeedbackResponse);
      } catch (error) {
        strapi.log.error('Error in updating feedback entry:', error);
        return ctx.throw(400, error.message);
      }
    },
  })
);
