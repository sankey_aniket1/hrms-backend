/* eslint-disable indent */
'use strict';

/**
 * employee-experience service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::employee-experience.employee-experience',
  ({ strapi }) => ({
    async customWork(employee, workData) {
      let responseEntries = [];
      let createEntries = [];
      let updateEntries = [];

      for (const element of workData) {
        if (element.id === null) {
          delete element.id;
          createEntries.push({
            ...element,
            publishedAt: new Date().toISOString().split('T')[0],
            employee: employee,
          });
        } else {
          updateEntries.push(element);
        }
      }

      const createPromises = createEntries.map(async (element) => {
        const createdEntry = await strapi.entityService.create(
          'api::employee-experience.employee-experience',
          { data: element }
        );
        if (createdEntry) {
          const {
            // eslint-disable-next-line no-unused-vars
            createdAt,
            // eslint-disable-next-line no-unused-vars
            updatedAt,
            // eslint-disable-next-line no-unused-vars
            publishedAt,
            ...createdEntryWithoutMetadata
          } = createdEntry;
          responseEntries.push(createdEntryWithoutMetadata);
        }
      });

      await Promise.all(createPromises);

      const updatePromises = updateEntries.map(async (element) => {
        let dataWithoutId = { ...element };
        delete dataWithoutId.id;

        const updatedEntry = await strapi.entityService.update(
          'api::employee-experience.employee-experience',
          element.id,
          { data: dataWithoutId }
        );
        if (updatedEntry) {
          const {
            // eslint-disable-next-line no-unused-vars
            createdAt,
            // eslint-disable-next-line no-unused-vars
            updatedAt,
            // eslint-disable-next-line no-unused-vars
            publishedAt,
            ...updatedEntryWithoutMetadata
          } = updatedEntry;
          responseEntries.push(updatedEntryWithoutMetadata);
        }
      });

      await Promise.all(updatePromises);

      const allRecordsForEmployee = await strapi.db.connection.raw(`
        SELECT ef.*
        FROM employees e
        JOIN employee_experiences_employee_links efl ON e.id = efl.employee_id
        JOIN employee_experiences ef ON efl.employee_experience_id = ef.id
        WHERE e.id = ${employee}
      `);

      const employeeExperienceData = allRecordsForEmployee.rows;

      const idsNotInBothArrays = employeeExperienceData
        .filter(
          (data) => !responseEntries.some((entry) => entry.id === data.id)
        )
        .map((data) => data.id);

      if (idsNotInBothArrays.length > 0) {
        const deletePromises = idsNotInBothArrays.map(async (element) => {
          await strapi.entityService.delete(
            'api::employee-experience.employee-experience',
            element
          );
        });
        await Promise.all(deletePromises);
      }

      const finalPayload = responseEntries.map((element) => {
        const { id, ...attributes } = element;
        return { id, attributes };
      });

      return { data: finalPayload };
    },
  })
);
