'use strict';

/**
 * employee-experience controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::employee-experience.employee-experience',
  ({ strapi }) => ({
    async customWorkPost(ctx) {
      try {
        // @ts-ignore
        const { employee, workData } = ctx.request.body;
        const data = await strapi
          .service('api::employee-experience.employee-experience')
          .customWork(employee, workData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
