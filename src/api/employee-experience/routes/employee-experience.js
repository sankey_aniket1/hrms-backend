'use strict';

/**
 * employee-experience router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::employee-experience.employee-experience'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/employee-experiences/custom-experience',
    handler: 'api::employee-experience.employee-experience.customWorkPost',
  },
];
module.exports = customRouter(defaultRouter, myExtraRoutes);
