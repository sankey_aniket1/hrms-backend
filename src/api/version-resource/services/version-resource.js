'use strict';

/**
 * version-resource service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::version-resource.version-resource',
  ({ strapi }) => ({
    async getVersionResources() {
      return await strapi.entityService.findMany(
        'plugin::pms.pmsversionresource'
        // {
        //   filters: { version: null },
        // }
      );
    },

    async getVersionResourceDetails(resourceId) {
      // Fetching version Resource details by ID
      const versionResource = await strapi.db
        .query('plugin::pms.pmsversionresource')
        .findOne({
          where: { id: resourceId },
          select: [
            'id',
            'threadName',
            'projectName',
            'start_date',
            'end_date',
            'billable',
            'type',
          ],
        });

      if (!versionResource) {
        throw new Error('Version Resources not found');
      }

      return versionResource;
    },
  })
);
