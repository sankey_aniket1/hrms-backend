'use strict';

/**
 * version-resource controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::version-resource.version-resource',
  ({ strapi }) => ({
    // Get list of all versions resources
    async getVersionResources(ctx) {
      try {
        const data = await strapi
          .service('api::version-resource.version-resource')
          .getVersionResources();
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async getVersionResourceDetails(ctx) {
      try {
        const { id } = ctx.params;

        const versionsDetails = await strapi
          .service('api::version-resource.version-resource')
          .getVersionResourceDetails(parseInt(id));
        ctx.body = versionsDetails;
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async getVersionResourceByID(ctx) {
      try {
        const { id } = ctx.params;
        const data = await strapi
          .service('api::version-resource.version-resource')
          .getVersionResourceByID(id);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
