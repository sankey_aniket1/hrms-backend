'use strict';

/**
 * version-resource router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::version-resource.version-resource'
);

const myExtraRoutes = [
  {
    method: 'GET',
    path: '/version-resources',
    handler: 'api::version-resource.version-resource.getVersionResources',
    config: {
      policies: [],
    },
  },

  {
    method: 'GET',
    path: '/version-resource-details/:id',
    handler: 'api::version-resource.version-resource.getVersionResourceDetails',
    config: {
      policies: [],
    },
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
