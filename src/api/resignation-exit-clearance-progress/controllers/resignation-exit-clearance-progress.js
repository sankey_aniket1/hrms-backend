'use strict';

/**
 * resignation-exit-clearance-progress controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-exit-clearance-progress.resignation-exit-clearance-progress');
