'use strict';

/**
 * resignation-exit-clearance-progress service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-exit-clearance-progress.resignation-exit-clearance-progress');
