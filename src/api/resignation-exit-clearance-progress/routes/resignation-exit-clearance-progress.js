'use strict';

/**
 * resignation-exit-clearance-progress router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-exit-clearance-progress.resignation-exit-clearance-progress');
