'use strict';

/**
 * checklist-enum router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::checklist-enum.checklist-enum');
