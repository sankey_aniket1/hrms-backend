'use strict';

/**
 * checklist-enum service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::checklist-enum.checklist-enum');
