'use strict';

/**
 * checklist-enum controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::checklist-enum.checklist-enum');
