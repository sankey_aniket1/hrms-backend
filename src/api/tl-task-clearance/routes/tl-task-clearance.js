'use strict';

/**
 * tl-task-clearance router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::tl-task-clearance.tl-task-clearance'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/tl-task-clearances/custom-clearance',
    handler: 'api::tl-task-clearance.tl-task-clearance.customClearancePost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
