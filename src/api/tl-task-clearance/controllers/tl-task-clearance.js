'use strict';

/**
 * tl-task-clearance controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::tl-task-clearance.tl-task-clearance',
  ({ strapi }) => ({
    async customClearancePost(ctx) {
      try {
        // @ts-ignore
        const { resignationId, clearanceData } = ctx.request.body;
        const data = await strapi
          .service('api::tl-task-clearance.tl-task-clearance')
          .customClearance(resignationId, clearanceData);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
