'use strict';

/**
 * version-comment-or-request controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::version-comment-or-request.version-comment-or-request',
  ({ strapi }) => ({
    async addCommentOrRequest(ctx) {
      try {
        const { comments, isRequested, isRequestApproved, version } =
          // @ts-ignore
          ctx.request.body;

        if (
          !comments ||
          isRequested === undefined ||
          isRequestApproved === undefined ||
          !version
        ) {
          return ctx.badRequest('All fields are required');
        }

        const data = await strapi
          .service('api::version-comment-or-request.version-comment-or-request')
          .addCommentOrRequest({
            comments,
            isRequested,
            isRequestApproved,
            version,
          });

        ctx.send({
          message: isRequested
            ? 'Request added successfully'
            : 'Comment added successfully',
          data: data,
        });
      } catch (err) {
        strapi.log.error(err.message);
        ctx.internalServerError(err);
      }
    },

    async getCommentOrRequestByID(ctx) {
      try {
        const { id } = ctx.params;
        const data = await strapi
          .service('api::version-comment-or-request.version-comment-or-request')
          .getCommentOrRequestByID(id);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
