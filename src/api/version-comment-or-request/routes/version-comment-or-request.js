'use strict';

/**
 * version-comment-or-request router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::version-comment-or-request.version-comment-or-request'
);
const myExtraRoutes = [
  {
    method: 'POST',
    path: '/add-comment-or-request',
    handler:
      'api::version-comment-or-request.version-comment-or-request.addCommentOrRequest',
    config: {
      policies: [],
    },
  },
  {
    method: 'GET',
    path: '/get-comment-or-request/:id',
    handler:
      'api::version-comment-or-request.version-comment-or-request.getCommentOrRequestByID',
    config: {
      policies: [],
    },
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
