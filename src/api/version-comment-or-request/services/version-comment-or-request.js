'use strict';

/**
 * version-comment-or-request service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::version-comment-or-request.version-comment-or-request',
  ({ strapi }) => ({
    async addCommentOrRequest(data) {
      try {
        const newCommentOrRequest = await strapi
          .query('plugin::pms.versioncommentorrequest')
          .create({ data });
        return newCommentOrRequest;
      } catch (err) {
        strapi.log.error(err.message);
        throw new Error('Failed to add comment or request');
      }
    },

    async getCommentOrRequestByID(reqId) {
      const commentsOrReq = await strapi.db
        .query('plugin::pms.versioncommentorrequest')
        .findOne({
          where: { id: reqId },
          select: [
            'id',
            'comments',
            'isRequested',
            'isRequestApproved',
            // 'version',
            // 'createdAt',
          ],
        });

      if (!commentsOrReq) {
        throw new Error('Comments Or Request not found!');
      }

      return commentsOrReq;
    },
  })
);
