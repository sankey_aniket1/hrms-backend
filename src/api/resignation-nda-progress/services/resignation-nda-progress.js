'use strict';

/**
 * resignation-nda-progress service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-nda-progress.resignation-nda-progress');
