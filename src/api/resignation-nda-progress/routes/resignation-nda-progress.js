'use strict';

/**
 * resignation-nda-progress router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-nda-progress.resignation-nda-progress');
