'use strict';

/**
 * resignation-nda-progress controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-nda-progress.resignation-nda-progress');
