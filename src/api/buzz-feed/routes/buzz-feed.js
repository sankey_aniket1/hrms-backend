'use strict';

/**
 * buzz-feed router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::buzz-feed.buzz-feed');
