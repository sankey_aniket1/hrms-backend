'use strict';

/**
 * buzz-feed controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::buzz-feed.buzz-feed');
