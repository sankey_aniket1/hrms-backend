'use strict';

/**
 * buzz-feed service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::buzz-feed.buzz-feed');
