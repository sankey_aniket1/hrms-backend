'use strict';

/**
 * version-detail service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::version-detail.version-detail',
  ({ strapi }) => ({
    async getAllVersions(query) {
      try {
        // Set default values for pagination
        const page = query.currentPage ? parseInt(query.currentPage, 10) : 1;
        const pageSize = query.pageSize ? parseInt(query.pageSize, 10) : 10;
        const searchText = query.searchText ? query.searchText.trim() : '';

        // Calculate start and limit for pagination
        const start = (page - 1) * pageSize;
        const limit = pageSize;

        // search filter
        const searchFilter = searchText
          ? {
              $or: [
                { versionName: { $containsi: searchText } },
                { projectName: { $containsi: searchText } },
                { thread: { threadName: { $containsi: searchText } } },
              ],
            }
          : {};

        // Fetch versions with pagination and related threads
        const versions = await strapi.entityService.findMany(
          'plugin::pms.pmsversion',
          {
            populate: ['thread'],
            filters: searchFilter,
            limit,
            start,
          }
        );

        // Map versions to include thread names
        const versionsWithThreadName = versions.map((version) => {
          const { thread, ...rest } = version;
          return {
            ...rest,
            threadName: thread ? thread.threadName : null,
          };
        });

        // Get the total count of versions matching the search filter
        const totalCount = await strapi.entityService.count(
          'plugin::pms.pmsversion',
          {
            filters: searchFilter,
          }
        );

        // Calculate total pages
        const totalPages = Math.ceil(totalCount / pageSize);

        // Return data with pagination metadata
        return {
          data: versionsWithThreadName,
          meta: {
            pagination: {
              page,
              pageSize,
              pageCount: totalPages,
              total: totalCount,
            },
          },
        };
      } catch (err) {
        strapi.log.error('Error fetching versions data', err);
        throw new Error('Unable to fetch versions data');
      }
    },

    async getVersionDetails(versionId) {
      try {
        // Fetch version details
        const versionDetailsQuery = `
          SELECT 
            pv.*, 
            pv.id as pv_id,
            pv.description as pv_description,
            pv.status as pv_status,
            pt.*, 
            pt.description as pt_description,
            pp.*, 
            pp.description as pp_description
          FROM 
            pmsversions pv
          JOIN 
            pmsversions_thread_links ptl ON pv.id = ptl.pmsversion_id
          JOIN 
            pms_threads pt ON ptl.pmsthread_id = pt.id
          JOIN 
            pms_threads_project_links tpl ON pt.id = tpl.pmsthread_id
          JOIN 
            pmsprojects pp ON tpl.pmsproject_id = pp.id
          WHERE 
            pv.id = ${versionId}`;

        const versionResourcesQuery = `
          SELECT vr.*, e.first_name, e.last_name
          FROM pmsversionresources vr
          JOIN pmsversionresources_version_dls_links vrl ON vr.id = vrl.pmsversionresource_id
          JOIN pmsversionresources_employee_links vel ON vel.pmsversionresource_id = vr.id
          JOIN employees e ON e.id = vel.employee_id
          WHERE vrl.pmsversion_id = ${versionId}`;

        const versionCommentsQuery = `
          SELECT vc.*
          FROM versioncommentorrequest vc
          JOIN versioncommentorrequest_version_links vcl ON vc.id = vcl.versioncommentorrequest_id 
          WHERE vcl.pmsversion_id = ${versionId}`;

        const [versionDetails, versionResources, versionCommentsOrRequests] =
          await Promise.all([
            strapi.db.connection.raw(versionDetailsQuery),
            strapi.db.connection.raw(versionResourcesQuery),
            strapi.db.connection.raw(versionCommentsQuery),
          ]);

        const threadProjectData = versionDetails.rows;
        if (!threadProjectData.length) {
          throw new Error('Details not found');
        }
        const data = threadProjectData[0];

        const modifiedVersionResources = versionResources.rows.map(
          (resource) => ({
            id: resource.id,
            createdAt: resource.created_at,
            updatedAt: resource.updated_at,
            threadName: resource.thread_name,
            projectName: resource.project_name,
            billable: resource.billable,
            type: resource.type,
            startDate: resource.start_date,
            endDate: resource.end_date,
            resourceName: `${resource.first_name} ${resource.last_name}`,
          })
        );

        const formattedCommentsOrRequests = versionCommentsOrRequests.rows.map(
          (commentOrRequest) => ({
            id: commentOrRequest.id,
            comments: commentOrRequest.comments,
            isRequested: commentOrRequest.is_requested,
            isRequestApproved: commentOrRequest.is_request_approved,
            createdAt: commentOrRequest.created_at,
            updatedAt: commentOrRequest.updated_at,
          })
        );

        // Structuring the response
        const versionDetailsWithId = {
          id: data.pv_id,
          versionName: data.version_name,
          description: data.pv_description,
          startDate: data.start_date,
          endDate: data.end_date,
          isDelayed: data.is_delayed,
          status: data.pv_status,
          resources: modifiedVersionResources,
          commentsOrRequests: formattedCommentsOrRequests,
          createdAt: data.created_at,
          updatedAt: data.updated_at,
          projectName: data.project_name,
          projectDetails: {
            id: data.pmsproject_id,
            projectName: data.project_name,
            description: data.pp_description,
            clientName: data.client_name,
            startDate: data.start_date,
            endDate: data.end_date,
            projectOwner: data.project_owner,
            region: data.region,
            country: data.country,
            status: data.status,
            createdAt: data.created_at,
            updatedAt: data.updated_at,
          },
          threadDetails: {
            id: data.pmsthread_id,
            threadName: data.thread_name,
            threadLead: data.thread_lead,
            description: data.pt_description,
            domainName: data.domain_name,
            techStack: data.tech_stack,
            type: data.type,
            createdAt: data.created_at,
            updatedAt: data.updated_at,
          },
        };

        return versionDetailsWithId;
      } catch (error) {
        strapi.log.error('Error fetching version details:', error);
        throw error;
      }
    },

    async getVersionByID(versionId) {
      // Fetching version details by ID
      const version = await strapi.db.query('plugin::pms.pmsversion').findOne({
        where: { id: versionId },
        select: [
          'id',
          'versionName',
          'description',
          'status',
          'startDate',
          'endDate',
          'isDelayed',
          'createdAt',
          'updatedAt',
        ],
      });

      if (!version) {
        throw new Error('Version not found');
      }

      return version;
    },
  })
);
