'use strict';

/**
 * version-detail router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter('api::version-detail.version-detail');
const myExtraRoutes = [
  {
    method: 'GET',
    path: '/version/version-list/:searchText?/:currentPage',
    handler: 'api::version-detail.version-detail.getAllVersions',
    config: {
      policies: [],
    },
  },

  {
    method: 'GET',
    path: '/version/:id',
    handler: 'api::version-detail.version-detail.getVersionByID',
    config: {
      policies: [],
    },
  },

  {
    method: 'GET',
    path: '/version/version-details/:id',
    handler: 'api::version-detail.version-detail.getVersionDetails',
    config: {
      policies: [],
    },
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
