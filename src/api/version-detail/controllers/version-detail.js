'use strict';

/**
 * version-detail controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::version-detail.version-detail',
  ({ strapi }) => ({
    // Get list of all versions
    async getAllVersions(ctx) {
      try {
        // query parameters for pagination
        const { pageSize = 10 } = ctx.query;
        // searchText from the request parameters
        const searchText = ctx.params.searchText || '';
        const currentPage = ctx.params.currentPage || 1;

        const data = await strapi
          .service('api::version-detail.version-detail')
          .getAllVersions({ currentPage, pageSize, searchText });

        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async getVersionDetails(ctx) {
      try {
        const { id } = ctx.params;

        const versionsDetails = await strapi
          .service('api::version-detail.version-detail')
          .getVersionDetails(parseInt(id));
        ctx.body = versionsDetails;
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },

    async getVersionByID(ctx) {
      try {
        const { id } = ctx.params;
        const data = await strapi
          .service('api::version-detail.version-detail')
          .getVersionByID(id);
        ctx.send(data);
      } catch (err) {
        strapi.log.info(err.message);
        ctx.internalServerError(err);
      }
    },
  })
);
