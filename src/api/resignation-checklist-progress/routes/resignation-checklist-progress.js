'use strict';

/**
 * resignation-checklist-progress router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-checklist-progress.resignation-checklist-progress');
