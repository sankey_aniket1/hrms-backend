'use strict';

/**
 * resignation-checklist-progress service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-checklist-progress.resignation-checklist-progress');
