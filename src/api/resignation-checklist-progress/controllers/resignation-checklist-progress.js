'use strict';

/**
 * resignation-checklist-progress controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-checklist-progress.resignation-checklist-progress');
