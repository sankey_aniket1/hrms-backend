'use strict';

/**
 * employee-asset controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::employee-asset.employee-asset');
