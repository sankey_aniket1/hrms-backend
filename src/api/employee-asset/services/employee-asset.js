'use strict';

/**
 * employee-asset service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::employee-asset.employee-asset');
