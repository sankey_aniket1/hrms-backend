'use strict';

/**
 * employee-asset router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::employee-asset.employee-asset');
