'use strict';

/**
 * resignation-checklist-asset-enum controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-checklist-asset-enum.resignation-checklist-asset-enum');
