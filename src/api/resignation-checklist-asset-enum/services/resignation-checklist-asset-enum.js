'use strict';

/**
 * resignation-checklist-asset-enum service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-checklist-asset-enum.resignation-checklist-asset-enum');
