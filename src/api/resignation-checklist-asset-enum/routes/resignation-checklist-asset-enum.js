'use strict';

/**
 * resignation-checklist-asset-enum router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-checklist-asset-enum.resignation-checklist-asset-enum');
