'use strict';

/**
 * asset service
 */

module.exports = {
  async getRoleScreens(user) {
    const rolesScreens = await strapi.entityService.findMany(
      'api::roles-screen.roles-screen',
      {
        filters: {
          role: {
            id: user.role.id,
          },
        },
        fields: ['id'],
        populate: {
          screens: {
            fields: ['name', 'url', 'subScreens'],
            populate: ['logo'],
          },
        },
      }
    );
    return rolesScreens;
  },
};
