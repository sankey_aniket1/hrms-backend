'use strict';

/**
 * roles-screen router
 */

// const { createCoreRouter } = require('@strapi/strapi').factories;

// module.exports = createCoreRouter('api::roles-screen.roles-screen');
module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/user-screens',
      handler: 'roles-screen.getEmployeeScreens',
      config: {
        policies: [],
        middlewares: [],
      },
    },
  ],
};
