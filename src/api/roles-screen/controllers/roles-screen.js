'use strict';

/**
 * roles-screen controller
 */

module.exports = {
  async getEmployeeScreens(ctx) {
    try {
      // @ts-ignore
      const user = ctx.state.user;
      const data = await strapi
        .service('api::roles-screen.roles-screen')
        .getRoleScreens(user);
      ctx.send(data);
    } catch (err) {
      strapi.log.info(err.message);
      ctx.internalServerError(err);
    }
  },
};
