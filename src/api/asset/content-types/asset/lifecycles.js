async function createEmployeeAssetData(
  employeeId,
  employeeName,
  serialNumber,
  model,
  startDate
) {
  await strapi.entityService.create('api::employee-asset.employee-asset', {
    data: {
      employeeId,
      employeeName,
      serialNumber,
      model,
      startDate,
      publishedAt: Date.now(),
    },
  });
}

async function closeEmployeeAsset(employeeAssetId, endDate) {
  await strapi.entityService.update(
    'api::employee-asset.employee-asset',
    employeeAssetId,
    {
      data: {
        endDate,
        status: 'Inactive',
      },
    }
  );
}

async function getEmployeeAssetDetails(serialNumber) {
  return await strapi.db.query('api::employee-asset.employee-asset').findOne({
    where: {
      serialNumber,
      endDate: null,
      status: 'Active',
    },
    populate: true,
  });
}

async function getEmployeeData(assetId) {
  return await strapi.entityService.findOne('api::asset.asset', assetId, {
    populate: ['employee'],
  });
}

module.exports = {
  async afterUpdate(event) {
    try {
      const { result } = event;

      if (result.publishedAt !== null && result.employee.count !== 0) {
        const getEmployeeAssetDetailsResult = await getEmployeeAssetDetails(
          result.serialNumber
        );
        const getEmployeeDataResult = await getEmployeeData(result.id);

        if (!getEmployeeAssetDetailsResult) {
          await createEmployeeAssetData(
            `${getEmployeeDataResult.employee.empId}`,
            `${getEmployeeDataResult.employee.firstName} ${
              getEmployeeDataResult.employee.lastName || ''
            }`,
            getEmployeeDataResult.serialNumber,
            getEmployeeDataResult.model,
            getEmployeeDataResult.createdAt
          );
        } else {
          if (result.assignedStatus !== 'Assigned') {
            await closeEmployeeAsset(
              getEmployeeAssetDetailsResult.id,
              result.updatedAt
            );
          }
          if (
            getEmployeeDataResult.employee.empId !==
            getEmployeeAssetDetailsResult.employeeId
          ) {
            await closeEmployeeAsset(
              getEmployeeAssetDetailsResult.id,
              result.updatedAt
            );
            await createEmployeeAssetData(
              `${getEmployeeDataResult.employee.empId}`,
              `${getEmployeeDataResult.employee.firstName} ${
                getEmployeeDataResult.employee.lastName || ''
              }`,
              result.serialNumber,
              result.model,
              result.createdAt
            );
          }
        }
      }
    } catch (error) {
      strapi.log.info(`${error}`);
    }
  },
};
