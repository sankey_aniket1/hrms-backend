'use strict';

/**
 * asset router
 */
module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/employee-assets',
      handler: 'asset.getEmployeeAssets',
    },
  ],
};
