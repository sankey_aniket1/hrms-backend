'use strict';

/**
 * asset service
 */

module.exports = {
  async getAssets(user) {
    const userId = await strapi
      .service('api::employee.employee')
      .getEmployeeId(user.id);
    const assets = await strapi.entityService.findMany('api::asset.asset', {
      filters: {
        employee: {
          id: userId.employee.id,
        },
      },
    });
    return assets;
  },
};
