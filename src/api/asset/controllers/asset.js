'use strict';

/**
 * asset controller
 */

module.exports = {
  async getEmployeeAssets(ctx) {
    try {
      // @ts-ignore
      const user = ctx.state.user;
      const data = await strapi.service('api::asset.asset').getAssets(user);
      ctx.send(data);
    } catch (err) {
      strapi.log.info(err.message);
      ctx.internalServerError(err);
    }
  },
};
