'use strict';

/**
 * qualification-list controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::qualification-list.qualification-list'
);
