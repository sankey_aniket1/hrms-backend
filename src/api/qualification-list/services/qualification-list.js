'use strict';

/**
 * qualification-list service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::qualification-list.qualification-list'
);
