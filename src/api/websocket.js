// Initialize WebSocket module with strapi object passed as parameter
module.exports = ({ strapi }) => {
  return {
    name: 'websocket',
    async initialize() {
      const httpServer = strapi.server.httpServer;
      // @ts-ignore
      const io = require('socket.io')(httpServer);
      strapi.io = io;
    },
  };
};
