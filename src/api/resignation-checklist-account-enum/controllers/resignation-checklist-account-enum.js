'use strict';

/**
 * resignation-checklist-account-enum controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-checklist-account-enum.resignation-checklist-account-enum');
