'use strict';

/**
 * resignation-checklist-account-enum service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-checklist-account-enum.resignation-checklist-account-enum');
