'use strict';

/**
 * resignation-checklist-account-enum router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-checklist-account-enum.resignation-checklist-account-enum');
