'use strict';

/**
 * resignation-checklist-exit-form-enum service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resignation-checklist-exit-form-enum.resignation-checklist-exit-form-enum');
