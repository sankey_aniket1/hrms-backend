'use strict';

/**
 * resignation-checklist-exit-form-enum controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resignation-checklist-exit-form-enum.resignation-checklist-exit-form-enum');
