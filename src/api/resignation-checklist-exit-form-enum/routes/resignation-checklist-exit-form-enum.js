'use strict';

/**
 * resignation-checklist-exit-form-enum router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resignation-checklist-exit-form-enum.resignation-checklist-exit-form-enum');
