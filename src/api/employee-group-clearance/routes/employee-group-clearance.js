'use strict';

/**
 * employee-group-clearance router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;
const { customRouter } = require('../../../../config/common');

const defaultRouter = createCoreRouter(
  'api::employee-group-clearance.employee-group-clearance'
);

const myExtraRoutes = [
  {
    method: 'POST',
    path: '/employee-group-clearances/custom-clearance',
    handler:
      'api::employee-group-clearance.employee-group-clearance.customClearancePost',
  },
];

module.exports = customRouter(defaultRouter, myExtraRoutes);
