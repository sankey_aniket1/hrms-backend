// @ts-ignore
import logo from './extensions/sankey-logo.png';
// @ts-ignore
import favicon from './extensions/sankey-favicon.png';

export default {
  config: {
    auth: {
      logo: logo,
    },
    head: {
      favicon: favicon,
    },
    menu: {
      logo: logo,
    },
    translations: {
      en: {
        'app.components.LeftMenu.navbrand.title': 'HRMS Admin Dashboard',
        'app.components.LeftMenu.navbrand.workplace': 'Sankey Solutions',
        'Auth.form.welcome.title': 'HRMS Admin Portal',
        'Auth.form.welcome.subtitle': ' ', //PLEASE DON'T REMOVE SPACE BETWEEN QUOTES
      },
    },
    tutorials: false,
    notifications: { releases: false },
  },

  bootstrap() {},
};
