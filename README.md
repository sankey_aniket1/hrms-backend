# Sankey HRMS

**This application is created using STRAPI.**

**Steps to run this project locally:**

1. Install Node 18.19.0
2. Run `yarn` command
3. Change the database configuration in the .env file
4. Change URL on the .env if using ngrok for authorization on https (Enable provide and add client id and secret in settings after running the project) 
5. Run `yarn develop` command


**Steps to run this project in docker:**

1. Create a docker network. Run `docker network create hrms` command
2. Create a Postgres Container. Run `docker run --name hrms-db -e POSTGRES_PASSWORD=postgrespw -p 5432:5432 -d --network=hrms postgres` command
3. Create a image of Strapi project. Run `docker build -t hrms-backend .` command
4. Make necessary changes to the env.list file before creating the strapi project container
5. Run `docker run --name hrms-backend --env-file env.list -p 1337:1337 -d --network=hrms hrms-backend` command


**Steps to this maintain code:**

1. Make changes to the code
2. Run `yarn scan` command (Proceed only if no errors are detected)
3. Group similar changes in one commit and add commit message for changes
4. Fetch changes from development branch in origin using `git pull origin development`
5. Push the code to your repo - `git push origin (branch-name)`



**Note:**

1. Install LibreOffice from this link on local machine, if using windows (https://www.libreoffice.org/download/download-libreoffice/)
