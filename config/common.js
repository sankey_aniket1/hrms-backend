module.exports = {
  customRouter(innerRouter, extraRoutes = []) {
    let routes;
    return {
      get prefix() {
        return innerRouter.prefix;
      },
      get routes() {
        if (!routes) routes = extraRoutes.concat(innerRouter.routes);
        return routes;
      },
    };
  },
};
