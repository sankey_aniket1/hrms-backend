module.exports = ({ env }) => ({
  'users-permissions': {
    config: {
      jwtSecret: env('JWT_SECRET'),
      jwt: {
        expiresIn: env('JWT_SECRET_EXPIRES'),
      },
    },
  },
  pms: {
    enabled: true,
    resolve: './src/plugins/pms',
  },
  feedbacks: {
    enabled: true,
    resolve: './src/plugins/feedbacks',
  },
});
