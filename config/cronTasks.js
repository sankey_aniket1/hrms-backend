module.exports = {
  myJob: {
    task: async ({ strapi }) => {
      /* Add your own logic here */
      const shiningStarDates = [0, 3, 6, 9];
      const currentDate = new Date();
      if (shiningStarDates.includes(currentDate.getMonth())) {
        currentDate.setMonth(currentDate.getMonth() - 3);
        try {
          // const projects = await strapi.entityService.findMany(
          //   'plugin::pms.pmsproject'
          // );
          const versionResource = await strapi.entityService.findMany(
            'plugin::pms.pmsversionresource',
            {
              filters: {
                endDate: {
                  $gte: currentDate,
                },
              },
            }
          );

          const shinigStarAssignmentFormFormat =
            await strapi.entityService.findMany(
              'plugin::feedbacks.pmsfeedbackassesmentformformat',
              {
                filters: {
                  type: 'Shining Star',
                },
              }
            );
          if (shinigStarAssignmentFormFormat.length) {
            const cooldownDate = new Date();
            cooldownDate.setDate(
              cooldownDate.getDate() +
                shinigStarAssignmentFormFormat[0].cooldownPriod
            );
            await strapi.entityService.createMany(
              'plugin::feedbacks.pmsfeedback',
              {
                data: versionResource.map((resource) => ({
                  feedback: shinigStarAssignmentFormFormat[0].FormParameters,
                  cooldown_period_date: cooldownDate,
                  versionResource: resource.id,
                  feedbackAssesmentForm: shinigStarAssignmentFormFormat[0].id,
                })),
              }
            );
          }
          console.log('versionResource------------', versionResource);
        } catch (error) {
          console.error('Error handling Shining Star event:', error);
          throw error;
        }
      }
    },
    options: {
      // for add period of crown job
      rule: '* * * * *',
      tz: 'Asia/Dhaka',
    },
  },
};
